unit UCostosDoctoInventarios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  Data.DB, FireDAC.Comp.Client, FireDAC.Phys.FB, FireDAC.Phys.FBDef;

type
  TCostosDoctoInventarios = class(TForm)
    btnProceso: TButton;
    txtDoctoID: TEdit;
    Conexion: TFDConnection;
    Label1: TLabel;
    procedure btnProcesoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CostosDoctoInventarios: TCostosDoctoInventarios;

implementation

{$R *.dfm}

procedure TCostosDoctoInventarios.btnProcesoClick(Sender: TObject);
begin
  Conexion.ExecSQL('execute block'+
    ' as'+
    ' declare variable articulo_id integer;'+
    ' declare variable docto_in_det_id integer;'+
    ' declare variable costo double precision;'+
    ' declare variable unid double precision;'+
    ' begin'+
    '     for select articulo_id, docto_in_det_id, unidades from doctos_in_det where docto_in_id = '+txtDoctoID.Text+' into :articulo_id, :docto_in_det_id, :unid do'+
    '     begin'+
    '         select costo_ultima_compra from articulos where articulo_id=:articulo_id into :costo;'+
    '         update doctos_in_det set costo_unitario = :costo, costo_total= (:costo * :unid) where docto_in_det_id=:docto_in_det_id AND costo_unitario=0;'+
    '     end'+
    ' end');
    showmessage('Listo');
end;

end.
