unit UControlCajonesMaquilaDetalleEntrada;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.FB, FireDAC.Phys.FBDef, Vcl.ComCtrls, Data.DB,
  FireDAC.Comp.Client, Vcl.Grids;

type
  TControlCajonesMaquilaDetalleEntrada = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    txtFolioMaquila: TEdit;
    txtProveedor: TEdit;
    txtFecha: TEdit;
    Label13: TLabel;
    Label5: TLabel;
    Edit3: TEdit;
    Label3: TLabel;
    Edit1: TEdit;
    GroupBox1: TGroupBox;
    StringGrid1: TStringGrid;
    Label4: TLabel;
    Edit2: TEdit;
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
  private
    { Private declarations }
  public
    { Public declarations }
    maquila_id:integer;
  end;

var
  ControlCajonesMaquilaDetalleEntrada: TControlCajonesMaquilaDetalleEntrada;

implementation

{$R *.dfm}

end.
