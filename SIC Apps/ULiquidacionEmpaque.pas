unit ULiquidacionEmpaque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, Data.DB, FireDAC.Comp.Client, FireDAC.Comp.DataSet,
  frxClass, Vcl.ComCtrls, Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls, frxDBSet, UConfirmaCostoLiquidacion,
  FireDAC.VCLUI.Error, FireDAC.Comp.UI,system.Character, Vcl.Samples.Gauges,
  Vcl.ImgList, Vcl.DBCtrls, System.DateUtils, FireDAC.VCLUI.Wait,
  System.ImageList;

type
  TLiquidacionEmpaque = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Reporte: TfrxReport;
    Query: TFDQuery;
    Conexion: TFDConnection;
    DataSource: TDataSource;
    StatusBar1: TStatusBar;
    GridRecosteo: TDBGrid;
    GridReporte: TDBGrid;
    Label1: TLabel;
    txtBusquedaReporte: TEdit;
    Label2: TLabel;
    txtBusquedaRecepciones: TEdit;
    QueryRecosteo: TFDQuery;
    DSRecosteo: TDataSource;
    frxDBDataset1: TfrxDBDataset;
    DSReporte: TDataSource;
    QueryReporte: TFDQuery;
    TabSheet3: TTabSheet;
    GridPolizas: TDBGrid;
    QueryPolizas: TFDQuery;
    DSPolizas: TDataSource;
    btnActualizarListaPolizas: TButton;
    btnActualizarListaLiquidacion: TButton;
    btnActualizarListaRecosteo: TButton;
    QueryDetallesFactura: TFDQuery;
    cbMostrarLiquidacion: TComboBox;
    Label3: TLabel;
    Label4: TLabel;
    cbMostrarRecosteo: TComboBox;
    Label5: TLabel;
    txtBusquedaFacturas: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    cbMostrarFacturas: TComboBox;
    ErrorDialog: TFDGUIxErrorDialog;
    TabSheet4: TTabSheet;
    btnCalcularUtilidadViaje: TButton;
    QueryBaseUtilidad: TFDQuery;
    lblProgress: TLabel;
    gaProgress: TGauge;
    ConexionDetallado: TFDConnection;
    cbCalculoTotal: TCheckBox;
    ImageList1: TImageList;
    TabSheet5: TTabSheet;
    txtRecepcionFolio: TEdit;
    txtFlete: TEdit;
    Button1: TButton;
    Label8: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    txtDscto: TEdit;
    btnRecalcularTodo: TButton;
    TabSheet6: TTabSheet;
    cbCuentasConta: TDBLookupComboBox;
    QueryCuentas: TFDQuery;
    DSCuentas: TDataSource;
    btnGuardarGasto: TButton;
    txtImporteGasto: TEdit;
    txtdescGasto: TMemo;
    Label10: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    lblFechaGasto: TLabel;
    dtpFechaGasto: TDateTimePicker;
    GridGastosExtra: TDBGrid;
    QueryGastosExtra: TFDQuery;
    DSGastosExtra: TDataSource;
    btnCancelarGasto: TButton;
    btnNuevoGasto: TButton;
    lblPolizas: TLabel;
    dtpFechaInicioFacturas: TDateTimePicker;
    dtpFechaFinFacturas: TDateTimePicker;
    Label14: TLabel;
    Label15: TLabel;
    btnBalanceGeneral: TButton;
    txtDsctoCompra: TEdit;
    Label16: TLabel;
    btnConcentrarInfo: TButton;
    tbLiquidaciones: TFDTable;
    spActualizaLiquidacion: TFDStoredProc;
    Gauge1: TGauge;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure txtBusquedaReporteChange(Sender: TObject);
    procedure GridReporteDblClick(Sender: TObject);
    procedure GridRecosteoDblClick(Sender: TObject);
    procedure txtBusquedaRecepcionesChange(Sender: TObject);
    procedure btnActualizarListaPolizasClick(Sender: TObject);
    procedure btnActualizarListaLiquidacionClick(Sender: TObject);
    procedure btnActualizarListaRecosteoClick(Sender: TObject);
    procedure GridPolizasDblClick(Sender: TObject);
    procedure cbMostrarLiquidacionChange(Sender: TObject);
    procedure cbMostrarRecosteoChange(Sender: TObject);
    procedure cbMostrarFacturasChange(Sender: TObject);
    procedure txtBusquedaFacturasChange(Sender: TObject);
    procedure btnCalcularUtilidadViajeClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure txtRecepcionFolioExit(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure recalculapoliza();
    procedure btnRecalcularTodoClick(Sender: TObject);
    procedure btnGuardarGastoClick(Sender: TObject);
    procedure DSGastosExtraDataChange(Sender: TObject; Field: TField);
    procedure btnNuevoGastoClick(Sender: TObject);
    procedure btnCancelarGastoClick(Sender: TObject);
    procedure QueryGastosExtraAfterClose(DataSet: TDataSet);
    procedure QueryPolizasAfterOpen(DataSet: TDataSet);
    procedure dtpFechaInicioFacturasChange(Sender: TObject);
    procedure btnBalanceGeneralClick(Sender: TObject);
    procedure btnConcentrarInfoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    condicion_mostrar_liquidacion,condicion_mostrar_recalc,condicion_mostrar_facturas, condicion_fechas:string;
    recepcion_id, gasto_extra_id, cargo_id :integer;
  end;

var
  LiquidacionEmpaque: TLiquidacionEmpaque;

implementation

{$R *.dfm}

{$region 'Funciones Generales'}
procedure TLiquidacionEmpaque.recalculapoliza();
var
factura_id,poliza_id,detalle_id : integer;
  cta_ventas_id,cta_clientes_id,cta_almacen_id,cta_costo_ventas_id:integer;
  cta_ventas,cta_clientes,cta_almacen,cta_costo_ventas, query_str,folio_poliza:string;
  poliza_existe :boolean;
  mes,anio,tipo_poliza_det_id,consecutivo_poliza:integer;
  query_result:variant;
  falta_costo,falta_alm : TStringList;
begin
  falta_costo := TStringList.Create;
  falta_alm := TStringList.create;
  factura_id := strtoint(GridPolizas.Fields[0].AsString);
  QueryDetallesFactura.sql.Text := ' SELECT  a.articulo_id,a.nombre,'+
      ' ad.clave as lote,'+
      ' dvd.unidades,'+
      ' (dvd.precio_unitario*ded.unidades) as precio_detalle,'+
      ' dc.folio as recepcion,'+
      ' dv.importe_neto,'+
      ' lib.facturada,'+
      ' lre.comision,'+
      ' (CASE coalesce(lre.flete,0)'+
                  ' when 0 then (did.costo_unitario*ded.unidades)'+
                  ' else (((dcd.precio_unitario)+(lre.flete / (select sum(unidades) from doctos_cm_det  where docto_cm_id=dcd.docto_cm_id)))*ded.unidades)'+
      ' end) as COSTO_final'+
      ' FROM doctos_ve dv join'+
      ' doctos_ve_det dvd on dvd.docto_ve_id = dv.docto_ve_id  join'+
      ' articulos a on a.articulo_id = dvd.articulo_id join'+
      ' desglose_en_discretos_ve ded on ded.docto_ve_det_id = dvd.docto_ve_det_id left join'+
      ' articulos_discretos ad on ad.art_discreto_id = ded.art_discreto_id left join'+
      ' desglose_en_discretos_cm dedc on dedc.art_discreto_id = ad.art_discreto_id left join'+
      ' doctos_cm_det dcd on dcd.docto_cm_det_id = dedc.docto_cm_det_id join'+
      ' doctos_cm dc on dc.docto_cm_id = dcd.docto_cm_id left join'+
      ' doctos_cm_ligas dl on dl.docto_cm_fte_id = dc.docto_cm_id left join'+
      ' doctos_cm com on com.docto_cm_id = dl.docto_cm_dest_id left join'+
      ' libres_com_cm lib on lib.docto_cm_id = com.docto_cm_id left join'+
      ' libres_rec_cm lre on lre.docto_cm_id = dc.docto_cm_id left join'+
      ' doctos_entre_sis des on des.docto_fte_id = dc.docto_cm_id left join'+
      ' doctos_in di on di.docto_in_id = des.docto_dest_id left join'+
      ' doctos_in_det did on (did.docto_in_id=di.docto_in_id) AND (DID.articulo_id=DCD.articulo_id)'+
      ' where dv.docto_ve_id ='+inttostr(factura_id)+
      ' and dc.estatus in (''P'',''F'')';


      {*********************OBTIENE EL SIGUIENTE FOLIO DE LA POLIZA EN CASO DE QUE NO EXISTA*****************************}
       tipo_poliza_det_id := -1;
       mes := Conexion.ExecSQLScalar('select extract(month from current_date) from rdb$database');
       anio := Conexion.ExecSQLScalar('select extract(year from current_date) from rdb$database');
       query_result := Conexion.ExecSQLScalar('select tipo_poliza_det_id from tipos_polizas_det where tipo_poliza_id = 74 and mes=:mes and ano=:a',[mes,anio]);
       if query_result <> null then
          tipo_poliza_det_id := query_result;
      {*******************************TERMINA DE OBTENER LOS DATOS DE LA POLIZA (FOLIO)**********************************}

      // DEFINE SI YA EXISTE LA POLIZA Y OBTIENE EL ID
      if Conexion.ExecSQLScalar('select count (distinct docto_co_id) from doctos_co_det where refer=:r',[GridPolizas.Fields[1].AsString]) > 0 then
      begin
        poliza_id := Conexion.ExecSQLScalar('select distinct docto_co_id from doctos_co_det where refer=:r',[GridPolizas.Fields[1].AsString]);
        poliza_existe := True;
      end
      else
      begin
        {***********SI YA EXISTE UN DETALLADO DEL TIPO DE POLIZA DE EL MES CORRESPONDIENTE*********}
        if tipo_poliza_det_id > -1 then
        begin
          consecutivo_poliza := Conexion.ExecSQLScalar('select consecutivo from tipos_polizas_det where tipo_poliza_det_id = :id',[tipo_poliza_det_id]);
        end
        else
        begin
          tipo_poliza_det_id := Conexion.ExecSQLScalar('insert into tipo_polizas_det values(-1,74,:anio,:mes,1) returning tipo_poliza_det_id',[anio,mes]);
          consecutivo_poliza := 1;
        end;
        folio_poliza := Format('%.*d',[9, consecutivo_poliza]);

        {***************************INSERTA EL ENCABEZADO DE LA POLIZA*******************************}
        query_str := 'insert into doctos_co(docto_co_id,tipo_poliza_id,poliza,fecha,moneda_id,tipo_cambio,estatus,sistema_origen,descripcion,aplicado)'+
                      ' values(-1,74,'+QuotedStr(folio_poliza)+',current_date,1,1,''N'',''CO'','+Quotedstr('Factura:'+GridPolizas.Fields[1].AsString)+',''N'') returning docto_co_id';
        poliza_id := Conexion.ExecSQLScalar(query_str);

        {**************************ACTUALIZA EL DETALLE DEL TIPO DE POLIZA***************************}
        Conexion.ExecSQL('update tipos_polizas_det set consecutivo = consecutivo+1 where tipo_poliza_det_id = :t',[tipo_poliza_det_id]);
        poliza_existe := False;

        {************************LIGA EL CFDI DE LA FACTURA A LA POLIZA RECIEN CREADA****************}
        query_result := null;
        query_result := Conexion.ExecSQLScalar('SELECT CFDI_ID FROM usos_folios_fiscaleS UFF WHERE UFF.docto_id = :ID',[factura_id]);
        if query_result <> Unassigned then
          Conexion.ExecSQL('insert into doctos_co_cfdi values(-1,:p,:qr,''S'')',[poliza_id,query_result]);
          Conexion.Commit;
      end;

      {***************ELIMINA EL DETALLADO EXCEPTO LAS CUENTAS DE CLIENTES Y LAS DE VENTAS*************}
      Conexion.ExecSQL('delete from doctos_co_det where docto_co_det_id in('+
                       ' SELECT  dcd.docto_co_det_id'+
                       ' FROM DOCTOS_CO_DET dcd JOIN'+
                       ' cuentas_co cc on cc.cuenta_id = dcd.cuenta_id'+
                       ' where dcd.docto_co_id =:id and (cc.cuenta_pt not like ''105%'' and cc.cuenta_pt not like ''401%''))',[poliza_id]);
      {***************TERMINA DE ELIMINAR DETALLADO****************************************************}

      {*******************AGREGA LOS ASIENTOS DE CLIENTES VS VENTAS**********************}
      cta_clientes := Conexion.ExecSQLScalar('select valor from registry where nombre=''CUENTA_CXC''');
      if not poliza_existe then
      begin
        cta_clientes_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt=:cta_ventas',[cta_clientes]);
        query_str := 'insert into doctos_co_det(docto_co_det_id,docto_co_id,cuenta_id,depto_co_id,tipo_asiento'+
                          ',importe,importe_mn,refer,posicion,moneda_id,aplicado) values(-1,'+inttostr(poliza_id)+','+
                          ':cta_clientes_id,76,''C'','+ GridPolizas.Fields[4].asstring+','+GridPolizas.Fields[4].Asstring+','''+GridPolizas.Fields[1].AsString+''',-1,1,''S'')';
        Conexion.ExecSQL(query_str,[cta_clientes_id]);
        Conexion.Commit;

      end;

      QueryDetallesFactura.Open;
      QueryDetallesFactura.First;
      while not QueryDetallesFactura.Eof do
      begin
          {*********ASIENTOS DE VENTAS (SOLO SE INSERTAN CUANDO SE CREA LA POLIZA)***********}
          if not poliza_existe then
          begin
            cta_ventas := Conexion.ExecSQLScalar('execute procedure pol_ve_get_cta_ventas(:a_id,19,''N'')',[QueryDetallesFactura.FieldByName('articulo_id').Value]);
            cta_ventas_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt=:cta_ventas',[cta_ventas]);
            {******SI NO EXISTE UN DETALLE CON ESA CUENTA SE INSERTA*********}
            if (Conexion.ExecSQLScalar('select count(*) from doctos_co_det where docto_co_id=:p and cuenta_id=:cta',[poliza_id,cta_ventas_id])=0) then
            begin
              query_str := 'insert into doctos_co_det(docto_co_det_id,docto_co_id,cuenta_id,depto_co_id,tipo_asiento'+
                          ',importe,importe_mn,refer,posicion,moneda_id,aplicado) values(-1,'+inttostr(poliza_id)+','+
                          ':cta_ventas_id,76,''A'','+
                          QueryDetallesFactura.Fields[4].asstring+','+QueryDetallesFactura.Fields[4].asstring+','''+GridPolizas.Fields[1].AsString+''',-1,1,''S'')';
              Conexion.ExecSQL(query_str,[cta_ventas_id]);
              Conexion.Commit;
              Conexion.ExecSQL('execute procedure recalc_saldos_cta_co(:cta_id)',[cta_ventas_id]);
            end
            {*******SI YA EXISTE UN DETALLE EN ESA POLIZA CON ESA CUENTA SE ACUMULA******}
            else
            begin
              detalle_id := Conexion.ExecSQLScalar('select docto_co_det_id from doctos_co_det where docto_co_id=:p and cuenta_id=:cta',[poliza_id,cta_ventas_id]);
              query_str := 'UPDATE doctos_co_det set importe = importe+:i, importe_mn =importe_mn+:imn where docto_co_det_id = :det_id';
              Conexion.ExecSQL(query_str,[QueryDetallesFactura.Fields[4].asstring,QueryDetallesFactura.Fields[4].asstring,detalle_id]);
              Conexion.Commit;
              Conexion.ExecSQL('execute procedure recalc_saldos_cta_co(:cta_id)',[cta_ventas_id]);
            end;
          end;
          {************************TERMINA ASIENTOS DE VENTAS******************************}

          {******************************************************}
          if QueryDetallesFactura.Fields[7].asstring = 'S' then
          begin
            {****************************ASIENTOS DE COSTOS DE VENTAS**********************************}
             try
                cta_costo_ventas := Conexion.ExecSQLScalar('execute procedure POL_IN_GET_CTA_ALM(:a_id,19,''S'')',[QueryDetallesFactura.FieldByName('articulo_id').Value]);
             except
                 ShowMessage(QueryDetallesFactura.fields[1].AsString);
                 cta_costo_ventas_id := -1;
             end;
              cta_costo_ventas_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt=:cta_ventas',[cta_costo_ventas]);
              {******SI NO EXISTE UN DETALLE CON ESA CUENTA SE INSERTA*********}
              if (Conexion.ExecSQLScalar('select count(*) from doctos_co_det where docto_co_id=:p and cuenta_id=:cta',[poliza_id,cta_costo_ventas_id])=0) then
              begin
                query_str := 'insert into doctos_co_det(docto_co_det_id,docto_co_id,cuenta_id,depto_co_id,tipo_asiento'+
                            ',importe,importe_mn,refer,posicion,moneda_id,aplicado) values(-1,'+inttostr(poliza_id)+','+
                            ':cta_ventas_id,76,''C'','+
                            QueryDetallesFactura.Fields[9].asstring+','+QueryDetallesFactura.Fields[9].asstring+','''+GridPolizas.Fields[1].AsString+''',-1,1,''S'')';
                Conexion.ExecSQL(query_str,[cta_costo_ventas_id]);
                Conexion.Commit;
                Conexion.ExecSQL('execute procedure recalc_saldos_cta_co(:cta_id)',[cta_costo_ventas_id]);
              end
              {*******SI YA EXISTE UN DETALLE EN ESA POLIZA CON ESA CUENTA SE ACUMULA******}
              else
              begin
                detalle_id := Conexion.ExecSQLScalar('select docto_co_det_id from doctos_co_det where docto_co_id=:p and cuenta_id=:cta',[poliza_id,cta_costo_ventas_id]);
                query_str := 'UPDATE doctos_co_det set importe = importe+:i, importe_mn =importe_mn+:imn where docto_co_det_id = :det_id';
                Conexion.ExecSQL(query_str,[QueryDetallesFactura.Fields[9].asstring,QueryDetallesFactura.Fields[9].asstring,detalle_id]);
                Conexion.Commit;
                Conexion.ExecSQL('execute procedure recalc_saldos_cta_co(:cta_id)',[cta_costo_ventas_id]);
              end;
            {*************************TERMINA ASIENTOS DE COSTOS DE VENTAS*****************************}

            {****************************ASIENTOS DE INVENTARIOS**********************************}
              try
                cta_almacen := Conexion.ExecSQLScalar('execute procedure POL_IN_GET_CTA_ALM(:a_id,19,''N'')',[QueryDetallesFactura.FieldByName('articulo_id').Value]);
              except
                ShowMessage(QueryDetallesFactura.fields[1].AsString);
                cta_almacen_id := -1;
              end;
              cta_almacen_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt=:cta_ventas',[cta_almacen]);
              {******SI NO EXISTE UN DETALLE CON ESA CUENTA SE INSERTA*********}
              if (Conexion.ExecSQLScalar('select count(*) from doctos_co_det where docto_co_id=:p and cuenta_id=:cta',[poliza_id,cta_almacen_id])=0) then
              begin
                query_str := 'insert into doctos_co_det(docto_co_det_id,docto_co_id,cuenta_id,depto_co_id,tipo_asiento'+
                            ',importe,importe_mn,refer,posicion,moneda_id,aplicado) values(-1,'+inttostr(poliza_id)+','+
                            ':cta_ventas_id,76,''A'','+
                            QueryDetallesFactura.Fields[9].asstring+','+QueryDetallesFactura.Fields[9].asstring+','''+GridPolizas.Fields[1].AsString+''',-1,1,''S'')';
                Conexion.ExecSQL(query_str,[cta_almacen_id]);
                Conexion.Commit;
                Conexion.ExecSQL('execute procedure recalc_saldos_cta_co(:cta_id)',[cta_almacen_id]);
              end
              {*******SI YA EXISTE UN DETALLE EN ESA POLIZA CON ESA CUENTA SE ACUMULA******}
              else
              begin
                detalle_id := Conexion.ExecSQLScalar('select docto_co_det_id from doctos_co_det where docto_co_id=:p and cuenta_id=:cta',[poliza_id,cta_almacen_id]);
                query_str := 'UPDATE doctos_co_det set importe = importe+:i, importe_mn =importe_mn+:imn where docto_co_det_id = :det_id';
                Conexion.ExecSQL(query_str,[QueryDetallesFactura.Fields[9].asstring,QueryDetallesFactura.Fields[9].asstring,detalle_id]);
                Conexion.Commit;
                Conexion.ExecSQL('execute procedure recalc_saldos_cta_co(:cta_id)',[cta_almacen_id]);
              end;
            {*************************TERMINA ASIENTOS DE INVENTARIOS*****************************}
          end;
          QueryDetallesFactura.Next;
      end;
      Conexion.ExecSQL('update doctos_co set aplicado=''S'' where docto_co_id = :pid',[poliza_id]);
      Conexion.ExecSQL('execute procedure aplica_docto_co(:docto_id,''A'')',[poliza_id]);
      Conexion.Commit;
      {}
      QueryDetallesFactura.Close;
      //********************TERMINA ARTICULOS SIN CUENTAS*****************************
      Conexion.Commit;
end;

function GetNumFolio(folio : String) : String;
var
  i:integer;
begin
  Result := '';
  for I := 1 to (folio.Length) do
  begin
    if (IsNumber(folio[i]))
    then Result := Result+folio[i];
  end;
end;

function GetSerie(folio : String) : String;
var
  i:integer;
begin
  Result := '';
  for I := 1 to (folio.Length) do
  begin
    if (IsLetter(folio[i]))
    then Result := Result+folio[i]
    else exit;
  end;
end;
{$endregion}

{$region 'Botones de Actualizar'}
procedure TLiquidacionEmpaque.btnActualizarListaLiquidacionClick(Sender: TObject);
var
i:integer;
  begin
  if Query.Active then
  begin
    Query.Close;
  end;
  Query.Open;
  for i := 0 to GridReporte.Columns.Count - 1 do
  begin
    GridReporte.Columns[i].Width := 120
  end;
  GridReporte.Columns[0].Visible := False;
  GridReporte.Columns[7].Width := 40;
end;

procedure TLiquidacionEmpaque.btnActualizarListaPolizasClick(Sender: TObject);
var
  i:integer;
begin
  if QueryPolizas.Active then
    begin
    QueryPolizas.Close;
  end;
  Querypolizas.sql.Text := ' select * from ('+
                  ' Select dve.docto_ve_id,'+
                         ' dve.folio,'+
                         ' dve.fecha,'+
                         ' cl.nombre as cliente,'+
                         ' dve.importe_neto,'+
                         ' (select first 1 poliza from doctos_co dc join doctos_co_det dcd on dcd.docto_co_id = dc.docto_co_id'+
                          ' where estatus in (''P'',''N'') and dcd.refer = dve.folio) as poliza,'+
                          ' /**********************DEFINE SI CONTIENE O NO CONTIENE FACTURAS CERRADAS*******************/'+
                         ' (SELECT  iif(position (''x'' in list(coalesce(LIB.cerrada,''x'')))>0,''S'',''N'')'+
                          ' FROM doctos_ve dv join'+
                          ' doctos_ve_det dvd on dvd.docto_ve_id = dv.docto_ve_id  join'+
                          ' articulos a on a.articulo_id = dvd.articulo_id join'+
                          ' desglose_en_discretos_ve ded on ded.docto_ve_det_id = dvd.docto_ve_det_id  join'+
                          ' articulos_discretos ad on ad.art_discreto_id = ded.art_discreto_id  join'+
                          ' desglose_en_discretos_cm dedc on dedc.art_discreto_id = ad.art_discreto_id  join'+
                          ' doctos_cm_det dcd on dcd.docto_cm_det_id = dedc.docto_cm_det_id join'+
                          ' doctos_cm dc on dc.docto_cm_id = dcd.docto_cm_id left join'+
                          ' doctos_cm_ligas dl on dl.docto_cm_fte_id = dc.docto_cm_id left join'+
                          ' doctos_cm com on com.docto_cm_id = dl.docto_cm_dest_id left join'+
                          ' libres_com_cm lib on lib.docto_cm_id = com.docto_cm_id left join'+
                          ' libres_rec_cm lre on lre.docto_cm_id = dc.docto_cm_id'+
                          ' where dv.docto_ve_id = dve.docto_ve_id'+
                          ' and dc.estatus in (''P'',''F'')) as rec_pend'+
                         ' /***********************TERMINA CON COMPRAS FACTURADAS*********************/'+
                  ' from doctos_ve dve join'+
                  ' clientes cl on cl.cliente_id=dve.cliente_id'+
                  ' where dve.tipo_docto=''F'' and dve.estatus=''N'' '+
                  ' order by folio ) where (folio like ''%'+txtBusquedaFacturas.Text+'%'' or'+
                  ' cliente like ''%'+txtBusquedaFacturas.Text+'%'' or poliza like ''%'+txtBusquedaFacturas.Text+'%'')'+
                  ' '+condicion_mostrar_facturas  ;


  QueryPolizas.Open;
  for i := 0 to GridPolizas.Columns.Count - 1 do
  begin
    GridPolizas.Columns[i].Width := 120
  end;
  GridPolizas.Columns[0].Visible := False;

end;

procedure TLiquidacionEmpaque.btnActualizarListaRecosteoClick(Sender: TObject);
var
  i:integer;
begin
   if QueryRecosteo.Active then
  begin
    QueryRecosteo.Close;
  end;
  QueryRecosteo.Open;
  for i := 0 to GridRecosteo.Columns.Count - 1 do
  begin
    GridRecosteo.Columns[i].Width := 120;
  end;
  GridRecosteo.Columns[0].Visible := False;
  GridRecosteo.Columns[7].Width := 40;
end;


procedure TLiquidacionEmpaque.btnBalanceGeneralClick(Sender: TObject);
var
  proveedoresabierto : double;
  existe_registro : integer;
begin
  proveedoresabierto := Conexion.ExecSQLScalar(' select sum(total-(flete_x_caja*unidades)) as saldo'+
 ' from('+
 ' select  a.nombre as almacen,'+
         ' ar.nombre as articulo,'+
         ' ld.clave as loteviaje,'+
         ' ld.existencia as unidades,'+
         ' (lc.flete /(select sum(unidades) from doctos_cm_det where docto_cm_id = dc.docto_cm_id)) as flete_x_caja,'+
         ' (CASE coalesce(lc.flete,0)'+
            ' when 0 then (did.costo_unitario)'+
            ' else ((dcd.precio_unitario)+(lc.flete / (select sum(unidades) from doctos_cm_det  where docto_cm_id=dcd.docto_cm_id)))'+
        ' end) as COSTO,'+
        ' (CASE coalesce(lc.flete,0)'+
            ' when 0 then (ld.existencia*did.costo_unitario)'+
            ' else (ld.existencia*((dcd.precio_unitario)+(lc.flete / (select sum(unidades) from doctos_cm_det  where docto_cm_id=dcd.docto_cm_id))))'+
        ' end) as total,'+
         ' (case COALESCE(LIco.cerrada,''N'')'+
              ' WHEN ''S'' THEN '''''+
              ' ELSE ''X'''+
              ' END) AS ABIERTO,'+
         ' ld.fecha,'+
         ' datediff(day from ld.fecha to current_date) as dias'+
 ' from articulos ar  join'+
 ' almacenes a on 1=1 left join'+
 ' lista_arts_discretos(''L'',''E'',a.almacen_id,''01/01/2000'',current_date,0,null) ld on ld.articulo_id = ar.articulo_id join'+
 ' desglose_en_discretos_cm dedc on dedc.art_discreto_id = ld.art_discreto_id join'+
 ' doctos_cm_det dcd on dcd.docto_cm_det_id = dedc.docto_cm_det_id join'+
 ' doctos_cm dc on dc.docto_cm_id = dcd.docto_cm_id left join'+
 ' libres_rec_cm lc on lc.docto_cm_id = dc.docto_cm_id left join'+
 ' doctos_entre_sis des on des.docto_fte_id = dc.docto_cm_id left join'+
 ' doctos_in di on di.docto_in_id = des.docto_dest_id left join'+
 ' doctos_in_det did on (did.docto_in_id=di.docto_in_id) AND (DID.articulo_id=DCD.articulo_id)left join'+
 ' /*Liga de Recepcion y la compra*/'+
 ' doctos_cm_ligas lig on lig.docto_cm_fte_id = dc.docto_cm_id left join'+
 ' doctos_cm com on com.docto_cm_id = lig.docto_cm_dest_id left join'+
 ' libres_com_cm lico on lico.docto_cm_id = com.docto_cm_id'+
 ' /* fin de las tablas de la liga*/'+
 ' where ld.existencia > 0 and dc.tipo_docto = ''R'' and dc.estatus<>''C'''+
 ' order by  ld.clave,ar.nombre,a.nombre ) where abierto = ''X''');

    existe_registro := Conexion.ExecSQLScalar('select count(*) from registry where nombre = ''SIC_PROVEEDORABIERTO_BALANCEGENERAL'' ');
    if existe_registro = 0
    then Conexion.ExecSQL('insert into Registry values(null,''SIC_PROVEEDORABIERTO_BALANCEGENERAL'',''V'',198,:val,null,null)',[proveedoresabierto])
    else Conexion.ExecSQL('update Registry set Valor=:val where nombre = ''SIC_PROVEEDORABIERTO_BALANCEGENERAL''',[proveedoresabierto]);
    showmessage('Proceso Completado');


end;

procedure TLiquidacionEmpaque.cbMostrarFacturasChange(Sender: TObject);
var
  i:integer;
  fecha_inicio_polizas_str, fecha_fin_polizas_str: string;
begin
  fecha_inicio_polizas_str := FormatDateTime('mm/dd/yyyy', dtpFechaInicioFacturas.Date);
  fecha_fin_polizas_str := FormatDateTime('mm/dd/yyyy', dtpFechaFinFacturas.Date);
  condicion_fechas := ' and fecha between '''+fecha_inicio_polizas_str+''' and '''+fecha_fin_polizas_str+''' ';
  case cbMostrarFacturas.ItemIndex of
    0: condicion_mostrar_facturas := '';
    1: condicion_mostrar_facturas := ' and poliza is null';
    2: condicion_mostrar_facturas := ' and not poliza is null and rec_pend=''S''';
    3: condicion_mostrar_facturas := ' and rec_pend = ''S''';
  end;
  Querypolizas.sql.Text := ' select * from ('+
                  ' Select dve.docto_ve_id,'+
                         ' dve.folio,'+
                         ' dve.fecha,'+
                         ' cl.nombre as cliente,'+
                         ' dve.importe_neto,'+
                         ' (select first 1 poliza from doctos_co dc join doctos_co_det dcd on dcd.docto_co_id = dc.docto_co_id'+
                          ' where estatus in (''P'',''N'') and dcd.refer = dve.folio) as poliza,'+
                          ' /**********************DEFINE SI CONTIENE O NO CONTIENE FACTURAS CERRADAS*******************/'+
                         ' (SELECT  iif(position (''x'' in list(coalesce(LIB.cerrada,''x'')))>0,''S'',''N'')'+
                          ' FROM doctos_ve dv join'+
                          ' doctos_ve_det dvd on dvd.docto_ve_id = dv.docto_ve_id  join'+
                          ' articulos a on a.articulo_id = dvd.articulo_id join'+
                          ' desglose_en_discretos_ve ded on ded.docto_ve_det_id = dvd.docto_ve_det_id  join'+
                          ' articulos_discretos ad on ad.art_discreto_id = ded.art_discreto_id  join'+
                          ' desglose_en_discretos_cm dedc on dedc.art_discreto_id = ad.art_discreto_id  join'+
                          ' doctos_cm_det dcd on dcd.docto_cm_det_id = dedc.docto_cm_det_id join'+
                          ' doctos_cm dc on dc.docto_cm_id = dcd.docto_cm_id left join'+
                          ' doctos_cm_ligas dl on dl.docto_cm_fte_id = dc.docto_cm_id left join'+
                          ' doctos_cm com on com.docto_cm_id = dl.docto_cm_dest_id left join'+
                          ' libres_com_cm lib on lib.docto_cm_id = com.docto_cm_id left join'+
                          ' libres_rec_cm lre on lre.docto_cm_id = dc.docto_cm_id'+
                          ' where dv.docto_ve_id = dve.docto_ve_id'+
                          ' and dc.estatus in (''P'',''F'')) as rec_pend'+
                         ' /***********************TERMINA CON COMPRAS FACTURADAS*********************/'+
                  ' from doctos_ve dve join'+
                  ' clientes cl on cl.cliente_id=dve.cliente_id'+
                  ' where dve.tipo_docto=''F'' and dve.estatus=''N'' '+
                  ' order by folio ) where (folio like ''%'+txtBusquedaFacturas.Text+'%'' or'+
                  ' cliente like ''%'+txtBusquedaFacturas.Text+'%'' or poliza like ''%'+txtBusquedaFacturas.Text+'%'')'+
                  ' '+condicion_fechas + condicion_mostrar_facturas  ;
  QueryPolizas.Open;
  for i := 0 to GridPolizas.Columns.Count - 1 do
  begin
    GridPolizas.Columns[i].Width := 120
  end;
  GridPolizas.Columns[0].Visible := False;

end;

procedure TLiquidacionEmpaque.cbMostrarLiquidacionChange(Sender: TObject);
var
  i:integer;
begin
  case cbMostrarLiquidacion.ItemIndex of
    0: condicion_mostrar_liquidacion := '';
    1: condicion_mostrar_liquidacion := ' and (dpc.cerrada =''N'' or dpc.cerrada is null)';
    2: condicion_mostrar_liquidacion := ' and (dpc.facturada =''S'' or dpc.facturada is null)';
    3: condicion_mostrar_liquidacion := ' and dpc.cerrada = ''S'' ';
  end;
  Query.SQL.Text := ' select * from(SELECT  DC.DOCTO_CM_ID,'+
                            ' dc.folio,'+
                            ' dc.folio_prov,'+
                            ' p.nombre as proveedor,'+
                            ' dc.fecha,'+
                            ' (select first 1 ad.clave from desglose_en_discretos_cm dg join'+
                            ' articulos_discretos ad on ad.art_discreto_id=dg.art_discreto_id'+
                            ' where dg.docto_cm_det_id = (select first 1 docto_cm_det_id from doctos_cm_det where docto_cm_id = dc.docto_cm_id))as lote,'+
                            ' dc.importe_neto,'+
                            ' dc.recalculada'+
                    ' FROM doctos_cm dc join'+
                    ' proveedores p on p.proveedor_id = dc.proveedor_id left join '+
                    ' doctos_cm_ligas dcl on dcl.docto_cm_fte_id = dc.docto_cm_id left join'+
                    ' doctos_cm com on com.docto_cm_id = dcl.docto_cm_dest_id left join'+
                    ' libres_com_cm dpc on dpc.docto_cm_id = com.docto_cm_id'+
                    ' where dc.tipo_docto = ''R'' and dc.estatus in(''P'',''F'')  '+condicion_mostrar_liquidacion+')'+
                    ' where (folio like ''%'+txtBusquedaReporte.Text+'%'' or folio_prov like ''%'+txtBusquedaReporte.Text+'%'''+
                    ' or proveedor like ''%'+txtBusquedaReporte.Text+'%'' or lote like ''%'+txtBusquedaReporte.Text+'%'') order by folio';
  Query.Open;


  for i := 0 to GridReporte.Columns.Count - 1 do
  begin
    GridReporte.Columns[i].Width := 120
  end;
  GridReporte.Columns[0].Visible := False;
  GridReporte.Columns[7].Width := 40;
end;

procedure TLiquidacionEmpaque.cbMostrarRecosteoChange(Sender: TObject);
var
  i:integer;
begin
  case cbMostrarRecosteo.ItemIndex of
    0: condicion_mostrar_recalc := '';
    1: condicion_mostrar_recalc := ' and (dpc.cerrada =''N'' or dpc.cerrada is null)';
    2: condicion_mostrar_recalc := ' and (dpc.facturada=''S'' or dpc.facturada is null)';
    3: condicion_mostrar_recalc := ' and dpc.cerrada = ''S'' ';
  end;
  QueryRecosteo.SQL.Text := ' select * from(SELECT  DC.DOCTO_CM_ID,'+
                            ' dc.folio,'+
                            ' dc.folio_prov,'+
                            ' p.nombre as proveedor,'+
                            ' dc.fecha,'+
                            ' (select first 1 ad.clave from desglose_en_discretos_cm dg join'+
                            ' articulos_discretos ad on ad.art_discreto_id=dg.art_discreto_id'+
                            ' where dg.docto_cm_det_id = (select first 1 docto_cm_det_id from doctos_cm_det where docto_cm_id = dc.docto_cm_id))as lote,'+
                            ' dc.importe_neto,'+
                            ' dc.recalculada'+
                    ' FROM doctos_cm dc join'+
                    ' proveedores p on p.proveedor_id = dc.proveedor_id left join '+
                    ' doctos_cm_ligas dcl on dcl.docto_cm_fte_id = dc.docto_cm_id left join'+
                    ' doctos_cm com on com.docto_cm_id = dcl.docto_cm_dest_id left join'+
                    ' libres_com_cm dpc on dpc.docto_cm_id = com.docto_cm_id'+
                    ' where dc.tipo_docto = ''R'' and dc.estatus in(''P'',''F'')  '+condicion_mostrar_recalc+')'+
                    ' where (folio like ''%'+txtBusquedaRecepciones.Text+'%'' or folio_prov like ''%'+txtBusquedaRecepciones.Text+'%'''+
                    ' or proveedor like ''%'+txtBusquedaRecepciones.Text+'%'' or lote like ''%'+txtBusquedaRecepciones.Text+'%'') order by folio';
  QueryRecosteo.Open;

  for i := 0 to GridRecosteo.Columns.Count - 1 do
  begin
    GridRecosteo.Columns[i].Width := 120
  end;
  GridRecosteo.Columns[0].Visible := False;
  GridRecosteo.Columns[7].Width := 40;
end;

procedure TLiquidacionEmpaque.DSGastosExtraDataChange(Sender: TObject;
  Field: TField);
begin
  cbCuentasConta.KeyValue := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where nombre=:nom',[QueryGastosExtra.FieldByName('concepto').AsString]);
  txtImporteGasto.text := QueryGastosExtra.FieldByName('importe').AsString;
  dtpFechaGasto.Date := QueryGastosExtra.FieldByName('fecha').AsDateTime;
  txtdescGasto.Text := QueryGastosExtra.FieldByName('descripcion').asstring;
  gasto_extra_id := QueryGastosExtra.FieldByName('gasto_id').asinteger;

end;

procedure TLiquidacionEmpaque.dtpFechaInicioFacturasChange(Sender: TObject);
var
  fecha_inicio_polizas_str, fecha_fin_polizas_str:string;
  i:integer;
begin
  fecha_inicio_polizas_str := FormatDateTime('mm/dd/yyyy', dtpFechaInicioFacturas.Date);
  fecha_fin_polizas_str := FormatDateTime('mm/dd/yyyy', dtpFechaFinFacturas.Date);
  condicion_fechas := ' and fecha between '''+fecha_inicio_polizas_str+''' and '''+fecha_fin_polizas_str+''' ';
  Querypolizas.sql.Text := ' select * from ('+
                  ' Select dve.docto_ve_id,'+
                         ' dve.folio,'+
                         ' dve.fecha,'+
                         ' cl.nombre as cliente,'+
                         ' dve.importe_neto,'+
                         ' (select first 1 poliza from doctos_co dc join doctos_co_det dcd on dcd.docto_co_id = dc.docto_co_id'+
                          ' where estatus in (''P'',''N'') and dcd.refer = dve.folio) as poliza,'+
                          ' /**********************DEFINE SI CONTIENE O NO CONTIENE FACTURAS CERRADAS*******************/'+
                         ' (SELECT  iif(position (''x'' in list(coalesce(LIB.cerrada,''x'')))>0,''S'',''N'')'+
                          ' FROM doctos_ve dv join'+
                          ' doctos_ve_det dvd on dvd.docto_ve_id = dv.docto_ve_id  join'+
                          ' articulos a on a.articulo_id = dvd.articulo_id join'+
                          ' desglose_en_discretos_ve ded on ded.docto_ve_det_id = dvd.docto_ve_det_id  join'+
                          ' articulos_discretos ad on ad.art_discreto_id = ded.art_discreto_id  join'+
                          ' desglose_en_discretos_cm dedc on dedc.art_discreto_id = ad.art_discreto_id  join'+
                          ' doctos_cm_det dcd on dcd.docto_cm_det_id = dedc.docto_cm_det_id join'+
                          ' doctos_cm dc on dc.docto_cm_id = dcd.docto_cm_id left join'+
                          ' doctos_cm_ligas dl on dl.docto_cm_fte_id = dc.docto_cm_id left join'+
                          ' doctos_cm com on com.docto_cm_id = dl.docto_cm_dest_id left join'+
                          ' libres_com_cm lib on lib.docto_cm_id = com.docto_cm_id left join'+
                          ' libres_rec_cm lre on lre.docto_cm_id = dc.docto_cm_id'+
                          ' where dv.docto_ve_id = dve.docto_ve_id'+
                          ' and dc.estatus in (''P'',''F'')) as rec_pend'+
                         ' /***********************TERMINA CON COMPRAS FACTURADAS*********************/'+
                  ' from doctos_ve dve join'+
                  ' clientes cl on cl.cliente_id=dve.cliente_id'+
                  ' where dve.tipo_docto=''F'' and dve.estatus=''N'' '+
                  ' order by folio ) where (folio like ''%'+txtBusquedaFacturas.Text+'%'' or'+
                  ' cliente like ''%'+txtBusquedaFacturas.Text+'%'' or poliza like ''%'+txtBusquedaFacturas.Text+'%'')'+
                  ' '+condicion_fechas + condicion_mostrar_facturas;


  QueryPolizas.Open;
  for i := 0 to GridPolizas.Columns.Count - 1 do
  begin
    GridPolizas.Columns[i].Width := 120
  end;
  GridPolizas.Columns[0].Visible := False;
end;

{$endregion}

{$region 'Eventos del Form'}
procedure TLiquidacionEmpaque.FormClose(Sender: TObject;  var Action: TCloseAction);
begin
  Application.Terminate;
end;

procedure TLiquidacionEmpaque.FormShow(Sender: TObject);
var
  i,campo_recalc, tabla_util, tabla_gastos:integer;
  fecha_inicio_polizas_str,fecha_fin_polizas_str : string;
begin
  campo_recalc := Conexion.ExecSQLScalar('select count(*) from rdb$relation_fields where rdb$field_name=''RECALCULADA'' AND RDB$RELATION_NAME=''DOCTOS_CM''');
  if campo_recalc = 0
  then Conexion.ExecSQL('alter table doctos_cm add recalculada si_no_n');

  tabla_util := Conexion.ExecSQLScalar('select count(*) from rdb$relations where rdb$relation_name = ''SIC_UTILIDAD'' ');
  if tabla_util = 0
  then  Conexion.ExecSQL(' CREATE TABLE SIC_UTILIDAD ('+
        ' ORDEN_COMPRA                    VARCHAR(9),'+
        ' NUM_COMPRA                      VARCHAR(9),'+
        ' PROVEEDOR                       VARCHAR(100),'+
        ' FOLIO_PROV                      VARCHAR(9),'+
        ' IMPORTE_NETO                    DOUBLE PRECISION,'+
        ' NC                              DOUBLE PRECISION,'+
        ' FACTMENNC                       DOUBLE PRECISION,'+
        ' SALDO_CARGO                     DOUBLE PRECISION,'+
        ' FLETE                           DOUBLE PRECISION,'+
        ' OTROS_GASTOS                    DOUBLE PRECISION,'+
        ' STATUS                          VARCHAR(20),'+
        ' VENTAS                          DOUBLE PRECISION,'+
        ' EXISTENCIA                      DOUBLE PRECISION,'+
        ' COSTO_CERRADO                   DOUBLE PRECISION,'+
        ' COSTO_X_CAJA_FLETE              DOUBLE PRECISION,'+
        ' COSTO_VIAJES_ABIERTOS           DOUBLE PRECISION,'+
        ' COSTO_VIAJES_CERRADOS_CON_EXIS  DOUBLE PRECISION,'+
        ' COSTO_VIAJES_CERRADOS_SIN_EXIS  DOUBLE PRECISION,'+
        ' UTILIDAD_ANTES_NC_FYC           DOUBLE PRECISION,'+
        ' DSCTO_VENTA                     DOUBLE PRECISION,'+
        ' DSCTO_COMPRAS                   DOUBLE PRECISION,'+
        ' UT_FINAL_ANTES_GASTOS_FLETES    DOUBLE PRECISION,'+
        ' UNIDADES_VENDIDAS               DOUBLE PRECISION,'+
        ' CAJAS_RECIBIDAS                 DOUBLE PRECISION,'+
        ' UT_X_CAJA_IMPORTE               DOUBLE PRECISION,'+
        ' UT_X_CAJA_PCTJE                 DOUBLE PRECISION,'+
        ' FECHAHORA_CALCULO               FECHA_Y_HORA DEFAULT current_timestamp '+
        ' );');
  Conexion.Commit;

  tabla_util := Conexion.ExecSQLScalar('select count(*) from rdb$relations where rdb$relation_name = ''SIC_LIQUIDACIONES'' ');
  if tabla_util = 0
  then  Conexion.ExecSQL(' CREATE TABLE SIC_LIQUIDACIONES ( '+
  											 ' FOLIO_RECEPCION    VARCHAR(9), '+
                         '  VIAJE              VARCHAR(10), '+
                         ' ARTICULO_ID        INTEGER,        '+
                         ' EXISTENCIA         DOUBLE PRECISION, '+
                         ' UNIDADES_VENDIDAS  DOUBLE PRECISION,   '+
                         ' IMPORTE_VENTAS     DOUBLE PRECISION  '+
                     ' );');
  Conexion.Commit;
 { if UpperCase(Conexion.Params.Values['User_Name']) = UpperCase('SYSDBA') then
  begin
    Conexion.ExecSQL(' create or alter procedure sic_actualiza_viaje ('+#13+
                          ' viaje varchar(10))'+#13+
                      ' as'+#13+
                      ' declare variable art_discreto_id integer;'+#13+
                      ' declare variable docto_cm_id integer;'+#13+
                      ' declare variable folio_recepcion varchar(9);'+#13+
                      ' declare variable articulo_id integer;'+#13+
                      ' declare variable existencia double precision;'+#13+
                      ' declare variable unidades_vendidas double precision;'+#13+
                      ' declare variable importe_vendido double precision;'+#13+
                      ' begin'+#13+
                        ' /* Se actualiza el registro del viaje en cuestion */'+#13+
                        ' select first 1 distinct dc.folio from doctos_cm dc join'+#13+
                        ' doctos_cm_det dcd on dcd.docto_cm_id = dc.docto_cm_id join'+#13+
                        ' desglose_en_discretos_cm dedc on dedc.docto_cm_det_id = dcd.docto_cm_det_id join'+#13+
                        ' articulos_discretos ad on ad.art_discreto_id = dedc.art_discreto_id'+#13+
                        ' where dc.tipo_docto = ''R'' and ad.clave = :viaje into folio_recepcion;'+#13+
                        ' delete from sic_liquidaciones sl where sl.folio_recepcion = :folio_recepcion;'+#13+
                      ' '+#13+
                        ' for select dc.docto_cm_id,'+#13+
                                   ' dc.folio,'+#13+
                                   ' dcd.articulo_id,'+#13+
                                   ' ad.art_discreto_id'+#13+
                        ' from doctos_cm dc join'+#13+
                        ' doctos_cm_det dcd on dcd.docto_cm_id = dc.docto_cm_id join'+#13+
                        ' desglose_en_discretos_cm dedc on dedc.docto_cm_det_id = dcd.docto_cm_det_id join'+#13+
                        ' articulos_discretos ad on ad.art_discreto_id = dedc.art_discreto_id'+#13+
                        ' where dc.tipo_docto = ''R'' and ad.clave = :viaje into docto_cm_id, folio_recepcion,articulo_id,art_discreto_id do'+#13+
                        ' begin'+#13+
                          ' /******************UNIDADES VENDIDAS*********************/'+#13+
                          ' select  coalesce(sum(dvd.unidades),0) as total'+#13+
                          ' from'+#13+
                          ' doctos_ve dv join'+#13+
                          ' doctos_ve_det dvd on dvd.docto_ve_id = dv.docto_ve_id join'+#13+
                          ' articulos a on a.articulo_id = dvd.articulo_id left join'+#13+
                          ' libres_rem_ve lrem on lrem.docto_ve_id = dv.docto_ve_id left join'+#13+
                          ' vendedores ve on ve.vendedor_id = dv.vendedor_id join'+#13+
                          ' clientes cl on cl.cliente_id = dv.cliente_id join'+#13+
                          ' desglose_en_discretos_ve dedv on dedv.docto_ve_det_id = dvd.docto_ve_det_id join'+#13+
                          ' articulos_discretos ad on ad.art_discreto_id = dedv.art_discreto_id'+#13+
                          ' where dv.tipo_docto = ''R'' and dv.estatus in(''P'',''F'') and dvd.articulo_id=:articulo_id and ad.clave =:viaje INTO unidades_vendidas;'+#13+
                      ' '+#13+
                          ' /***************EXISTENCIAS********************/'+#13+
                          ' select sum(ld.existencia) from almacenes al left join'+#13+
                          ' lista_discretos(:articulo_id,al.almacen_id,current_date) ld on 1=1'+#13+
                          ' where ld.art_discreto_id = :art_discreto_id into existencia;'+#13+
                      ' '+#13+
                          ' /**************UNIDADES VENDIDAS*****************/'+#13+
                          ' SELECT coalesce(SUM(UNIDADES),0) FROM auxiliar_discreto(:art_discreto_id,19,''01/01/2000'',CURRENT_DATE) aux'+#13+
                          ' where aux.nombre_concep = ''Remisi�n'' into unidades_vendidas;'+#13+
                      ' '+#13+
                          ' /**************IMPORTE VENDIDO*********************/'+#13+
                          ' SELECT coalesce(SUM(imp),0) as importe'+#13+
                          ' from(select distinct ddv.docto_ve_det_id, (ddv.unidades*dvd.precio_unitario) as imp'+#13+
                          ' FROM auxiliar_discreto(:art_discreto_id,19,''01/01/2000'',CURRENT_DATE) aux JOIN'+#13+
                          ' doctos_ve DV ON DV.folio = AUX.folio join'+#13+
                          ' doctos_ve_det dvd on (dvd.docto_ve_id = dv.docto_ve_id) join'+#13+
                          ' desglose_en_discretos_ve ddv on ddv.docto_ve_det_id = dvd.docto_ve_det_id'+#13+
                          ' where aux.nombre_concep = ''Remisi�n'' AND DV.tipo_docto = ''R'' and ddv.unidades=aux.unidades and ddv.art_discreto_id = :art_discreto_id) into importe_vendido;'+#13+
                      ' '+#13+
                          ' insert into sic_liquidaciones values(:folio_recepcion,:viaje,:articulo_id,:existencia,:unidades_vendidas,:importe_vendido);'+#13+
                        ' end'+#13+
                      ' '+#13+
                      ' end');
    Conexion.Commit;
  end;  }

  Query.Open;
  QueryRecosteo.Open;


  for i := 0 to GridRecosteo.Columns.Count - 1 do
  begin
    GridRecosteo.Columns[i].Width := 120;
  end;
  GridRecosteo.Columns[0].Visible := False;
  GridRecosteo.Columns[7].Width := 40;

  for i := 0 to GridReporte.Columns.Count - 1 do
  begin
    GridReporte.Columns[i].Width := 120;
  end;
  GridReporte.Columns[0].Visible := False;
  GridReporte.Columns[7].Width := 40;

  {*************POLIZAS CON EL FILTRO DE LAS FECHAS*****************}
  dtpFechaInicioFacturas.Date := EncodeDate(CurrentYear,Monthof(Date),1);
  dtpFechaFinFacturas.Date := EndofAMonth(CurrentYear,Monthof(Date));
  fecha_inicio_polizas_str := FormatDateTime('mm/dd/yyyy', dtpFechaInicioFacturas.Date);
  fecha_fin_polizas_str := FormatDateTime('mm/dd/yyyy', dtpFechaFinFacturas.Date);
  condicion_fechas := ' and fecha between '''+fecha_inicio_polizas_str+''' and '''+fecha_fin_polizas_str+''' ';
  Querypolizas.sql.Text := ' select * from ('+
                  ' Select dve.docto_ve_id,'+
                         ' dve.folio,'+
                         ' dve.fecha,'+
                         ' cl.nombre as cliente,'+
                         ' dve.importe_neto,'+
                         ' (select first 1 poliza from doctos_co dc join doctos_co_det dcd on dcd.docto_co_id = dc.docto_co_id'+
                          ' where estatus in (''P'',''N'') and dcd.refer = dve.folio) as poliza,'+
                          ' /**********************DEFINE SI CONTIENE O NO CONTIENE FACTURAS CERRADAS*******************/'+
                         ' (SELECT  iif(position (''x'' in list(coalesce(LIB.cerrada,''x'')))>0,''S'',''N'')'+
                          ' FROM doctos_ve dv join'+
                          ' doctos_ve_det dvd on dvd.docto_ve_id = dv.docto_ve_id  join'+
                          ' articulos a on a.articulo_id = dvd.articulo_id join'+
                          ' desglose_en_discretos_ve ded on ded.docto_ve_det_id = dvd.docto_ve_det_id  join'+
                          ' articulos_discretos ad on ad.art_discreto_id = ded.art_discreto_id  join'+
                          ' desglose_en_discretos_cm dedc on dedc.art_discreto_id = ad.art_discreto_id  join'+
                          ' doctos_cm_det dcd on dcd.docto_cm_det_id = dedc.docto_cm_det_id join'+
                          ' doctos_cm dc on dc.docto_cm_id = dcd.docto_cm_id left join'+
                          ' doctos_cm_ligas dl on dl.docto_cm_fte_id = dc.docto_cm_id left join'+
                          ' doctos_cm com on com.docto_cm_id = dl.docto_cm_dest_id left join'+
                          ' libres_com_cm lib on lib.docto_cm_id = com.docto_cm_id left join'+
                          ' libres_rec_cm lre on lre.docto_cm_id = dc.docto_cm_id'+
                          ' where dv.docto_ve_id = dve.docto_ve_id'+
                          ' and dc.estatus in (''P'',''F'')) as rec_pend'+
                         ' /***********************TERMINA CON COMPRAS FACTURADAS*********************/'+
                  ' from doctos_ve dve join'+
                  ' clientes cl on cl.cliente_id=dve.cliente_id'+
                  ' where dve.tipo_docto=''F'' and dve.estatus=''N'' '+
                  ' order by folio ) where (folio like ''%'+txtBusquedaFacturas.Text+'%'' or'+
                  ' cliente like ''%'+txtBusquedaFacturas.Text+'%'' or poliza like ''%'+txtBusquedaFacturas.Text+'%'')'+
                  ' '+condicion_fechas;


  QueryPolizas.Open;
  for i := 0 to GridPolizas.Columns.Count - 1 do
  begin
    GridPolizas.Columns[i].Width := 120
  end;
  GridPolizas.Columns[0].Visible := False;

  {**************TERMINA POLIZAS CON FILTRO DE FECHAS**************}

  //PageControl1.Pages[3].TabVisible := Conexion.Params.Values['User_Name'] = 'SYSDBA';
  PageControl1.Pages[4].TabVisible := Conexion.Params.Values['User_Name'] = 'SYSDBA';
  PageControl1.ActivePageIndex := 0;
  Conexion.Open;
  QueryCuentas.Open;
  QueryCuentas.First;
  cbCuentasConta.KeyValue := QueryCuentas.FieldByName('cta').Asstring;


  //GASTOS EXTRA
  tabla_gastos := Conexion.ExecSQLScalar('select count(*) from rdb$relations where rdb$relation_name = ''SIC_GASTOS_EXTRA'' ');
  if tabla_gastos = 0 then
  begin
    Conexion.ExecSQL(' CREATE TABLE SIC_GASTOS_EXTRA ('+
                          ' GASTO_ID     INTEGER,'+
                          ' CUENTA       VARCHAR(10),'+
                          ' FECHA        DATE,'+
                          ' IMPORTE      DOUBLE PRECISION,'+
                          ' DESCRIPCION  VARCHAR(100)'+
                      ' );');
    Conexion.ExecSQL('grant ALL ON sic_gastos_extra TO USUARIO_MICROSIP;');
    Conexion.ExecSQL('CREATE GENERATOR sic_id_gasto_extra;');
    Conexion.ExecSQL('set GENERATOR sic_id_gasto_extra TO 1;');
    Conexion.ExecSQL(' CREATE TRIGGER SIC_gastos_extra_bi0 FOR SIC_gastos_extra'+
                      ' ACTIVE BEFORE INSERT POSITION 0'+
                      ' AS'+
                      ' BEGIN'+
                          ' if (NEW.gasto_id is NULL) then NEW.gasto_id = GEN_ID(sic_id_gasto_extra, 1);'+
                      ' END');
    Conexion.Commit;
  end;
  QueryGastosExtra.open;
  GridGastosExtra.Columns[0].Visible := false;
  GridGastosExtra.Columns[1].Width := 50;
  GridGastosExtra.Columns[1].Width := 50;
  GridGastosExtra.Columns[1].Width := 50;
  GridGastosExtra.Columns[1].Width := 150;
end;
{$endregion}

{$region 'Grids de informacion'}
procedure TLiquidacionEmpaque.GridPolizasDblClick(Sender: TObject);
begin

  if MessageDlg('Desea Generar la P�liza de esta factura o recalcularla?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    recalculapoliza;
    btnActualizarListaPolizas.Click;
  end;

end;

procedure TLiquidacionEmpaque.GridRecosteoDblClick(Sender: TObject);
var
  recepcion_id, af, resultado, compra_id :integer;
  comision,total,flete,otros_gastos,gastos_aduana,monto_a_pagar,
  unidades_docto,total_vendidas,costo_sugerido, costo_calculado, costo_a_insertar : double;
  query_result: variant;
  FCosto : TConfirmaCostoLiquidacion;
begin
  recepcion_id := strtoint(GridRecosteo.Fields[0].AsString);
  if MessageDlg('Desea Recostear Esta Recepci�n?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
    FCosto := TConfirmaCostoLiquidacion.Create(nil);
    //CALCULA EL TOTAL DE LO VENDIDO EN IMPORTE
    query_result := Conexion.ExecSQLScalar(' select  sum(SL.importe_ventas) AS TOTAL'+
            ' from sic_liquidaciones sl join'+
            ' doctos_cm dc on dc.folio = sl.folio_recepcion join'+
            ' doctos_cm_det dcd on dcd.docto_cm_id = dc.docto_cm_id join'+
            ' articulos a on (a.articulo_id = sl.articulo_id AND A.articulo_id = dcd.articulo_id)'+
            ' where dc.docto_cm_id = '+ inttostr(recepcion_id)+' group by dc.docto_cm_id');
    if (query_result <> null)
    then total := query_result
    else total := 0;

    //CALCULA EL TOTAL DE LO VENDIDO EN UNIDADES
    query_result := Conexion.ExecSQLScalar(' select  SUM(SL.unidades_vendidas) AS VENDIDAS'+
            ' from sic_liquidaciones sl join'+
            ' doctos_cm dc on dc.folio = sl.folio_recepcion join'+
            ' doctos_cm_det dcd on dcd.docto_cm_id = dc.docto_cm_id join'+
            ' articulos a on (a.articulo_id = sl.articulo_id AND A.articulo_id = dcd.articulo_id)'+
            ' where dc.docto_cm_id = '+ inttostr(recepcion_id)+' group by dc.docto_cm_id');
    if (query_result <> null)
    then total_vendidas := query_result
    else total_vendidas := 0;


    //comision
    query_result := Conexion.ExecSQLScalar('select comision from libres_rec_cm where docto_cm_id = :d',[recepcion_id]);
    if (query_result <> null) then
    begin
       comision :=query_result/100;
       comision := total * comision;
    end
    else
    begin
       comision := 0;
    end;

    //FLETE
    query_result := Conexion.ExecSQLScalar('select flete from libres_rec_cm where docto_cm_id = :d',[recepcion_id]);
    if (query_result <> null)
    then flete := query_result
    else flete := 0;

    //GASTOS DE ADUANA
    query_result := Conexion.ExecSQLScalar('select gastos_de_aduana from libres_rec_cm where docto_cm_id = :d',[recepcion_id]);
    if (query_result <> null)
    then gastos_aduana := query_result
    else gastos_aduana := 0;


    //OTROS GASTOS
    query_result := Conexion.ExecSQLScalar('select otros_gastos from libres_rec_cm where docto_cm_id = :d',[recepcion_id]);
    if (query_result <> null)
    then otros_gastos := query_result
    else otros_gastos := 0;

    monto_a_pagar := total-comision-flete-gastos_aduana-otros_gastos;
    costo_sugerido := monto_a_pagar/total_vendidas;
    unidades_docto := conexion.ExecSQLScalar('select sum(unidades) from doctos_cm_det where docto_cm_id =:D',[recepcion_id]);
    costo_calculado := monto_a_pagar/unidades_docto;

    // COSTO SUGERIDO
    FCosto.txtCosto.Text := floattostrf(costo_sugerido,ffFixed,4,2);
    FCosto.lblCalculado.Caption := 'Costo Calculado: '+floattostrf(costo_calculado, ffFixed, 4, 2);
    resultado := FCosto.ShowModal;
    if resultado = mrOk
    then costo_a_insertar := FCosto.costo
    else costo_a_insertar := costo_calculado;

    //MODIFICA LA RECEPCION (Y LA COMPRA) CON EL COSTO SELECCIONADO
    if resultado <> mrCancel then
      begin
        //selecciona el id de la compra en caso de que exista
        query_result := Conexion.ExecSQLScalar('select docto_cm_id from doctos_cm dc join'+
                                            ' doctos_cm_ligas dcl on dcl.docto_cm_dest_id = dc.docto_cm_id'+
                                            ' where dcl.docto_cm_fte_id=:d and dc.tipo_docto=''C'' and dc.estatus = ''N'' ',[recepcion_id]);
        if query_result <> null
        then compra_id := query_result
        else compra_id := -1;

        // se modifica el detallado de la recepcion y se recalculan los totales
        af := Conexion.ExecSQL('update doctos_cm_det set precio_unitario = :pu, precio_total_neto=:ptn*unidades where docto_cm_id =:d',[(costo_a_insertar),(( costo_a_insertar)),recepcion_id]);
        Conexion.ExecSQL('execute procedure calc_totales_docto_cm(:d,''N'')',[recepcion_id]);
        Conexion.ExecSQL('execute procedure desAPLICA_DOCTO_CM(:d)',[recepcion_id]);
        Conexion.ExecSQL('execute procedure APLICA_DOCTO_CM(:d)',[recepcion_id]);

        if compra_id <> -1 then
        begin
           af := af + Conexion.ExecSQL('update doctos_cm_det set precio_unitario = :pu, precio_total_neto=:ptn*unidades where docto_cm_id =:d',[(costo_a_insertar),(( costo_a_insertar)),compra_id]);
           Conexion.ExecSQL('execute procedure calc_totales_docto_cm(:d,''N'')',[compra_id]);
        end;
        Conexion.ExecSQL('update doctos_cm set recalculada = ''S'' where docto_cm_id = :d',[recepcion_id]);
        showmessage(inttostr(af)+' Registros actualizados correctamente.');
        btnActualizarListaRecosteo.Click;
        btnActualizarListaLiquidacion.Click;

      end;
    end;

end;

procedure TLiquidacionEmpaque.GridReporteDblClick(Sender: TObject);
var
  recepcion_id :integer;
  txt : TfrxMemoView;
  comision,total,flete,otros_gastos,gastos_aduana,monto_a_pagar, total_vendidas : double;
  query_result: variant;
begin
  recepcion_id := strtoint(GridReporte.Fields[0].AsString);
  if MessageDlg('Desea obtener la Liquidacion de esta recepci�n', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
    //DATOS DE ENCABEZADO
    txt := Reporte.FindObject('txtFolioCompra') as TfrxMemoView;
    txt.Text := 'Viaje Distribuidor: '+GridReporte.Fields[1].AsString;
    txt := Reporte.FindObject('txtFolioRemision') as TfrxMemoView;
    txt.Text := 'Viaje Productor: '+GridReporte.Fields[2].AsString;
    txt := Reporte.FindObject('txtProveedor') as TfrxMemoView;
    txt.Text := 'Productor: '+GridReporte.Fields[3].AsString;

    QueryReporte.SQL.Text := ' select  a.nombre,'+
            ' dcd.unidades as entrada,'+
            ' sl.existencia,'+
            ' SL.unidades_vendidas AS VENDIDAS,'+
            ' SL.importe_ventas AS TOTAL,'+
            ' (case sl.unidades_vendidas'+
            '   when 0 then 0'+
            '   else (sl.importe_ventas/sl.unidades_vendidas)'+
            '   end) as promedio'+
            ' from sic_liquidaciones sl join'+
            ' doctos_cm dc on dc.folio = sl.folio_recepcion join'+
            ' doctos_cm_det dcd on dcd.docto_cm_id = dc.docto_cm_id join'+
            ' articulos a on (a.articulo_id = sl.articulo_id AND A.articulo_id = dcd.articulo_id)'+
            ' where dc.docto_cm_id = '+ inttostr(recepcion_id) ;

    //CALCULA EL TOTAL DE LO VENDIDO EN IMPORTE
    query_result := Conexion.ExecSQLScalar(' select  sum(SL.importe_ventas) AS TOTAL'+
            ' from sic_liquidaciones sl join'+
            ' doctos_cm dc on dc.folio = sl.folio_recepcion join'+
            ' doctos_cm_det dcd on dcd.docto_cm_id = dc.docto_cm_id join'+
            ' articulos a on (a.articulo_id = sl.articulo_id AND A.articulo_id = dcd.articulo_id)'+
            ' where dc.docto_cm_id = '+ inttostr(recepcion_id)+' group by dc.docto_cm_id');
    if (query_result <> null)
    then total := query_result
    else total := 0;

    //CALCULA EL TOTAL DE LO VENDIDO EN UNIDADES
    query_result := Conexion.ExecSQLScalar(' select  SUM(SL.unidades_vendidas) AS VENDIDAS'+
            ' from sic_liquidaciones sl join'+
            ' doctos_cm dc on dc.folio = sl.folio_recepcion join'+
            ' doctos_cm_det dcd on dcd.docto_cm_id = dc.docto_cm_id join'+
            ' articulos a on (a.articulo_id = sl.articulo_id AND A.articulo_id = dcd.articulo_id)'+
            ' where dc.docto_cm_id = '+ inttostr(recepcion_id)+' group by dc.docto_cm_id');
    if (query_result <> null)
    then total_vendidas := query_result
    else total_vendidas := 0;

    QueryReporte.Open;
    try
      Reporte.preparereport;
    EXCEPT

    end;


    //comision
    txt := Reporte.FindObject('txtComisionEnc') as TfrxMemoView;
    query_result := Conexion.ExecSQLScalar('select comision from libres_rec_cm where docto_cm_id = :d',[recepcion_id]);
    if (query_result <> null) then
    begin
       comision :=query_result/100;
       txt.text := 'Comisi�n: '+floattostr(comision*100)+'%';
       txt := Reporte.FindObject('txtComision') as TfrxMemoView;
       comision := total * comision;
       txt.Text := format('%m',[(comision)]);
    end
    else
    begin
       txt.Text := '0';
       comision := 0;
    end;

    //FLETE
    txt := Reporte.FindObject('txtFlete') as TfrxMemoView;
    query_result := Conexion.ExecSQLScalar('select flete from libres_rec_cm where docto_cm_id = :d',[recepcion_id]);
    if (query_result <> null) then
    begin
       flete := query_result;
       txt.Text := format('%m',[flete]);
    end
    else
    begin
       txt.Text := '';
       flete := 0;
    end;

    //GASTOS DE ADUANA
    txt := Reporte.FindObject('txtGastosAduana') as TfrxMemoView;
    query_result := Conexion.ExecSQLScalar('select gastos_de_aduana from libres_rec_cm where docto_cm_id = :d',[recepcion_id]);
    if (query_result <> null) then
    begin
       gastos_aduana := query_result;
       txt.Text := format('%m',[gastos_aduana]);
    end
    else
    begin
       txt.Text := '';
       gastos_aduana := 0;
    end;

    //OTROS GASTOS
    txt := Reporte.FindObject('txtOtrosGastos') as TfrxMemoView;
    query_result := Conexion.ExecSQLScalar('select otros_gastos from libres_rec_cm where docto_cm_id = :d',[recepcion_id]);
    if (query_result <> null) then
    begin
       otros_gastos := query_result;
       txt.Text := format('%m',[otros_gastos]);
    end
    else
    begin
       txt.Text := '';
       otros_gastos := 0;
    end;

    monto_a_pagar := total-comision-flete-gastos_aduana-otros_gastos;
    txt := Reporte.FindObject('txtMontoFinal') as TfrxMemoView;
    txt.Text := format('%m',[monto_a_pagar]);
    txt := Reporte.FindObject('txtCostoSugerido') as TfrxMemoView;
    if total_vendidas > 0 then    
      txt.Text := format('%m',[monto_a_pagar/total_vendidas]);
    Reporte.ShowReport(True);
    end;

end;

procedure TLiquidacionEmpaque.btnRecalcularTodoClick(Sender: TObject);
begin
  //glk
  if MessageDlg('Desea Generar las P�lizas de estas facturas o recalcularlas?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    btnRecalcularTodo.Enabled := false;
    btnActualizarListaPolizas.Click;
    QueryPolizas.First;
    while not QueryPolizas.Eof do
    begin
      recalculapoliza;
      QueryPolizas.Next;
    end;
    showmessage('Proceso Completado');
    btnActualizarListaPolizas.Click;
    btnRecalcularTodo.Enabled := True;
  end;

end;

procedure TLiquidacionEmpaque.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePageIndex = 3 then
  begin
    Height := 378;
    Width := 750;
  end
  else
  begin
    Height := 526;
    Width := 973;
  end;
end;

procedure TLiquidacionEmpaque.QueryGastosExtraAfterClose(DataSet: TDataSet);
begin
  gasto_extra_id := 0;
end;

procedure TLiquidacionEmpaque.QueryPolizasAfterOpen(DataSet: TDataSet);
begin
  QueryPolizas.FetchAll;
  lblPolizas.Caption := inttostr(QueryPolizas.RecordCount)+' Facturas';
end;

{$endregion}

{$region 'Cajas de Texto Eventos Change'}
procedure TLiquidacionEmpaque.txtBusquedaFacturasChange(Sender: TObject);
var
  i:integer;
  fecha_inicio_polizas_str,fecha_fin_polizas_str:string;
begin
  fecha_inicio_polizas_str := FormatDateTime('mm/dd/yyyy', dtpFechaInicioFacturas.Date);
  fecha_fin_polizas_str := FormatDateTime('mm/dd/yyyy', dtpFechaFinFacturas.Date);
  condicion_fechas := ' and fecha between '''+fecha_inicio_polizas_str+''' and '''+fecha_fin_polizas_str+''' ';
  Querypolizas.sql.Text := ' select * from ('+
                  ' Select dve.docto_ve_id,'+
                         ' dve.folio,'+
                         ' dve.fecha,'+
                         ' cl.nombre as cliente,'+
                         ' dve.importe_neto,'+
                         ' (select first 1 poliza from doctos_co dc join doctos_co_det dcd on dcd.docto_co_id = dc.docto_co_id'+
                          ' where estatus in (''P'',''N'') and dcd.refer = dve.folio) as poliza,'+
                          ' /**********************DEFINE SI CONTIENE O NO CONTIENE FACTURAS CERRADAS*******************/'+
                         ' (SELECT  iif(position (''x'' in list(coalesce(LIB.cerrada,''x'')))>0,''S'',''N'')'+
                          ' FROM doctos_ve dv join'+
                          ' doctos_ve_det dvd on dvd.docto_ve_id = dv.docto_ve_id  join'+
                          ' articulos a on a.articulo_id = dvd.articulo_id join'+
                          ' desglose_en_discretos_ve ded on ded.docto_ve_det_id = dvd.docto_ve_det_id  join'+
                          ' articulos_discretos ad on ad.art_discreto_id = ded.art_discreto_id  join'+
                          ' desglose_en_discretos_cm dedc on dedc.art_discreto_id = ad.art_discreto_id  join'+
                          ' doctos_cm_det dcd on dcd.docto_cm_det_id = dedc.docto_cm_det_id join'+
                          ' doctos_cm dc on dc.docto_cm_id = dcd.docto_cm_id left join'+
                          ' doctos_cm_ligas dl on dl.docto_cm_fte_id = dc.docto_cm_id left join'+
                          ' doctos_cm com on com.docto_cm_id = dl.docto_cm_dest_id left join'+
                          ' libres_com_cm lib on lib.docto_cm_id = com.docto_cm_id left join'+
                          ' libres_rec_cm lre on lre.docto_cm_id = dc.docto_cm_id'+
                          ' where dv.docto_ve_id = dve.docto_ve_id'+
                          ' and dc.estatus in (''P'',''F'')) as rec_pend'+
                         ' /***********************TERMINA CON COMPRAS FACTURADAS*********************/'+
                  ' from doctos_ve dve join'+
                  ' clientes cl on cl.cliente_id=dve.cliente_id'+
                  ' where dve.tipo_docto=''F'' and dve.estatus=''N'' '+
                  ' order by folio ) where (folio like ''%'+txtBusquedaFacturas.Text+'%'' or'+
                  ' cliente like ''%'+txtBusquedaFacturas.Text+'%'' or poliza like ''%'+txtBusquedaFacturas.Text+'%'')'+
                  ' '+condicion_fechas + condicion_mostrar_facturas;


  QueryPolizas.Open;
  for i := 0 to GridPolizas.Columns.Count - 1 do
  begin
    GridPolizas.Columns[i].Width := 120
  end;
  GridPolizas.Columns[0].Visible := False;  

end;

procedure TLiquidacionEmpaque.txtBusquedaRecepcionesChange(Sender: TObject);
var
  i:integer;
begin
  QueryRecosteo.SQL.Text := ' select * from(SELECT  DC.DOCTO_CM_ID,'+
                            ' dc.folio,'+
                            ' dc.folio_prov,'+
                            ' p.nombre as proveedor,'+
                            ' dc.fecha,'+
                            ' (select first 1 ad.clave from desglose_en_discretos_cm dg join'+
                            ' articulos_discretos ad on ad.art_discreto_id=dg.art_discreto_id'+
                            ' where dg.docto_cm_det_id = (select first 1 docto_cm_det_id from doctos_cm_det where docto_cm_id = dc.docto_cm_id))as lote,'+
                            ' dc.importe_neto,'+
                            ' dc.recalculada'+
                    ' FROM doctos_cm dc join'+
                    ' proveedores p on p.proveedor_id = dc.proveedor_id left join '+
                    ' doctos_cm_ligas dcl on dcl.docto_cm_fte_id = dc.docto_cm_id left join'+
                    ' doctos_cm com on com.docto_cm_id = dcl.docto_cm_dest_id left join'+
                    ' libres_com_cm dpc on dpc.docto_cm_id = com.docto_cm_id'+
                    ' where dc.tipo_docto = ''R'' and dc.estatus in(''P'',''F'')  '+condicion_mostrar_recalc+' )'+
                    ' where (folio like ''%'+txtBusquedaRecepciones.Text+'%'' or folio_prov like ''%'+txtBusquedaRecepciones.Text+'%'''+
                    ' or proveedor like ''%'+txtBusquedaRecepciones.Text+'%'' or lote like ''%'+txtBusquedaRecepciones.Text+'%'') order by folio';
  QueryRecosteo.Open;
  for i := 0 to GridRecosteo.Columns.Count - 1 do
  begin
    GridRecosteo.Columns[i].Width := 120;
  end;
  GridRecosteo.Columns[0].Visible := False;
  GridRecosteo.Columns[7].Width := 40;
end;

procedure TLiquidacionEmpaque.txtBusquedaReporteChange(Sender: TObject);
var
  i: integer;
begin
  Query.SQL.Text := ' select * from(SELECT  DC.DOCTO_CM_ID,'+
                            ' dc.folio,'+
                            ' dc.folio_prov,'+
                            ' p.nombre as proveedor,'+
                            ' dc.fecha,'+
                            ' (select first 1 ad.clave from desglose_en_discretos_cm dg join'+
                            ' articulos_discretos ad on ad.art_discreto_id=dg.art_discreto_id'+
                            ' where dg.docto_cm_det_id = (select first 1 docto_cm_det_id from doctos_cm_det where docto_cm_id = dc.docto_cm_id))as lote,'+
                            ' dc.importe_neto,'+
                            ' dc.recalculada'+
                    ' FROM doctos_cm dc join'+
                    ' proveedores p on p.proveedor_id = dc.proveedor_id left join '+
                    ' doctos_cm_ligas dcl on dcl.docto_cm_fte_id = dc.docto_cm_id left join'+
                    ' doctos_cm com on com.docto_cm_id = dcl.docto_cm_dest_id left join'+
                    ' libres_com_cm dpc on dpc.docto_cm_id = com.docto_cm_id'+
                    ' where dc.tipo_docto = ''R'' and dc.estatus in(''P'',''F'')  '+condicion_mostrar_liquidacion+')'+
                    ' where (folio like ''%'+txtBusquedaReporte.Text+'%'' or folio_prov like ''%'+txtBusquedaReporte.Text+'%'''+
                    ' or proveedor like ''%'+txtBusquedaReporte.Text+'%'' or lote like ''%'+txtBusquedaReporte.Text+'%'') order by folio';
  Query.Open;


  for i := 0 to GridReporte.Columns.Count - 1 do
  begin
    GridReporte.Columns[i].Width := 120
  end;
  GridReporte.Columns[0].Visible := False;
  GridRecosteo.Columns[7].Width := 40;
end;


{$endregion}

{$region 'Utilidad por viaje'}
procedure TLiquidacionEmpaque.btnCalcularUtilidadViajeClick(Sender: TObject);
var
  orden_compra,num_compra,proveedor,folio_prov,status,query_insert: string;

  importe_neto,nc,factmennc,saldo_cargo,flete,otros_gastos,ventas,
  existencias,utilidad,utilidad_pctje,costo_cerrado,
  costo_x_caja_flete,costo_viajes_abiertos,
  costo_viajes_cerrados_con_exis,costo_viajes_cerrados_sin_exis,
  utilidad_antes_nc_fyc,dscto_venta,dscto_compras,
  ut_final_antes_gastos_fletes,unidades_vendidas,cajas_recibidas,
  ut_x_caja_importe,ut_x_caja_pctje,comision,factor_comision:double;

  rec_id,viaje,eliminados:integer;
begin
  {ConexionDetallado.ExecSQL('delete from sic_utilidad');}
  if cbCalculoTotal.Checked
  then ConexionDetallado.ExecSQL('delete from sic_utilidad')
  else ConexionDetallado.ExecSQL('delete from sic_utilidad where status=''ABIERTO'' or existencia>0');

  Conexion.StartTransaction;
  try
    QueryBaseUtilidad.sql.Text := ' select orden_compra,'+
                                  ' num_compra,'+
                                  ' proveedor,'+
                                  ' folio_prov,'+
                                  ' importe_neto,'+
                                  ' sum(nc) as nc,'+
                                  ' (importe_neto-sum(nc))as factmennc,'+
                                  ' (select saldo_cargo from saldo_cargo_cp_s(cargo_id,current_date,0)) as saldo_cargo,'+
                                  ' flete,'+
                                  ' otros_gastos,'+
                                  ' (case cerrada'+
                                      ' when ''S'' then ''CERRADO'''+
                                      ' ELSE ''ABIERTO'''+
                                   ' end) as status,'+
                                   ' rec_id'+
                          ' from('+
                                  ' select  max(rec.docto_cm_id) as rec_id,'+
                                          ' max(com.folio) as num_compra,'+
                                          ' cargo.docto_cp_id as cargo_id,'+
                                          ' rec.folio as orden_compra,'+
                                          ' p.nombre as proveedor,'+
                                          ' com.folio_prov,'+
                                          ' max(rec.importe_neto) as importe_neto,'+
                                          ' (iif( nc.concepto_cp_id = 60,sum(idnc.importe+idnc.impuesto),0)) as nc,'+
                                          ' max(lrec.flete) as flete,'+
                                          ' max(lcom.cerrada) as cerrada,'+
                                          ' max(lrec.otros_gastos) as otros_gastos'+
                                  ' from'+
                                  ' doctos_cm rec left join'+
                                  ' doctos_cm_ligas dcl on dcl.docto_cm_fte_id=rec.docto_cm_id left join'+
                                  ' doctos_cm com on com.docto_cm_id = dcl.docto_cm_dest_id left join'+
                                  ' proveedores p on p.proveedor_id = rec.proveedor_id left join'+
                                  ' doctos_entre_sis des on des.docto_fte_id = com.docto_cm_id left join'+
                                  ' doctos_cp cargo on cargo.docto_cp_id = des.docto_dest_id left join'+
                                  ' importes_doctos_cp idcp on idcp.docto_cp_acr_id = cargo.docto_cp_id left join'+
                                  ' doctos_cp nc on nc.docto_cp_id = idcp.docto_cp_id left join'+
                                  ' importes_doctos_cp idnc on idnc.docto_cp_id = nc.docto_cp_id left join'+
                                  ' libres_rec_cm lrec on lrec.docto_cm_id = rec.docto_cm_id left join'+
                                  ' libres_com_cm lcom on lcom.docto_cm_id = com.docto_cm_id'+
                                  ' where rec.tipo_docto = ''R'' and rec.estatus in (''P'',''F'') and (com.tipo_docto = ''C'' or (com.tipo_docto is null) )'+
                                  ' group by rec.folio, p.nombre,com.folio_prov,nc.concepto_cp_id,cargo_id'+
                          ' order by rec.folio)'+
                          ' group by orden_compra,proveedor, folio_prov,importe_neto,cargo_id,'+
                                  ' flete,cerrada,rec_id,num_compra,otros_gastos';
    QueryBaseUtilidad.Open;
    QueryBaseUtilidad.First;
    QueryBaseUtilidad.FetchAll;
    gaProgress.MaxValue := QueryBaseUtilidad.RecordCount;
    while not QueryBaseUtilidad.Eof do
    begin
      // PROCESO DE CALCULAR LOS CAMPOS RESTANTES
      orden_compra := QueryBaseUtilidad.FieldByName('orden_compra').AsString;
      num_compra := QueryBaseUtilidad.FieldByName('num_compra').AsString;
      proveedor := Querybaseutilidad.FieldByName('proveedor').asstring;
      folio_prov := QueryBaseUtilidad.FieldByName('folio_prov').AsString;
      importe_neto := QueryBaseUtilidad.FieldByName('importe_neto').AsFloat;
      nc := QueryBaseUtilidad.FieldByName('nc').AsFloat;
      factmennc := QueryBaseUtilidad.FieldByName('factmennc').AsFloat;
      saldo_cargo := QueryBaseUtilidad.FieldByName('saldo_cargo').AsFloat;
      otros_gastos := QueryBaseUtilidad.FieldByName('otros_gastos').AsFloat;
      status := Querybaseutilidad.FieldByName('status').AsString;
      rec_id := QueryBaseUtilidad.FieldByName('rec_id').asinteger;
      // VALORA SI YA EXISTE EN LA TABLA PARA HACER EL CALCULO DE NUEVO
      if (Conexion.ExecSQLScalar('select count(*) from sic_utilidad where orden_compra='''+orden_compra+'''')=0) then
      begin
        flete := Conexion.ExecSQLScalar('select coalesce(flete,0) from libres_rec_cm where docto_cm_id = :rec',[rec_id]);
        if flete = 0 then flete := Conexion.ExecSQLScalar('select coalesce(otros_gastos,0) from doctos_cm where docto_cm_id = :rec',[rec_id]);

        comision := Conexion.ExecSQLScalar('select comision from libres_rec_cm where docto_cm_id=:rec',[rec_id]);
        factor_comision := 1-(comision/100);
        viaje := strtoint(GetnumFolio(orden_compra));

        {unidades_vendidas := Conexion.ExecSQLScalar(' select  coalesce(sum(dvd.unidades),0) as total'+
              ' from'+
              ' doctos_ve dv join'+
              ' doctos_ve_det dvd on dvd.docto_ve_id = dv.docto_ve_id join'+
              ' articulos a on a.articulo_id = dvd.articulo_id left join'+
              ' libres_rem_ve lrem on lrem.docto_ve_id = dv.docto_ve_id left join'+
              ' vendedores ve on ve.vendedor_id = dv.vendedor_id join'+
              ' clientes cl on cl.cliente_id = dv.cliente_id join'+
              ' desglose_en_discretos_ve dedv on dedv.docto_ve_det_id = dvd.docto_ve_det_id join'+
              ' articulos_discretos ad on ad.art_discreto_id = dedv.art_discreto_id'+
              ' where dv.tipo_docto = ''R'' and dv.estatus in(''P'',''F'') and ad.clave =:clave',[viaje]);}
        unidades_vendidas := Conexion.ExecSQLScalar('select coalesce(sum(unidades_vendidas),0) FROM SIC_LIQUIDACIONES '+
        																						' WHERE VIAJE = :VIAJE',[viaje]);
        ventas := Conexion.ExecSQLScalar('select coalesce(sum(importe_ventas),0) FROM SIC_LIQUIDACIONES '+
        																						' WHERE VIAJE = :VIAJE',[viaje]);
        existencias := Conexion.ExecSQLScalar('select coalesce(sum(existencia),0) FROM SIC_LIQUIDACIONES '+
        																						' WHERE VIAJE = :VIAJE',[viaje]);

        {ventas := Conexion.ExecSQLScalar(' SELECT coalesce(SUM(TOTAL),0)'+
               ' FROM('+
              ' select'+
                      ' q1.nombre,'+
                      ' q1.entrada,'+
                      ' q1.existencia,'+
                      ' (case q1.naturaleza'+
                       ' when ''S'' then sum(q1.vendidas)'+
                       ' else 0'+
                       ' end) as vendidas,'+
                       ' q1.naturaleza,'+
                       ' max(q1.docto_cm_id) as docto_cm_id,'+
                   ' coalesce(sum(q1.precio_total_neto - q1.dev_precio_total_neto),'+
                              ' sum(q1.precio_total_neto),0) AS TOTAL'+
              ' from ('+
                  ' select distinct a.nombre,'+
                   ' dcd.unidades as entrada,'+
                   ' (select sum(lid.existencia) from almacenes al left join'+
                      ' lista_discretos(a.articulo_id,al.almacen_id,current_date) lid on 1=1'+
                      ' where lid.art_discreto_id = ad.art_discreto_id) as existencia,'+
                   ' (case coalesce(dedv.unidades,0)'+
                   '   when 0 then 0'+
                   '   else aux.unidades'+
                   '   end) as vendidas,'+
                   ' ci.naturaleza,'+
                   ' (dvd.precio_unitario*dedv.unidades) as precio_total_neto,'+
                   ' dev_detalles.precio_total_neto as dev_precio_total_neto,'+
                   ' dvd.docto_ve_det_id,'+
                   ' dc.docto_cm_id'+
                   ' from articulos a LEFT join'+
                   ' doctos_cm_det dcd on dcd.articulo_id=a.articulo_id inner join'+
                   ' doctos_cm dc on dc.docto_cm_id=dcd.docto_cm_id inner join'+
                   ' articulos_discretos ad on ad.articulo_id=a.articulo_id left join'+
                   ' auxiliar_discreto(ad.art_discreto_id,19,''01/01/2000'',current_date) aux on aux.art_discreto_id = Ad.art_discreto_id join'+
                   ' conceptos_in ci on ci.nombre_abrev = aux.nombre_concep left join'+
                   ' lista_discretos(a.articulo_id,19,current_date) ld on ld.art_discreto_id = ad.art_discreto_id left join'+
                   ' doctos_ve  dv on dv.folio = aux.folio left join'+
                   ' doctos_ve_det dvd on ((dvd.docto_ve_id = dv.docto_ve_id and (dvd.articulo_id=a.articulo_id) ))left join'+
                   ' desglose_en_discretos_ve dedv on (dedv.docto_ve_det_id = dvd.docto_ve_det_id and dedv.unidades = aux.unidades) left join'+
                   ' doctos_ve_ligas dvl on dvl.docto_ve_fte_id = dv.docto_ve_id left join'+
                   ' doctos_ve fact on fact.docto_ve_id = dvl.docto_ve_dest_id left join'+
                   ' doctos_ve_ligas dvl2 on dvl2.docto_ve_fte_id = fact.docto_ve_id left join'+
                   ' doctos_ve devolucion on devolucion.docto_ve_id = dvl2.docto_ve_dest_id left join'+
                   ' doctos_ve_det dev_detalles on (dev_detalles.docto_ve_id = devolucion.docto_ve_id AND DEV_detalles.articulo_id = a.articulo_id)'+
                   ' where  dc.docto_cm_id =:REC_ID'+
                   ' and ld.art_discreto_id = (select first 1 art_discreto_id From desglose_en_discretos_cm where docto_cm_det_id=dcd.docto_cm_det_id)'+
                   ' AND CI.tipo <> ''S'' and ((ad.art_discreto_id =dedv.art_discreto_id) or (dedv.art_discreto_id is null))) q1'+
                   ' group by q1.nombre,q1.existencia, q1.entrada, q1.naturaleza ) pr'+
              ' where (vendidas + existencia = entrada)',[rec_id]);

        existencias := Conexion.ExecSQLScalar(' SELECT coalesce(SUM(existencia),0)'+
               ' FROM('+
              ' select'+
                      ' q1.nombre,'+
                      ' q1.entrada,'+
                      ' q1.existencia,'+
                      ' (case q1.naturaleza'+
                       ' when ''S'' then sum(q1.vendidas)'+
                       ' else 0'+
                       ' end) as vendidas,'+
                       ' q1.naturaleza,'+
                       ' max(q1.docto_cm_id) as docto_cm_id,'+
                   ' coalesce(sum(q1.precio_total_neto - q1.dev_precio_total_neto),'+
                              ' sum(q1.precio_total_neto),0) AS TOTAL'+
              ' from ('+
                  ' select distinct a.nombre,'+
                   ' dcd.unidades as entrada,'+
                   ' (select sum(lid.existencia) from almacenes al left join'+
                      ' lista_discretos(a.articulo_id,al.almacen_id,current_date) lid on 1=1'+
                      ' where lid.art_discreto_id = ad.art_discreto_id) as existencia,'+
                   '(case coalesce(dedv.unidades,0)'+
                   '   when 0 then 0 '+
                   '   else aux.unidades'+
                   '   end) as vendidas,'+
                   ' ci.naturaleza,'+
                   ' (dvd.precio_unitario*dedv.unidades) as precio_total_neto,'+
                   ' dev_detalles.precio_total_neto as dev_precio_total_neto,'+
                   ' dvd.docto_ve_det_id,'+
                   ' dc.docto_cm_id'+
                   ' from articulos a LEFT join'+
                   ' doctos_cm_det dcd on dcd.articulo_id=a.articulo_id inner join'+
                   ' doctos_cm dc on dc.docto_cm_id=dcd.docto_cm_id inner join'+
                   ' articulos_discretos ad on ad.articulo_id=a.articulo_id left join'+
                   ' auxiliar_discreto(ad.art_discreto_id,19,''01/01/2000'',current_date) aux on aux.art_discreto_id = Ad.art_discreto_id join'+
                   ' conceptos_in ci on ci.nombre_abrev = aux.nombre_concep left join'+
                   ' lista_discretos(a.articulo_id,19,current_date) ld on ld.art_discreto_id = ad.art_discreto_id left join'+
                   ' doctos_ve  dv on dv.folio = aux.folio left join'+
                   ' doctos_ve_det dvd on ((dvd.docto_ve_id = dv.docto_ve_id and (dvd.articulo_id=a.articulo_id) ))left join'+
                   ' desglose_en_discretos_ve dedv on (dedv.docto_ve_det_id = dvd.docto_ve_det_id and dedv.unidades = aux.unidades) left join'+
                   ' doctos_ve_ligas dvl on dvl.docto_ve_fte_id = dv.docto_ve_id left join'+
                   ' doctos_ve fact on fact.docto_ve_id = dvl.docto_ve_dest_id left join'+
                   ' doctos_ve_ligas dvl2 on dvl2.docto_ve_fte_id = fact.docto_ve_id left join'+
                   ' doctos_ve devolucion on devolucion.docto_ve_id = dvl2.docto_ve_dest_id left join'+
                   ' doctos_ve_det dev_detalles on (dev_detalles.docto_ve_id = devolucion.docto_ve_id AND DEV_detalles.articulo_id = a.articulo_id)'+
                   ' where  dc.docto_cm_id =:REC_ID'+
                   ' and ld.art_discreto_id = (select first 1 art_discreto_id From desglose_en_discretos_cm where docto_cm_det_id=dcd.docto_cm_det_id)'+
                   ' AND CI.tipo <> ''S'' and ((ad.art_discreto_id =dedv.art_discreto_id) or (dedv.art_discreto_id is null))) q1'+
                   ' group by q1.nombre,q1.existencia, q1.entrada, q1.naturaleza ) pr'+
              ' where (vendidas + existencia = entrada)',[rec_id]);}
        cajas_recibidas := Conexion.ExecSQLScalar('select sum(unidades) from doctos_cm_det where docto_cm_id = :rec',[rec_id]);
        {***************VALIDA SI LAS VENTAS SON CERO PARA NO CALCULAR LA UTILIDAD COMO NEGATIVA*******}
        {if ventas = 0  then
        begin
          existencias := cajas_recibidas - existencias;
        end; }
        {**************************************TERMINA VALIDACION**************************************}


        costo_cerrado := 0;
        if status = 'CERRADO' then
          costo_cerrado := Conexion.ExecSQLScalar(' select coalesce (sum(total),0) from(select'+
                  ' (CASE coalesce(lc.flete,0)'+
                  ' when 0 then (ld.existencia*did.costo_unitario)'+
                  ' else (ld.existencia*((dcd.precio_unitario)+(lc.flete / (select sum(unidades) from doctos_cm_det  where docto_cm_id=dcd.docto_cm_id))))'+
                  ' end) as total'+
                  ' from articulos ar  join'+
                  ' almacenes a on 1=1 left join'+
                  ' lista_discretos(ar.articulo_id,a.almacen_id,current_date) ld on ld.articulo_id = ar.articulo_id join'+
                  ' desglose_en_discretos_cm dedc on dedc.art_discreto_id = ld.art_discreto_id join'+
                  ' doctos_cm_det dcd on dcd.docto_cm_det_id = dedc.docto_cm_det_id join'+
                  ' doctos_cm dc on dc.docto_cm_id = dcd.docto_cm_id left join'+
                  ' libres_rec_cm lc on lc.docto_cm_id = dc.docto_cm_id left join'+
                  ' doctos_entre_sis des on des.docto_fte_id = dc.docto_cm_id left join'+
                  ' doctos_in di on di.docto_in_id = des.docto_dest_id left join'+
                  ' doctos_in_det did on (did.docto_in_id=di.docto_in_id) AND (DID.articulo_id=DCD.articulo_id)'+
                  ' where ld.existencia > 0 and dc.tipo_docto = ''R'' and dc.estatus<>''C'' and dc.docto_cm_id = :rec_id)',[rec_id]);

        if unidades_vendidas > cajas_recibidas
        then unidades_vendidas := cajas_recibidas;

        costo_x_caja_flete := flete / cajas_recibidas;

        costo_viajes_abiertos := 0;
        costo_viajes_cerrados_con_exis := 0;
        costo_viajes_cerrados_sin_exis := 0;
        if status='ABIERTO' then
        begin
          costo_viajes_abiertos := (ventas*factor_comision)+(unidades_vendidas*costo_x_caja_flete);
        end
        else
        begin
          if existencias > 0
          then costo_viajes_cerrados_con_exis := ((importe_neto+flete-costo_cerrado))
          else costo_viajes_cerrados_sin_exis := (importe_neto+flete)
        end;

        dscto_venta := Conexion.ExecSQLScalar('select coalesce(descuento_venta,0) from libres_rec_cm where docto_cm_id=:did',[rec_id]);
        dscto_compras := Conexion.ExecSQLScalar('select coalesce(descuento_compra,0) from libres_rec_cm where docto_cm_id=:did',[rec_id]);
        utilidad_antes_nc_fyc := ventas - costo_viajes_abiertos - costo_viajes_cerrados_con_exis - costo_viajes_cerrados_sin_exis;
        ut_final_antes_gastos_fletes := utilidad_antes_nc_fyc - dscto_venta + dscto_compras;

        if unidades_vendidas > 0 then ut_x_caja_importe := ut_final_antes_gastos_fletes / unidades_vendidas else ut_x_caja_importe := 0;
        if ventas > 0 then ut_x_caja_pctje := (ut_final_antes_gastos_fletes / ventas) * 100 else ut_x_caja_pctje := 0;

        //**************MODIFICACION DE NOTAS DE CREDITO PARA IGUALARLAS A DSCTO SOBRE COMPRA****************
        nc := dscto_compras;
        factmennc := importe_neto - nc;

        query_insert := 'insert into sic_utilidad  (ORDEN_COMPRA,'+
                      ' NUM_COMPRA,'+
                      ' PROVEEDOR,'+
                      ' FOLIO_PROV,'+
                      ' IMPORTE_NETO,'+
                      ' NC,'+
                      ' FACTMENNC,'+
                      ' SALDO_CARGO,'+
                      ' FLETE,'+
                      ' OTROS_GASTOS,'+
                      ' STATUS,'+
                      ' VENTAS,'+
                      ' EXISTENCIA,'+
                      ' COSTO_CERRADO,'+
                      ' COSTO_X_CAJA_FLETE,'+
                      ' COSTO_VIAJES_ABIERTOS,'+
                      ' COSTO_VIAJES_CERRADOS_CON_EXIS,'+
                      ' COSTO_VIAJES_CERRADOS_SIN_EXIS,'+
                      ' UTILIDAD_ANTES_NC_FYC,'+
                      ' DSCTO_VENTA,'+
                      ' DSCTO_COMPRAS,'+
                      ' UT_FINAL_ANTES_GASTOS_FLETES,'+
                      ' UNIDADES_VENDIDAS,'+
                      ' CAJAS_RECIBIDAS,'+
                      ' UT_X_CAJA_IMPORTE,'+
                      ' UT_X_CAJA_PCTJE,'+
                      ' FECHAHORA_CALCULO)'+
                      ' values('''+orden_compra+''','''+num_compra+''','''+proveedor+''','''+folio_prov+''','+
                        ' '+FloatToStr(importe_neto)+','+FloatToStr(nc)+','+FloatToStr(factmennc)+','+FloatToStr(saldo_cargo)+','+
                        ' '+FloatToStr(flete)+','+FloatToStr(otros_gastos)+','''+status+''','+floattostr(ventas)+','+
                        ' '+FloatToStr(existencias)+','+FloatToStr(costo_cerrado)+','+Floattostr(costo_x_caja_flete)+','+
                        ' '+Floattostr(costo_viajes_abiertos)+','+floattostr(costo_viajes_cerrados_con_exis)+','+FloatToStr(costo_viajes_cerrados_sin_exis)+','+
                        ' '+Floattostr(utilidad_antes_nc_fyc)+','+FloatToStr(dscto_venta)+','+FloatToStr(dscto_compras)+','+
                        ' '+Floattostr(ut_final_antes_gastos_fletes)+','+FloatToStr(unidades_vendidas)+','+FloatToStr(cajas_recibidas)+','+
                        ' '+Floattostr(ut_x_caja_importe)+','+FloatToStr(ut_x_caja_pctje)+',current_timestamp) returning orden_compra';
        orden_compra := ConexionDetallado.ExecSQLScalar(query_insert);
      end;
      gaProgress.Progress := gaProgress.Progress+1;

      QueryBaseUtilidad.Next;
    end;

  Except
    on E: Exception do
    begin
        ShowMessage('Error. '+E.Message);
    end;
  end;
  ShowMessage('correcto');
  gaProgress.Progress := 0;

end;


procedure TLiquidacionEmpaque.btnCancelarGastoClick(Sender: TObject);
begin
  QueryGastosExtra.Open;
  GridGastosExtra.Columns[0].Visible := false;
  GridGastosExtra.Columns[1].Width := 50;
  GridGastosExtra.Columns[1].Width := 50;
  GridGastosExtra.Columns[1].Width := 50;
  GridGastosExtra.Columns[1].Width := 150;
  btnNuevoGasto.Enabled := true;
  cbCuentasConta.KeyValue := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where nombre=:nom',[QueryGastosExtra.FieldByName('concepto').AsString]);
  txtImporteGasto.text := QueryGastosExtra.FieldByName('importe').AsString;
  dtpFechaGasto.Date := QueryGastosExtra.FieldByName('fecha').AsDateTime;
  txtdescGasto.Text := QueryGastosExtra.FieldByName('descripcion').asstring;
  gasto_extra_id := QueryGastosExtra.FieldByName('gasto_id').asinteger;
end;

procedure TLiquidacionEmpaque.btnConcentrarInfoClick(Sender: TObject);
var
	viaje : string;
  res : variant;
begin
	Query.first;
  Query.FetchAll;
  Gauge1.MaxValue := Query.RecordCount;
  while not Query.Eof do
  begin
  	viaje := inttostr(strtoint(GetnumFolio(Query.FieldByName('folio').Asstring)));

    Conexion.ExecSQL('execute procedure sic_actualiza_viaje('''+viaje+''')');
		conexion.Commit;
    Query.Next;
    Gauge1.Progress := Gauge1.Progress + 1;
  end;
  Query.First;
  ShowMessage('listo');
  Gauge1.Progress := 0;
end;

procedure TLiquidacionEmpaque.btnGuardarGastoClick(Sender: TObject);
var
  fecha_str:string;
begin
  fecha_str := FormatDateTime('mm/dd/yyyy', dtpFechaGasto.Date);
  if gasto_extra_id = 0 then
  begin
    Conexion.ExecSql('insert into sic_gastos_extra values(null,:cuenta, '''+fecha_str+''', :importe, :descrip)',[cbCuentasConta.KeyValue,strtofloat(txtImporteGasto.Text),txtdescGasto.Text]);
    Conexion.Commit;
    ShowMessage('Gasto Guardado Correctamente');
  end
  else
  begin
    Conexion.ExecSql('UPDATE sic_gastos_extra SET cuenta=:cuenta, fecha='''+fecha_str+''', importe = :importe, descripcion=:descrip where gasto_id = :gid',[cbCuentasConta.KeyValue,strtofloat(txtImporteGasto.Text),txtdescGasto.Text,gasto_extra_id]);
    Conexion.Commit;
    ShowMessage('Gasto Actualizado Correctamente');
  end;
  cbCuentasConta.SetFocus;
  QueryGastosExtra.Close;
  QueryGastosExtra.OPEN;
  GridGastosExtra.Columns[0].Visible := false;
  GridGastosExtra.Columns[1].Width := 50;
  GridGastosExtra.Columns[1].Width := 50;
  GridGastosExtra.Columns[1].Width := 50;
  GridGastosExtra.Columns[1].Width := 150;
  btnNuevoGasto.Enabled := TRUE;
end;

procedure TLiquidacionEmpaque.btnNuevoGastoClick(Sender: TObject);
begin
  QueryGastosExtra.Close;
  QueryCuentas.First;
  cbCuentasConta.KeyValue := QueryCuentas.FieldByName('cta').Asstring;
  txtImporteGasto.Clear;
  dtpFechaGasto.date := date;
  txtdescGasto.Clear;

  btnNuevoGasto.Enabled := false;
  btnCancelarGasto.Enabled := true;
  btnGuardarGasto.Enabled := true;
end;

{$endregion}

{$REGION 'Fletes'}
procedure TLiquidacionEmpaque.Button1Click(Sender: TObject);
begin
  Conexion.ExecSQL('update libres_rec_cm set flete=:flete where docto_cm_id=:did',[txtFlete.Text,recepcion_id]);
  Conexion.Commit;
  Conexion.ExecSQL('update libres_rec_cm set descuento_venta=:dscto where docto_cm_id=:did',[txtDscto.Text,recepcion_id]);
  Conexion.Commit;
  //************************** ACTUALIZA EL CARGO EN CUENTAS POR PAGAR*****************
  Conexion.ExecSQL('update libres_rec_cm set descuento_compra=:dscto where docto_cm_id=:did',[txtDsctoCompra.Text,recepcion_id]);
  Conexion.Commit;

  ShowMessage('Datos Guardados Correctamente');
  txtFlete.Clear;
  txtDscto.Clear;
  txtRecepcionFolio.Clear;
  txtRecepcionFolio.SetFocus;
end;




procedure TLiquidacionEmpaque.txtRecepcionFolioExit(Sender: TObject);
var
  consecutivo : integer;
  serie,folio_str  : string;

begin
  if txtRecepcionFolio.text <> '' then
  begin
    serie := GetSerie(txtRecepcionFolio.Text);
    consecutivo := strtoint(GetnumFolio(txtRecepcionFolio.Text));
    folio_str := serie + Format('%.*d',[9-length(serie),(consecutivo)]);
    recepcion_id := Conexion.ExecSQLScalar('select coalesce(docto_cm_id,0) from doctos_cm where folio = :folio and estatus in(''P'',''F'')',[folio_str]);

    //*********************************** SACA EL ID DEL CARGO CORRESPONDIENTE ***********************


    if recepcion_id > 0  then
    begin
      txtFlete.Text := Conexion.ExecSQLScalar('select coalesce(flete,0) from libres_rec_cm where docto_cm_id=:did',[recepcion_id]);
      txtDscto.Text := Conexion.ExecSQLScalar('select coalesce(descuento_venta,0) from libres_rec_cm where docto_cm_id=:did',[recepcion_id]);
      txtDsctoCompra.Text := Conexion.ExecSQLScalar('select coalesce(descuento_compra,0) from libres_rec_cm where docto_cm_id=:did',[recepcion_id]);
    end
    else
    begin
      showmessage('No se encontro la Recepci�n, o se encuentra Cancelada.');
      txtRecepcionFolio.SetFocus;
      txtRecepcionFolio.selectall;
    end;
  end;
end;
{$ENDREGION}

end.
