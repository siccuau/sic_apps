unit UPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Vcl.ComCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, Data.DB, FireDAC.Comp.Client, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, UAgregaListaPrecio, UImportaExistencias, UCambioCostos, UAgregaClavesAlternas, UCotizacionBusqueda,
  UCambioPrecios,UReporteFacturacionGeneral, FireDAC.VCLUI.Wait;

type
  TPrincipal = class(TForm)
    menu: TMainMenu;
    Articulos1: TMenuItem;
    Clientes1: TMenuItem;
    Documentos1: TMenuItem;
    mnu_listaprecios: TMenuItem;
    mnu_cambiocostos: TMenuItem;
    mnu_suspendeclientes: TMenuItem;
    StatusBar1: TStatusBar;
    Conexion: TFDConnection;
    mnu_importarexistencias: TMenuItem;
    mnuAgregarClavesAlternas: TMenuItem;
    CrearcotizacionporBusqueda1: TMenuItem;
    CambiarPreciosporCosto1: TMenuItem;
    ReportedeFacturacion1: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mnu_listapreciosClick(Sender: TObject);
    procedure mnu_importarexistenciasClick(Sender: TObject);
    procedure mnu_cambiocostosClick(Sender: TObject);
    procedure mnuAgregarClavesAlternasClick(Sender: TObject);
    procedure CrearcotizacionporBusqueda1Click(Sender: TObject);
    procedure CambiarPreciosporCosto1Click(Sender: TObject);
    procedure ReportedeFacturacion1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Principal: TPrincipal;

implementation

{$R *.dfm}

procedure TPrincipal.CambiarPreciosporCosto1Click(Sender: TObject);
var
  FormCambioPrecios:TCambioPrecios;
begin
  FormCambioPrecios := TCambioPrecios.Create(nil);
  FormCambioPrecios.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormCambioPrecios.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormCambioPrecios.Conexion.Params.Values['User_name'] := Conexion.Params.Values['User_name'];
  FormCambioPrecios.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormCambioPrecios.StatusBar1.Panels[0].Text := StatusBar1.Panels[0].Text;
  FormCambioPrecios.ShowModal;

end;

procedure TPrincipal.CrearcotizacionporBusqueda1Click(Sender: TObject);
var
  FormCotizacionBusqueda : TCotizacionBusqueda;
begin
   FormCotizacionBusqueda := TCotizacionBusqueda.Create(nil);
  FormCotizacionBusqueda.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormCotizacionBusqueda.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormCotizacionBusqueda.Conexion.Params.Values['User_name'] := Conexion.Params.Values['User_name'];
  FormCotizacionBusqueda.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
{  FormCreaCotizacionBusqueda.StatusBar1.Panels[0].Text := StatusBar1.Panels[0].Text;}
  FormCotizacionBusqueda.ShowModal;
end;

procedure TPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Application.Terminate;
end;

procedure TPrincipal.mnuAgregarClavesAlternasClick(Sender: TObject);
var
  FormAgregaClavesAlternas : TAgregaClavesAlternas;
begin
   FormAgregaClavesAlternas := TAgregaClavesAlternas.Create(nil);
  FormAgregaClavesAlternas.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormAgregaClavesAlternas.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormAgregaClavesAlternas.Conexion.Params.Values['User_name'] := Conexion.Params.Values['User_name'];
  FormAgregaClavesAlternas.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormAgregaClavesAlternas.StatusBar1.Panels[0].Text := StatusBar1.Panels[0].Text;
  FormAgregaClavesAlternas.ShowModal;
end;

procedure TPrincipal.mnu_cambiocostosClick(Sender: TObject);
var
  FormCambioCostos : TCambioCostos;
begin
  FormCambioCostos := TCambioCostos.Create(nil);
  FormCambioCostos.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormCambioCostos.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormCambioCostos.Conexion.Params.Values['User_name'] := Conexion.Params.Values['User_name'];
  FormCambioCostos.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormCambioCostos.Show;
end;

procedure TPrincipal.mnu_importarexistenciasClick(Sender: TObject);
var
  FormImportaExistencias : TImportaExistencias;
begin
  FormImportaExistencias := TImportaExistencias.Create(nil);
  FormImportaExistencias.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormImportaExistencias.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormImportaExistencias.Conexion.Params.Values['User_name'] := Conexion.Params.Values['User_name'];
  FormImportaExistencias.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormImportaExistencias.StatusBar1.Panels[0].Text := StatusBar1.Panels[0].Text;
  FormImportaExistencias.ShowModal;
end;

procedure TPrincipal.mnu_listapreciosClick(Sender: TObject);
var
  FormAgregaListaPrecios : TAgregaListaPrecio;
begin
  FormAgregaListaPrecios := TAgregaListaPrecio.Create(nil);
  FormAgregaListaPrecios.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormAgregaListaPrecios.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormAgregaListaPrecios.Conexion.Params.Values['UserName'] := Conexion.Params.Values['UserName'];
  FormAgregaListaPrecios.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormAgregaListaPrecios.Show;
end;

procedure TPrincipal.ReportedeFacturacion1Click(Sender: TObject);
var
  FormReporteFacturacion: TReporteFacturacionGeneral;
begin
  FormReporteFacturacion := TReporteFacturacionGeneral.Create(nil);
  FormReporteFacturacion.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormReporteFacturacion.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormReporteFacturacion.Conexion.Params.Values['UserName'] := Conexion.Params.Values['UserName'];
  FormReporteFacturacion.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormReporteFacturacion.Show;
end;

end.
