unit UReporteCPP;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, Data.DB, FireDAC.Comp.Client, Vcl.ComCtrls,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  Vcl.StdCtrls, Vcl.DBCtrls, FireDAC.Comp.DataSet;

type
  TReporteCPP = class(TForm)
    StatusBar1: TStatusBar;
    Conexion: TFDConnection;
    DataSourceEstatus: TDataSource;
    QueryEstatus: TFDQuery;
    cbEstatus: TDBLookupComboBox;
    Label1: TLabel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    txtInteres: TEdit;
    Label3: TLabel;
    ComboBox1: TComboBox;
    Button1: TButton;
    Button2: TButton;
    Label4: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReporteCPP: TReporteCPP;

implementation

{$R *.dfm}

procedure TReporteCPP.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Application.Terminate;
end;

procedure TReporteCPP.FormShow(Sender: TObject);
begin
  QueryEstatus.Open;
  cbEstatus.KeyValue := Conexion.ExecSQLScalar('select first 1 valor_desplegado from listas_atributos la join'+
        ' atributos atr on atr.atributo_id = la.atributo_id'+
        ' where atr.nombre_columna=''ESTATUS'' and atr.clave_objeto =''CLIENTES''' );
end;

end.
