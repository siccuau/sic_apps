unit UImportaRemision;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.FB, FireDAC.Phys.FBDef, Data.DB, FireDAC.Comp.Client,
  Vcl.ComCtrls;

type
  TImportaRemision = class(TForm)
    Button1: TButton;
    StatusBar1: TStatusBar;
    Conexion: TFDConnection;
    OpenDialog1: TOpenDialog;
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ImportaRemision: TImportaRemision;

implementation

{$R *.dfm}

procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ;
begin
   ListOfStrings.Clear;
   ListOfStrings.Delimiter       := Delimiter;
   ListOfStrings.StrictDelimiter := True; // Requires D2006 or newer.
   ListOfStrings.DelimitedText   := Str;
end;

Function getvalue (cad : String; separador : string; index:integer): string;
var
  arr : TStringList;//array of string;
begin
  arr := TstringList.create;
  Split(';',cad,arr);
  Result := arr[index];
End;

procedure TImportaRemision.Button1Click(Sender: TObject);
var
  txt : TextFile;
  text,full_text   : string;
  lista : TStringList;
  rfc_index,fecha_index,index_ini_con,index_fin_con,descripcion_index:integer;
  rfc,folio,descripcion,query_str,clave_cliente,folio_str,clave_articulo:string;
  cliente_id,dir_cli_id,remision_id,cond_pago_id,articulo_id:integer;
  i: Integer;
  unidades,precio_unitario,precio_total_neto:double;
begin
  lista := TStringList.Create;
  text := '';
  full_text := '';
  if OpenDialog1.Execute then
  begin
    // Abre y lee el archivo
    AssignFile(txt, OpenDialog1.FileName);
    Reset(txt);
    while not eof(txt) do
    begin
      ReadLn(txt, text);
      full_text := full_text+Trim(text)+'~';
    end;
    CloseFile(txt);
    //Separa el texto por lineas
    Split('~', full_text, lista) ;
    //Ubica los datos para extraerlos
    rfc_index := lista.IndexOf('->Receptor<-')+1;
    index_ini_con := lista.IndexOf('->Conceptos<-')+1;
    index_fin_con := lista.IndexOf('->Observaciones<-')-1;
    descripcion_index := lista.IndexOf('->Observaciones<-')+1;

    //Extrae los datos del encabezado
    rfc := getvalue(lista[rfc_index],';',0);
    descripcion := lista[descripcion_index];
    folio := getvalue(lista[1],';',1);
    folio_str := Format('%.*d',[9,(strtoint(folio))]);
    cliente_id := Conexion.ExecSQLScalar('select first 1 cliente_id from dirs_clientes where rfc_curp=:rfc',[rfc]);
    clave_cliente := Conexion.ExecSQLScalar('select coalesce(clave_cliente,'''') from get_clave_cli(:cliid)',[cliente_id]);
    dir_cli_id := Conexion.ExecSQLScalar('select dir_cli_id from dirs_clientes where cliente_id=:cliid and es_dir_ppal=''S''',[cliente_id]);
    cond_pago_id := Conexion.ExecSQLScalar('select cond_pago_id from clientes where cliente_id=:cliid',[cliente_id]);

    if Conexion.ExecSQLScalar('select count(*) from doctos_ve where tipo_docto=''R'' and folio =:f',[folio_str])=0 then
    begin
    
      Conexion.StartTransaction;
      try
        if cliente_id <> 0 then
        begin

          //Crea el encabezado
          query_str := 'insert into doctos_ve (docto_ve_id,tipo_docto,folio,fecha,clave_cliente,'+
                           ' cliente_id,dir_cli_id,dir_consig_id,almacen_id,moneda_id,tipo_cambio,'+
                           ' sistema_origen,tipo_dscto,estatus,cond_pago_id,descripcion) values (-1,''R'','''+folio_str+''','+
                           ' current_date,'''+clave_cliente+''','+inttostr(cliente_id)+','+inttostr(dir_cli_id)+','+inttostr(dir_cli_id)+','+
                           ' 19,1,1,''VE'',''P'',''P'','+inttostr(cond_pago_id)+','''+descripcion+''') returning docto_ve_id';
          remision_id := Conexion.ExecSQLScalar(query_str);

          //Crea Conceptos
          for i := index_ini_con to index_fin_con do
          begin
            clave_articulo := getvalue(lista[i],';',0);
            articulo_id := Conexion.ExecSQLScalar('select articulo_id from claves_articulos where clave_articulo=:ca',[clave_articulo]);
            unidades := StrToFloat(getvalue(lista[i],';',2));
            precio_unitario := StrToFloat(getvalue(lista[i],';',3));
            precio_total_neto := StrToFloat(getvalue(lista[i],';',7));
            if articulo_id <> 0 then
            begin
              query_str := 'insert into doctos_ve_det (docto_ve_det_id, docto_ve_id,'+
                         ' clave_articulo,articulo_id,unidades,precio_unitario,precio_total_neto,notas,pctje_dscto,posicion) values(-1,'+
                         ' '+inttostr(remision_id)+','''+clave_articulo+''','+inttostr(articulo_id)+','+FloatToStr(unidades)+
                         ' ,'+FloatToStr(precio_unitario)+','+FloatToStr(precio_total_neto)+','''',0,-1)';
              Conexion.ExecSQL(query_str);
            end;
          end;

          Conexion.ExecSQL('execute procedure CALC_TOTALES_DOCTO_VE(:id,''N'')',[remision_id]);
          Conexion.ExecSQL('execute procedure aplica_docto_ve(:id)',[remision_id]);
          ShowMessage('Remisi�n creada con folio: '+folio);
        end
        else
        begin
          ShowMessage('El cliente con RFC '+rfc+' no esta dado de alta');
        end;
      Except
        Conexion.Rollback;
        ShowMessage('Error en Archivo de texto');
      end;
      Conexion.Commit;
      OpenDialog1.FileName := '';
    end
    else
    begin
      ShowMessage('Ya existe una remisi�n con el folio '+folio);
    end;    
  end;
end;

procedure TImportaRemision.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Application.Terminate;
end;

procedure TImportaRemision.FormCreate(Sender: TObject);
var
  txt : TextFile;
  lista : TStringList;
  text :string;
begin
  lista := TStringList.Create;
  AssignFile(txt, 'config.txt');
  Reset(txt);
  while not eof(txt) do
  begin
    ReadLn(txt, text);
    lista.Add(Text);
  end;

  Conexion.Params.Values['Database'] := lista[0];
  Conexion.Params.Values['Server'] := lista[1];
  Conexion.Params.Values['User_Name'] := lista[2];
  Conexion.Params.Values['Password'] := lista[3];
  Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';

  StatusBar1.Panels.Items[0].Text := lista[0];
  try
    Conexion.Open;
  except
    ShowMessage('Error en archivo de Configuracion');
    Application.Terminate;
  end;
end;

end.
