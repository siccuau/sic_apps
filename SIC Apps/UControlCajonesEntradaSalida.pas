unit UControlCajonesEntradaSalida;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.DBCtrls, Vcl.StdCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef, Vcl.ComCtrls, Data.DB,
  FireDAC.Comp.Client, Vcl.Grids, Vcl.DBGrids, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, Vcl.Menus, UControlCajonesProductores,Ubusqueda, System.Character,
  frxClass, frxDBSet, Vcl.ToolWin, Vcl.ImgList, FireDAC.VCLUI.Wait,
  System.ImageList;

type
  TControlCajonesEntradaSalida = class(TForm)
    lblfolio: TLabel;
    TxtFolio: TEdit;
    cmbProveedor: TDBLookupComboBox;
    lblProveedor: TLabel;
    CmbTipo: TComboBox;
    LblTipo: TLabel;
    CmbClase: TComboBox;
    LblClase: TLabel;
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    LblFecha: TLabel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    cmbProductor: TDBLookupComboBox;
    LblProductor: TLabel;
    GroupChofer: TGroupBox;
    Label1: TLabel;
    txtPlacas: TEdit;
    Label2: TLabel;
    txtChofer: TEdit;
    DSProveedores: TDataSource;
    QueryProveedores: TFDQuery;
    DSProductores: TDataSource;
    QueryProductores: TFDQuery;
    mnuProductores: TPopupMenu;
    mnuItemAbrir: TMenuItem;
    mnuItemNuevo: TMenuItem;
    txtClaveProductor: TEdit;
    datos: TStringGrid;
    gbTotales: TGroupBox;
    txtTotalCajones: TEdit;
    txtTotalPesoBruto: TEdit;
    txtTotalCosto: TEdit;
    txtTotalTara: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    btnGuardar: TButton;
    cmbAlmacenes: TDBLookupComboBox;
    Label7: TLabel;
    DSAlmacenes: TDataSource;
    QueryAlmacenes: TFDQuery;
    dtpFecha: TDateTimePicker;
    Transaction: TFDTransaction;
    txtTotalPesoNeto: TEdit;
    Label8: TLabel;
    txtPromedioporCajon: TEdit;
    Label9: TLabel;
    Reporte: TfrxReport;
    DSReporte: TfrxDBDataset;
    QueryImprimir: TFDQuery;
    ImageList1: TImageList;
    btnRecibo: TButton;
    btnEtiquetas: TButton;
    Etiquetas: TfrxReport;
    DSEtiquetas: TfrxDBDataset;
    QueryEtiquetas: TFDQuery;
    QueryProductos: TFDQuery;
    grid_datos: TDBGrid;
    DSDetalles: TDataSource;
    QueryDetalles: TFDQuery;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure mnuProductoresPopup(Sender: TObject);
    procedure mnuItemAbrirClick(Sender: TObject);
    procedure mnuItemNuevoClick(Sender: TObject);
    procedure actualizar_productores();
    procedure cmbProveedorClick(Sender: TObject);
    procedure cmbProductorClick(Sender: TObject);
    procedure txtClaveProductorExit(Sender: TObject);
    procedure formato_grid();
    procedure datosKeyPress(Sender: TObject; var Key: Char);
    procedure datosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CreaRenglon();
    procedure datosSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure calculaRenglon(row:integer);
    procedure EntradaVacio();
    procedure EntradaManzana();
    procedure SalidaVacio();
    procedure btnGuardarClick(Sender: TObject);
    procedure Totales();
    procedure CmbTipoChange(Sender: TObject);
    procedure LimpiarGrid;
    procedure CmbClaseChange(Sender: TObject);
    procedure EnviarAFormato();
    procedure borrarRenglones();
    procedure DeleteRow(Grid: TStringGrid; ARow: Integer);
    procedure btnReciboClick(Sender: TObject);
    procedure btnEtiquetasClick(Sender: TObject);
    procedure ImprimirEtiquetas();
  private
    { Private declarations }
  public
    { Public declarations }
    docto_id:integer;
    liga_in:string
  end;

var
  ControlCajonesEntradaSalida: TControlCajonesEntradaSalida;

implementation

{$R *.dfm}
{$region 'Funciones'}
function GetSerie(folio : String) : String;
var
  i:integer;
begin
  Result := '';
  for I := 1 to (folio.Length) do
  begin
    if (IsLetter(folio[i]))
    then Result := Result+folio[i]
    else exit;
  end;
end;

function GetNumFolio(folio : String) : String;
var
  i:integer;
begin
  Result := '';
  for I := 1 to (folio.Length) do
  begin
    if (IsNumber(folio[i]))
    then Result := Result+folio[i];
  end;
end;

procedure TControlCajonesEntradaSalida.ImprimirEtiquetas();
var
  Memo:TfrxMemoView;
  folio_str,page_str,folio_viaje:string;
  folio_int,page,consec,viaje:integer;
  ds: TfrxDataSet;
  md: TfrxmasterData;
  query_et:string;
begin
  consec := 1;
  query_et := '';
  if (CmbTipo.Text = 'Entrada') and (CmbClase.text = 'Con Manzana') then
  begin
    folio_viaje := Conexion.ExecSQLScalar('select folio from doctos_cm where docto_cm_id=:did',[docto_id]);
  end
  else
  begin
    folio_viaje := Conexion.ExecSQLScalar('select folio from doctos_in where docto_in_id=:did',[docto_id]);
  end;

  viaje := strtoint(GetNumFolio(folio_viaje));
  QueryProductos.sql.text := ' select  a.nombre,'+
                              ' (select clave_articulo from get_clave_art(a.articulo_id))as clave,'+
                              ' sum(dcd.sic_num_cajones) as cajones'+
                        ' from doctos_cm_det dcd join'+
                        ' articulos a on a.articulo_id = dcd.articulo_id'+
                        ' where dcd.docto_cm_id = '+IntToStr(docto_id)+
                        ' group by a.nombre,a.articulo_id';
  QueryProductos.Open;
  QueryProductos.First;
  while not QueryProductos.Eof do
  begin
    if query_et<>''
    then query_et := query_et +' union ';

    query_et := query_et + ' select a.*,'''+QueryProductos.FieldByName('nombre').AsString+''' as articulo,'+
                            ''''+QueryProductos.FieldByname('clave').asstring+'''as clave,'+
                            ' '+IntToStr(viaje)+' as viaje from'+
                            ' (with recursive n as ('+
                                  ' select '+inttostr(consec)+' as n'+
                                  ' from rdb$database'+
                                  ' union all'+
                                  ' select n.n + 1'+
                                  ' from n'+
                                  ' where n < '+IntToStr(consec+QueryProductos.Fieldbyname('cajones').AsInteger-1)+''+
                                 ' )'+
                            ' select n.n'+
                            ' from n) a';
    consec := consec+QueryProductos.Fieldbyname('cajones').AsInteger;
    QueryProductos.Next;

  end;
  QueryEtiquetas.sql.Text := query_et;

  QueryEtiquetas.open;

  Etiquetas.PrepareReport;
  Memo := Etiquetas.FindObject('txtFecha') as TfrxMemoView;
  Memo.Text := formatdatetime('dddd, dd - mmmm - yyyy',dtpFecha.Date);
  Memo := Etiquetas.FindObject('txtAlmacen') as TfrxMemoView;
  Memo.Text := cmbAlmacenes.Text;
  Memo := Etiquetas.FindObject('txtProductor') as TfrxMemoView;
  Memo.Text := cmbProductor.Text;
  Memo := Etiquetas.FindObject('txtProductor2') as TfrxMemoView;
  Memo.Text := cmbProductor.Text;
  Memo := Etiquetas.FindObject('txtPromedio') as TfrxMemoView;
  Memo.Text := FormatFloat('######.00',(StrToFloat(txtPromedioporCajon.Text)))+' KG';
  Memo := Etiquetas.FindObject('txtPromedio2') as TfrxMemoView;
  Memo.Text := FormatFloat('######.00',(StrToFloat(txtPromedioporCajon.Text)))+' KG';
  Memo := Etiquetas.FindObject('txtProductor3') as TfrxMemoView;
  Memo.Text := cmbProductor.Text;
  Memo := Etiquetas.FindObject('txtAlmacen2') as TfrxMemoView;
  Memo.Text := cmbAlmacenes.Text;

  Etiquetas.ShowReport(true);

end;

procedure TControlCajonesEntradaSalida.borrarRenglones();
var
  i: Integer;
begin
  //
  for i := 1 to datos.RowCount-1 do
  begin
    DeleteRow(datos,i);
  end;
  CreaRenglon;
  datos.Col := 0;
  datos.Row := 1;
  txtTotalCajones.Text := '0';
  txtTotalPesoBruto.Text := '0';
  txtTotalCosto.Text := '0';
  txtTotalTara.Text := '0';
  txtTotalPesoNeto.Text := '0';
  txtPromedioporCajon.Text := '0';
end;

procedure TControlCajonesEntradaSalida.EnviarAFormato;
var
  Memo : TfrxMemoView;
  entrego,recibio : string;
begin
  //
  Memo := Reporte.FindObject('txtTitulo') as TfrxMemoView;
  Memo.Text := cmbTipo.text+' de Cajones ('+CmbClase.Text+')';


  if CmbTipo.Text = 'Entrada' then
  begin
    entrego := txtChofer.Text+' (Placas '+txtPlacas.text+')';
    recibio := '';
    if cmbclase.Text = 'Con Manzana' then
    begin
      QueryImprimir.sql.Text := ' select  p.nombre as proveedor,'+
                                    ' sp.nombre as productor,'+
                                    ' dc.folio,'+
                                    ' dc.fecha,'+
                                    ' dcd.sic_num_cajones,'+
                                    ' a.nombre as nombre_articulo,'+
                                    ' dcd.precio_unitario as costo_unitario,'+
                                    ' dcd.precio_total_neto as total,'+
                                    ' dcd.sic_peso_bruto,'+
                                    ' dcd.sic_tara,'+
                                    ' dcd.unidades as peso_neto,'+
                                    ' dcd.sic_propietario_cajon,'+
                                    ' ''.'' as recibio,'+
                                    ' '','' as entrego'+
                            ' from doctos_cm dc join'+
                            ' proveedores p on p.proveedor_id = dc.proveedor_id join'+
                            ' sic_productores sp on sp.id=dc.sic_productor join'+
                            ' doctos_cm_det dcd on dcd.docto_cm_id = dc.docto_cm_id join'+
                            ' articulos a on a.articulo_id = dcd.articulo_id where dc.docto_cm_id = '+IntToStr(docto_id);
    end
    else
    begin
      recibio := txtChofer.Text+' (Placas '+txtPlacas.text+')';
      entrego := '';
      QueryImprimir.sql.Text := ' select  '''' as proveedor,'+
                                    ' sp.nombre as productor,'+
                                    ' dc.folio,'+
                                    ' dc.fecha,'+
                                    ' dcd.unidades as sic_num_cajones,'+
                                    ' a.nombre as nombre_articulo,'+
                                    ' dcd.costo_unitario,'+
                                    ' dcd.costo_total as total,'+
                                    ' dcd.sic_peso_bruto,'+
                                    ' dcd.sic_tara,'+
                                    ' 0 as peso_neto,'+
                                    ' dcd.sic_propietario_cajon,'+
                                    ' ''.'' as recibio,'+
                                    ' '','' as entrego'+
                            ' from doctos_in dc join'+
                            ' sic_productores sp on sp.id=dc.sic_productor join'+
                            ' doctos_in_det dcd on dcd.docto_in_id = dc.docto_in_id join'+
                            ' articulos a on a.articulo_id = dcd.articulo_id where dc.docto_in_id = '+IntToStr(docto_id);
    end;
  end
  else
  begin
    QueryImprimir.sql.Text := ' select  '''' as proveedor,'+
                                    ' sp.nombre as productor,'+
                                    ' dc.folio,'+
                                    ' dc.fecha,'+
                                    ' dcd.unidades as sic_num_cajones,'+
                                    ' a.nombre as nombre_articulo,'+
                                    ' dcd.costo_unitario,'+
                                    ' dcd.costo_total as total,'+
                                    ' dcd.sic_peso_bruto,'+
                                    ' dcd.sic_tara,'+
                                    ' 0 as peso_neto,'+
                                    ' dcd.sic_propietario_cajon,'+
                                    ' ''.'' as recibio,'+
                                    ' '','' as entrego'+
                            ' from doctos_in dc join'+
                            ' sic_productores sp on sp.id=dc.sic_productor join'+
                            ' doctos_in_det dcd on dcd.docto_in_id = dc.docto_in_id join'+
                            ' articulos a on a.articulo_id = dcd.articulo_id where dc.docto_in_id = '+IntToStr(docto_id);
  end;
  QueryImprimir.Open;
  Memo := Reporte.FindObject('txtEntrego') as TfrxMemoView;
  Memo.Text := entrego;
  Memo := Reporte.FindObject('txtRecibio') as TfrxMemoView;
  Memo.Text := recibio;
  Reporte.PrepareReport;
  Reporte.ShowReport(true);
end;

procedure TControlCajonesEntradaSalida.LimpiarGrid();
var
  i: Integer;
begin
  //
  for i := 1 to datos.RowCount-1 do
  begin
    datos.Rows[i].Clear;
  end;
end;

procedure TControlCajonesEntradaSalida.CreaRenglon();
var
  i:integer;
begin
  //
  datos.RowCount := datos.RowCount + 1;
  for i := 0 to 7 do
  begin
    datos.cells[i,datos.RowCount-1] := '';
  end;

  if (datos.RowCount>2) then
  begin
    datos.cells[0,datos.RowCount-1] := datos.cells[0,datos.RowCount-2];
    datos.cells[1,datos.RowCount-1] := datos.cells[1,datos.RowCount-2];
    datos.cells[5,datos.RowCount-1] := datos.cells[5,datos.RowCount-2];
    datos.cells[7,datos.RowCount-1] := datos.cells[7,datos.RowCount-2];
    datos.cells[8,datos.RowCount-1] := datos.cells[8,datos.RowCount-2];
  end;

  datos.cells[2,datos.RowCount-1] := '0';
  datos.cells[3,datos.RowCount-1] := '0';
  datos.cells[4,datos.RowCount-1] := '0';
  datos.cells[6,datos.RowCount-1] := '0';

end;

procedure TControlCajonesEntradaSalida.DeleteRow(Grid: TStringGrid; ARow: Integer);
var
  i: Integer;
begin
  for i := ARow to Grid.RowCount - 2 do
    Grid.Rows[i].Assign(Grid.Rows[i + 1]);
  Grid.RowCount := Grid.RowCount - 1;
end;

procedure TControlCajonesEntradaSalida.formato_grid();
begin
  datos.Row := 1;
  datos.Col := 0;
  //********* TITULO DE COLUMNAS DE GRID************
  datos.Cells[0,0] := 'Cajones';
  datos.Cells[1,0] := 'Articulo';
  datos.Cells[2,0] := 'Costo Unitario';
  datos.Cells[3,0] := 'Total';
  datos.Cells[4,0] := 'Peso Bruto';
  datos.Cells[5,0] := 'Tara';
  datos.Cells[6,0] := 'Peso Neto';
  datos.Cells[7,0] := 'Propietario';

  //********* TAMA�O DE COLUMNAS DE GRID************
  datos.ColWidths[0] := 45;
  datos.ColWidths[1] := 350;
  datos.ColWidths[2] := 60;
  datos.ColWidths[3] := 80;
  datos.ColWidths[4] := 80;
  datos.ColWidths[5] := 60;
  datos.ColWidths[6] := 60;
  datos.ColWidths[7] := 140;
  datos.ColWidths[8] := 0;

  //********* PRIMER RENGLON************
  datos.Cells[2,1] := '0';
  datos.Cells[3,1] := '0';
  datos.Cells[4,1] := '0';
  datos.Cells[5,1] := '0';
  datos.Cells[6,1] := '0';
end;

procedure TControlCajonesEntradaSalida.Totales();
var
  i,num_cajones: Integer;
  costo_total,peso_bruto,tara,peso_neto : double;
begin
  //
  try
    costo_total := 0;peso_bruto := 0;tara := 0;peso_neto:=0;num_cajones:= 0;
    for i := 1 to datos.RowCount-1 do
    begin
      if (datos.cells[4,i] <> '0') AND (datos.cells[4,i] <> '') then
      begin
        num_cajones := num_cajones + StrToInt(datos.cells[0,i]);
        costo_total := costo_total + StrToFloat(datos.cells[3,i]);
        peso_bruto := peso_bruto + StrToFloat(datos.cells[4,i]);
        tara := tara + StrToFloat(datos.Cells[5,i]);
        peso_neto := peso_neto + StrToFloat(datos.cells[6,i]);
      end;
    end;
    txtTotalCajones.Text := inttostr(num_cajones);
    txtTotalCosto.Text := FloatToStr(costo_total);
    txtTotalTara.Text := FloatToStr(tara);
    txtTotalPesoBruto.Text := floattostr(peso_bruto);
    txtTotalPesoNeto.Text := FloatToStr(peso_neto);
    txtPromedioporCajon.Text := FormatFloat('######.00',peso_neto/num_cajones);
  except
  end;
end;

procedure TControlCajonesEntradaSalida.calcularenglon(row:integer);
begin
   //
  if datos.col <> 0 then
   begin
     try
       datos.Cells[6,row] := floattostr(strtofloat(datos.cells[4,row])-strtofloat(datos.cells[5,row]));
       datos.cells[3,row] := floattostr(StrToFloat(datos.cells[6,row])*strtofloat(datos.cells[2,row]));
     except
     end;
  end;
end;

procedure TControlCajonesEntradaSalida.actualizar_productores();
begin
  QueryProductores.SQL.Text := 'select * from sic_productores where proveedor_id = '+inttostr(cmbProveedor.KeyValue)+' order by nombre';
  QueryProductores.Open;
  cmbProductor.KeyValue := Conexion.ExecSQLScalar('select first 1 id from sic_productores where proveedor_id = '+inttostr(cmbProveedor.KeyValue)+' order by nombre');
  txtClaveProductor.Text := Conexion.ExecSQLScalar('select clave from sic_productores where id=:pid',[cmbProductor.KeyValue]);
end;


{$endregion}

{$REGION 'Funciones Entrada Salida'}
procedure TControlCajonesEntradaSalida.EntradaManzana();
var
  query_str,serie,folio_str,clave_proveedor,estatus,clave_articulo,notas,rol_ve,
  propietario_cajon,rol_valor,fecha:string;
  documento_id,proveedor_id,consecutivo,folio_id,
  dir_cli_id,almacen_id, moneda_id,cond_pago_id, articulo_id,detalle_id,
  num_cajones, productor_id, articulo_discreto_id : integer;
  precio_unitario, precio_total_neto, unidades,pctje_dscto,peso_bruto,tara: double;
  i,num_discretos: Integer;
  insertar:bool;
begin
  //
  Conexion.StartTransaction;
  try
    serie := Conexion.ExecSQLScalar('select serie from folios_compras where tipo_docto = ''R''');
    consecutivo := Conexion.ExecSQLScalar('select consecutivo from folios_compras where tipo_docto = ''R''');
    folio_id := Conexion.ExecSQLScalar('select folio_compras_id from folios_compras where tipo_docto = ''R''');
    //********** OBTIENE EL SIGUIENTE FOLIO DEL DOCUMENTO DESTINO Y SE ACTUALIZA LA TABLA DE FOLIOS*******
    if serie <> '@'
    then folio_str := serie + Format('%.*d',[9-length(serie),(consecutivo)])
    else folio_str := Format('%.*d',[9,(consecutivo)]);
    Conexion.ExecSQL('update folios_compras set consecutivo=consecutivo+1 where folio_compras_id=:folio_id',[folio_id]);

    //************************* SE OBTIENEN LOS DEMAS DATOS PARA DAR DE ALTA EL ENCABEZADO***************
    proveedor_id := cmbProveedor.KeyValue;
    clave_proveedor := Conexion.ExecSQLScalar('select coalesce(clave_prov,'''') from get_clave_prov(:id)',[proveedor_id]);
    almacen_id := cmbAlmacenes.KeyValue;
    moneda_id := 1;
    cond_pago_id := Conexion.ExecSQLScalar('select first 1 cond_pago_id from proveedores where proveedor_id = :pid',[proveedor_id]);
    fecha := FormatDateTime('mm/dd/yyyy', dtpFecha.Date);
    productor_id :=  cmbProductor.KeyValue;

    //****************************************DA DE ALTA EL ENCABEZADO***********************************

      query_str := 'insert into doctos_cm (docto_cm_id,tipo_docto,folio,fecha,clave_prov,'+
                   ' proveedor_id,almacen_id,moneda_id,tipo_cambio,'+
                   ' sistema_origen,tipo_dscto,estatus,cond_pago_id,sic_productor,sic_chofer,sic_placas) values (-1,''R'','''+folio_str+''','+
                   ' '''+fecha+''','''+clave_proveedor+''','+inttostr(proveedor_id)+','+
                   ' '+inttostr(almacen_id)+','+inttostr(moneda_id)+',1,''CM'',''P'',''P'','+inttostr(cond_pago_id)+','
                   + IntToStr(productor_id)+','''+txtChofer.Text+''','''+txtPlacas.Text+''') returning docto_cm_id';
      documento_id := Conexion.ExecSQLScalar(query_str);
      docto_id := documento_id;
     //*******************************DETALLADO DEL DOCUMENTO*********************************************
     for i := 1 to datos.RowCount-1 do
     begin
        insertar := true;
        try
          articulo_id := strtoint(datos.cells[8,i]);
          clave_articulo := Conexion.ExecSQLScalar('select coalesce(clave_articulo,'''') from get_clave_art(:aid)',[articulo_id]);
          unidades := strtofloat(datos.cells[6,i]);
          precio_unitario := StrToFloat(datos.cells[2,i]);
          precio_total_neto := StrToFloat(datos.cells[3,i]);
          num_cajones := StrToInt(datos.cells[0,i]);
          peso_bruto := StrToFloat(datos.cells[4,i]);
          tara := StrToFloat(datos.cells[5,i]);
          propietario_cajon := datos.Cells[7,i];
        except
          insertar := false;
        end;

        if insertar then
        begin
          query_str := 'insert into doctos_cm_det (docto_cm_det_id, docto_cm_id,'+
                       ' clave_articulo,articulo_id,unidades,precio_unitario,precio_total_neto,notas,pctje_dscto,sic_num_cajones,'+
                       ' sic_peso_bruto,sic_tara,sic_propietario_cajon) values(-1,'+
                       ' '+inttostr(documento_id)+','''+clave_articulo+''','+inttostr(articulo_id)+','+FloatToStr(unidades)+
                       ' ,'+FloatToStr(precio_unitario)+','+FloatToStr(precio_total_neto)+','''',0,'+
                       ' '+IntToStr(num_cajones)+','+FloatToStr(peso_bruto)+','+FloatToStr(tara)+','''+propietario_cajon+''') returning docto_cm_det_id ';
          detalle_id := Conexion.ExecSQLScalar(query_str);

          if Conexion.ExecSQLScalar('select count(*) from articulos_discretos where articulo_id='+inttostr(articulo_id)+' and clave='''+IntToStr(consecutivo)+''' ')>0
          then articulo_discreto_id := Conexion.ExecSQLScalar('select art_discreto_id from articulos_discretos where articulo_id='+inttostr(articulo_id)+' and clave='''+IntToStr(consecutivo)+'''')
          else articulo_discreto_id := Conexion.ExecSQLScalar('insert into articulos_discretos values(-1,'''+IntToStr(consecutivo)+''','+inttostr(articulo_id)+',''L'',current_date) returning art_discreto_id');

          Conexion.ExecSQL('insert into desglose_en_discretos_cm values(-1,'+inttostr(detalle_id)+','+IntToStr(articulo_discreto_id)+','+FloatToStr(unidades)+')');
        end
        else
        begin
          Conexion.Rollback;
        end;
     end;
     Conexion.ExecSQL('execute procedure calc_totales_docto_cm(:did,''N'')',[documento_id]);
     Conexion.ExecSQL('execute procedure aplica_docto_cm(:id)',[documento_id]);
     Conexion.Commit;
     ShowMessage('Orden de Compra con folio: '+folio_str);
  except
    Conexion.Rollback;
  end;

end;

procedure TControlCajonesEntradaSalida.EntradaVacio();
var
  query_str,serie,folio_str,clave_proveedor,clave_articulo,fecha,folio_nuevo:string;
  documento_id,productor_id,consecutivo,concepto_id,
  almacen_id, moneda_id,cond_pago_id, articulo_id, articulo_discreto_id : integer;
  precio_unitario, precio_total_neto, unidades: double;
  i: Integer;
  insertar:bool;
begin
  //
  Conexion.StartTransaction;
  try
    serie := GetSerie(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_in where nombre = ''ENTRADA CAJONES'''));
    consecutivo := strtoint(GetnumFolio(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_in where nombre = ''ENTRADA CAJONES''')));
    concepto_id := Conexion.ExecSQLScalar('select concepto_in_id from conceptos_in where nombre = ''ENTRADA CAJONES''');
    //********** OBTIENE EL SIGUIENTE FOLIO DEL DOCUMENTO DESTINO Y SE ACTUALIZA LA TABLA DE FOLIOS*******
    folio_str := serie + Format('%.*d',[9-length(serie),(consecutivo)]);
    folio_nuevo := serie + Format('%.*d',[9-length(serie),(consecutivo+1)]);
    Conexion.ExecSQL('update conceptos_in set sig_folio=:sig where concepto_in_id=:concepto_id',[folio_nuevo,concepto_id]);

    //************************* SE OBTIENEN LOS DEMAS DATOS PARA DAR DE ALTA EL ENCABEZADO***************
    almacen_id := cmbAlmacenes.KeyValue;
    moneda_id := 1;
    fecha := FormatDateTime('mm/dd/yyyy', dtpFecha.Date);
    productor_id :=  cmbProductor.KeyValue;
    //****************************************DA DE ALTA EL ENCABEZADO***********************************

      query_str := 'insert into doctos_in (docto_in_id,almacen_id,concepto_in_id,folio,naturaleza_concepto,fecha,'+
                   ' sistema_origen,sic_productor,sic_chofer,sic_placas,sic_proveedor,sic_integ) values (-1,'+inttostr(almacen_id)+','+inttostr(concepto_id)+','+
                   ' '''+folio_str+''',''E'','''+fecha+''','+
                   ' ''IN'','+IntToStr(productor_id)+','''+txtChofer.Text+''','''+txtPlacas.Text+''','+
                   ' '+inttostr(cmbProveedor.KeyValue)+','''+liga_in+''') returning docto_in_id';
      documento_id := Conexion.ExecSQLScalar(query_str);
      docto_id := documento_id;

     //*******************************DETALLADO DEL DOCUMENTO*********************************************

          articulo_id := Conexion.ExecSQLScalar('select articulo_id from articulos where nombre =''CAJON''');
          clave_articulo := Conexion.ExecSQLScalar('select coalesce(clave_articulo,'''') from get_clave_art(:aid)',[articulo_id]);
          unidades := StrToInt(txtTotalCajones.Text);
          precio_unitario := 0;
          precio_total_neto := 0;

     query_str := 'insert into doctos_in_det (docto_in_det_id, docto_in_id,almacen_id,concepto_in_id,'+
                 ' clave_articulo,articulo_id,tipo_movto,unidades,costo_unitario,costo_total,metodo_costeo,sic_num_cajones) values(-1,'+
                 ' '+inttostr(documento_id)+','+IntToStr(almacen_id)+','+IntToStr(concepto_id)+','''+clave_articulo+''','+
                 ' '+inttostr(articulo_id)+',''E'','+
                 ' '+FloatToStr(unidades)+
                 ' ,'+FloatToStr(precio_unitario)+','+FloatToStr(precio_total_neto)+',''C'','+FloatToStr(unidades)+') ';

     Conexion.ExecSQL(query_str);
     Conexion.ExecSQL('execute procedure aplica_docto_in(:did)',[documento_id]);
     Conexion.Commit;

  except
    Conexion.Rollback;
  end;
end;

procedure TControlCajonesEntradaSalida.SalidaVacio();
var
  query_str,serie,folio_str,clave_proveedor,clave_articulo,fecha,folio_nuevo:string;
  documento_id,productor_id,consecutivo,concepto_id,
  almacen_id, moneda_id,cond_pago_id, articulo_id, articulo_discreto_id : integer;
  precio_unitario, precio_total_neto, unidades: double;
begin
  //
  Conexion.StartTransaction;
  try
    serie := GetSerie(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_in where nombre = ''SALIDA CAJONES'''));
    consecutivo := strtoint(GetnumFolio(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_in where nombre = ''SALIDA CAJONES''')));
    concepto_id := Conexion.ExecSQLScalar('select concepto_in_id from conceptos_in where nombre = ''SALIDA CAJONES''');
    //********** OBTIENE EL SIGUIENTE FOLIO DEL DOCUMENTO DESTINO Y SE ACTUALIZA LA TABLA DE FOLIOS*******
    folio_str := serie + Format('%.*d',[9-length(serie),(consecutivo)]);
    folio_nuevo := serie + Format('%.*d',[9-length(serie),(consecutivo+1)]);
    Conexion.ExecSQL('update conceptos_in set sig_folio=:sig where concepto_in_id=:concepto_id',[folio_nuevo,concepto_id]);

    //************************* SE OBTIENEN LOS DEMAS DATOS PARA DAR DE ALTA EL ENCABEZADO***************
    almacen_id := cmbAlmacenes.KeyValue;
    moneda_id := 1;
    fecha := FormatDateTime('mm/dd/yyyy', dtpFecha.Date);
    productor_id :=  cmbProductor.KeyValue;
    //****************************************DA DE ALTA EL ENCABEZADO***********************************

      query_str := 'insert into doctos_in (docto_in_id,almacen_id,concepto_in_id,folio,naturaleza_concepto,fecha,'+
                   ' sistema_origen,sic_productor,sic_chofer,sic_placas) values (-1,'+inttostr(almacen_id)+','+inttostr(concepto_id)+','+
                   ' '''+folio_str+''',''S'','''+fecha+''','+
                   ' ''IN'','+IntToStr(productor_id)+','''+txtChofer.Text+''','''+txtPlacas.Text+''') returning docto_in_id';
      documento_id := Conexion.ExecSQLScalar(query_str);
      docto_id := documento_id;

     //*******************************DETALLADO DEL DOCUMENTO*********************************************

          articulo_id := Conexion.ExecSQLScalar('select articulo_id from articulos where nombre =''CAJON''');
          clave_articulo := Conexion.ExecSQLScalar('select coalesce(clave_articulo,'''') from get_clave_art(:aid)',[articulo_id]);
          //validar los campos vacios
          unidades := StrToInt(txtTotalCajones.Text);
          precio_unitario := 0;
          precio_total_neto := 0;

     query_str := 'insert into doctos_in_det (docto_in_det_id, docto_in_id,almacen_id,concepto_in_id,'+
                 ' clave_articulo,articulo_id,tipo_movto,unidades,costo_unitario,costo_total,metodo_costeo,sic_num_cajones) values(-1,'+
                 ' '+inttostr(documento_id)+','+IntToStr(almacen_id)+','+IntToStr(concepto_id)+','''+clave_articulo+''','+
                 ' '+inttostr(articulo_id)+',''S'','+
                 ' '+FloatToStr(unidades)+
                 ' ,'+FloatToStr(precio_unitario)+','+FloatToStr(precio_total_neto)+',''C'','+FloatToStr(unidades)+') ';

     Conexion.ExecSQL(query_str);
     Conexion.ExecSQL('execute procedure aplica_docto_in(:did)',[documento_id]);
     Conexion.Commit;

  except
    Conexion.Rollback;
  end;
end;

{$ENDREGION}

{$REGION 'Combos'}
procedure TControlCajonesEntradaSalida.cmbProductorClick(Sender: TObject);
begin
   txtClaveProductor.Text := Conexion.ExecSQLScalar('select clave from sic_productores where id=:pid',[cmbProductor.KeyValue]);
end;

procedure TControlCajonesEntradaSalida.cmbProveedorClick(Sender: TObject);
begin
  actualizar_productores;
end;
{$ENDREGION}

{$REGION 'Eventos del grid'}

procedure TControlCajonesEntradaSalida.datosKeyDown(Sender: TObject; var Key: Word;  Shift: TShiftState);
var
  FormBusqueda:TBusquedaArticulos;
begin
  if (Shift = [ssCtrl]) and (Key = VK_DELETE) and (datos.RowCount > 1 ) then
  begin
    DeleteRow(datos,datos.Row);
  end;

  if ((Key = VK_F4) and (datos.Col = 1)) then
  begin
    FormBusqueda := TBusquedaArticulos.Create(nil);
    FormBusqueda.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
    FormBusqueda.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
    FormBusqueda.Conexion.Params.Values['UserName'] := Conexion.Params.Values['UserName'];
    FormBusqueda.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
    FormBusqueda.txtBusqueda.Text := datos.cells[datos.Col,datos.Row];
    if FormBusqueda.ShowModal = mrOk then
    begin
      datos.Cells[datos.col,datos.row] := FormBusqueda.articulo_nombre;
      datos.Cells[8,datos.row] := IntToStr(FormBusqueda.articulo_id);
      datos.Col := datos.col+1;
    end;
  end;  
end;

procedure TControlCajonesEntradaSalida.datosKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) then
  begin
    if (datos.col = 7) then
    begin
      CreaRenglon;
      datos.Row := datos.RowCount-1;
      datos.Col := 0;
    end
    else
    begin
      datos.col := datos.Col+1;
      Totales;
    end;
    calculaRenglon(datos.Row);

  end;

end;
procedure TControlCajonesEntradaSalida.datosSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  if ((ACol=3) or (ACol = 6))  then
  begin
    CanSelect := False;
    datos.col := Acol+1;
    exit;
  end;
  try
    
  except
     
  end;
  
end;

{$ENDREGION}

{$REGION 'Eventos del Formulario'}

procedure TControlCajonesEntradaSalida.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Conexion.Close;
  //Application.Terminate;
end;

procedure TControlCajonesEntradaSalida.FormShow(Sender: TObject);
begin
  QueryProveedores.Open;
  QueryAlmacenes.Open;
  QueryProductores.Open;

  if docto_id = 0 then
  begin
    cmbProveedor.KeyValue := Conexion.ExecSQLScalar('select first 1 proveedor_id from proveedores order by nombre');
    cmbAlmacenes.KeyValue := Conexion.ExecSQLScalar('select almacen_id from almacenes where es_ppal=''S''');
    actualizar_productores;
    formato_grid;
    dtpFecha.Date := Date;
  end;


  try
    Conexion.ExecSQL('grant all on TABLE sic_productores to USUARIO_MICROSIP;');
  except
  end;
end;
{$ENDREGION}

{$REGION 'Menu Contextual'}

procedure TControlCajonesEntradaSalida.mnuItemAbrirClick(Sender: TObject);
var
  FormProductores : TControlcajonesproductores;
begin
  FormProductores := TControlCajonesProductores.Create(nil);
  FormProductores.Conexion := Conexion;
  FormProductores.Caption := 'Productor - '+cmbProductor.Text;
  FormProductores.productor_id := cmbProductor.KeyValue;
  FormProductores.ShowModal;
  actualizar_productores;

end;

procedure TControlCajonesEntradaSalida.mnuItemNuevoClick(Sender: TObject);
var
  FormProductores : TControlcajonesproductores;
begin
  FormProductores := TControlCajonesProductores.Create(nil);

  FormProductores.Conexion.Params := Conexion.Params;
  FormProductores.Caption := 'Productor - Nuevo Productor';
  FormProductores.productor_id := 0;
  FormProductores.ShowModal;
  actualizar_productores;
end;

procedure TControlCajonesEntradaSalida.mnuProductoresPopup(Sender: TObject);
begin
  mnuItemAbrir.Enabled := cmbProductor.Text<>'';
end;
{$ENDREGION}

{$REGION 'Clave de Productor'}

procedure TControlCajonesEntradaSalida.txtClaveProductorExit(Sender: TObject);
var
  productor_id :integer;
begin
  productor_id := Conexion.ExecSQLScalar('select id from sic_productores where clave=:cl',[txtClaveProductor.Text]);
  if productor_id <> 0 then
  begin
    cmbProductor.KeyValue := productor_id;
  end
  else
  begin
    ShowMessage('Proveedor no Encontrado');
    txtClaveProductor.selectall;
    txtClaveProductor.setfocus;
  end;
end;
{$ENDREGION}

{$REGION 'Boton Guardar'}

procedure TControlCajonesEntradaSalida.btnEtiquetasClick(Sender: TObject);
begin
  ImprimirEtiquetas;
end;

procedure TControlCajonesEntradaSalida.btnGuardarClick(Sender: TObject);
begin
  //******SI ES ENTRADA**********
  if CmbTipo.ItemIndex = 0 then
  begin
    case CmbClase.ItemIndex of
      0:begin
          liga_in := 'S';
          EntradaVacio;
          EntradaManzana;
          LimpiarGrid;
          EnviarAFormato;
          ImprimirEtiquetas;
        end;
      1:begin
          liga_in := 'N';
          EntradaVacio;
          LimpiarGrid;
          EnviarAFormato;
      end;
    end;
  end
  //******SI ES SALIDA***********
  else
  begin
    SalidaVacio;
    LimpiarGrid;
    txtTotalCajones.Clear;
    txtTotalCajones.SetFocus;
    EnviarAFormato;
  end;
  borrarRenglones;

end;
{$ENDREGION}

{$REGION 'Boton Imprimir Recibo'}
procedure TControlCajonesEntradaSalida.btnReciboClick(Sender: TObject);
begin
  EnviarAFormato;
end;
{$ENDREGION}

{$REGION 'Combos'}
procedure TControlCajonesEntradaSalida.CmbClaseChange(Sender: TObject);
begin
  //
  case CmbClase.ItemIndex of
    //********* ENTRADA**********
    0:begin
      datos.Visible := true;
      txtTotalCajones.Text := '0';
      txttotalCajones.Enabled := False;
      datos.SetFocus;
      
    end;
    //******* SALIDA ************
    1:begin
      datos.Visible := False;
      txtTotalCajones.Enabled := True;
      txtTotalCajones.SetFocus;
    end;

  end;
  LimpiarGrid;
end;

procedure TControlCajonesEntradaSalida.CmbTipoChange(Sender: TObject);
begin
   case CmbTipo.ItemIndex of
    //********* ENTRADA**********
    0:begin
      datos.Visible := true;
      txtTotalCajones.Text := '0';
      txttotalCajones.Enabled := False;
      datos.SetFocus;
      CmbClase.items.Clear;
      CmbClase.items.Add('Con Manzana');
      CmbClase.items.Add('Vacios');
      CmbClase.ItemIndex := 0;
      CmbClase.Enabled := True;
    end;
    //******* SALIDA ************
    1:begin
      datos.Visible := False;
      txtTotalCajones.Enabled := True;
      txtTotalCajones.SetFocus;
      CmbClase.items.Clear;
      CmbClase.items.Add('Vacios');
      CmbClase.ItemIndex := 0;
      CmbClase.Enabled := False;
    end;

  end;
  LimpiarGrid;
end;


{$ENDREGION}

end.

