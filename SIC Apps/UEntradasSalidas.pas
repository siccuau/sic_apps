unit UEntradasSalidas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef,
  FireDAC.VCLUI.Wait, Vcl.ComCtrls, Data.DB, FireDAC.Comp.Client,
  Vcl.ExtCtrls, Vcl.StdCtrls, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, System.Rtti,
  System.Bindings.Outputs, Vcl.Bind.Editors, Data.Bind.EngExt,
  Vcl.Bind.DBEngExt, Data.Bind.Components, Data.Bind.DBScope, System.Character, UBusqueda,
  Vcl.Grids, Vcl.Bind.Grid, Data.Bind.Grid, frxClass, frxDBSet;

type
  TEntradasSalidas = class(TForm)
    pnlGenerar: TPanel;
    cbTipo: TComboBox;
    cbConceptos: TComboBox;
    cbAlmacenes: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    lblAlmacen: TLabel;
    btnGenerar: TButton;
    qryConceptos: TFDQuery;
    qryAlmacenes: TFDQuery;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkListControlToField1: TLinkListControlToField;
    BindSourceDB2: TBindSourceDB;
    qryDoctosIn: TFDQuery;
    pnlCaptura: TPanel;
    lblCaptura: TLabel;
    Label4: TLabel;
    lblNombreArticulo: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    txtArticulo: TEdit;
    txtUnidades: TEdit;
    btnAgregar: TButton;
    txtDestino: TEdit;
    qryArticulosClaves: TFDQuery;
    qryDoctosInDet: TFDQuery;
    sgDetallesInv: TStringGrid;
    qryDetallesDocto: TFDQuery;
    BindSourceDB3: TBindSourceDB;
    LinkGridToDataSourceBindSourceDB3: TLinkGridToDataSource;
    btnGenerarNuevoDocto: TButton;
    Reporte: TfrxReport;
    DSReporte: TfrxDBDataset;
    btnImprimir: TButton;
    btnVistaprevia: TButton;
    Label3: TLabel;
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    procedure capturar(captura:boolean);
    procedure FormShow(Sender: TObject);
    procedure cbTipoChange(Sender: TObject);
    procedure btnGenerarClick(Sender: TObject);
    procedure btnAgregarClick(Sender: TObject);
    procedure txtArticuloKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sgDetallesInvDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure btnGenerarNuevoDoctoClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure btnVistapreviaClick(Sender: TObject);
    procedure sgDetallesInvKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    naturaleza : string;
  end;

var
  EntradasSalidas: TEntradasSalidas;

implementation

{$R *.dfm}

function split_letternumber(folio:string) : TStringList;
var
	l,n : string;
  letra : char;
  lista : TStringlist;
begin
	for letra in folio do
  begin
  	if IsLetter(letra) then
    	l := l + letra
    else
    	n := n+letra;
  end;
  lista := TStringList.Create;
  lista.Add(l);
  lista.Add(n);

  Result := lista;

end;

function get_long_folio(serie:string;consecutivo:string):string;
var
	folio: string;
  num : integer;
begin
	num := 9 - serie.Length;
  Result := serie + Format('%.*d',[num, strtoint(consecutivo)]);
end;

function get_short_folio(folio:string):string;
var
  num : integer;
  letra : char;
  l,n : string;
begin
	for letra in folio do
  begin
  	if IsLetter(letra) then
    	l := l + letra
    else
    	n := n+letra;
  end;
  n := inttostr(strtoint(n));
  Result := l+n;

end;

procedure TEntradasSalidas.capturar(captura:boolean);
begin
	pnlGenerar.Visible := not captura;
	pnlCaptura.Visible := captura;
end;

procedure TEntradasSalidas.btnAgregarClick(Sender: TObject);
var
	costo,unidades : Double;
  did:string;
begin
	costo := Conexion.ExecSQLScalar('select coalesce(costo_ultima_compra,0) from get_ultcom_art('+qryArticulosClaves.FieldByName('articulo_id').AsString+')');
  if TryStrToFloat(txtUnidades.Text,unidades) then
  begin
  	if unidades > 0 then
    begin
      with qryDoctosInDet do
      begin
        Append;
        FieldByName('docto_in_det_id').Value := -1;
        FieldByName('docto_in_id').Value := qryDoctosIn.FieldByName('docto_in_id').value;
        FieldByName('almacen_id').Value := qryAlmacenes.FieldByName('almacen_id').Value;
        FieldByName('concepto_in_id').Value := qryConceptos.FieldByName('concepto_in_id').Value;
        FieldByName('clave_articulo').Value := txtArticulo.Text;
        FieldByName('articulo_id').Value := qryArticulosClaves.FieldByName('articulo_id').Value;
        FieldByName('tipo_movto').Value := naturaleza;
        FieldByName('unidades').Value := unidades;
        FieldByName('costo_unitario').Value := costo;
        FieldByName('costo_total').Value := unidades * costo;
        FieldByName('aplicado').Value := 'S';
        FieldByName('metodo_costeo').Value := 'C';
        FieldByName('rol').Value := 'N';
        FieldByName('fecha').Value := Now;
        Post;
        ApplyUpdates(0);
        CommitUpdates;
        Refresh;
      end;
      did := Conexion.ExecSQLScalar('select max(docto_in_det_id) from doctos_in_det where docto_in_id ='+qryDoctosIn.FieldByName('docto_in_id').AsString);
      Conexion.ExecSQL('Insert into sicdest_destinos values('+did+','''+txtDestino.Text+''')');
      Conexion.ExecSQL('execute procedure aplica_docto_in('+qryDoctosIn.FieldByName('docto_in_id').AsString+');');
      Conexion.Commit;
      qryDetallesDocto.Refresh;
      qryDetallesDocto.Filtered := False;
      qryDetallesDocto.Refresh;
      qryDetallesDocto.Filtered := True;
      lblNombreArticulo.Caption := '';
      txtUnidades.Clear;
      txtDestino.Clear;
      txtArticulo.Clear;
      txtArticulo.SetFocus;
    end
    else
    begin
    	ShowMessage('Unidades incorrectas');
      txtUnidades.SelectAll;
      txtUnidades.SetFocus;
    end;
  end
  else
  begin
    ShowMessage('Unidades incorrectas');
    txtUnidades.SelectAll;
    txtUnidades.SetFocus;
  end;
end;

procedure TEntradasSalidas.btnGenerarClick(Sender: TObject);
var
	sig_folio,nuevo_folio : string;
  sig_consec,docid : string;
  v: boolean;
begin
	qryConceptos.Refresh;
  qryConceptos.Locate('nombre',cbConceptos.Text,[]);
	if qryConceptos.FieldByName('sig_folio').AsString = '' then
  begin
  	ShowMessage('El concepto seleccionado no tiene folio automatico. Indiquelo por favor.');
  end
  else
  begin
    sig_folio := Conexion.ExecSQLScalar('execute procedure get_sigfol_in('+qryConceptos.FieldByName('concepto_in_id').AsString+',''*'')');
    sig_consec := inttostr(strtoint(split_letternumber(sig_folio)[1]) + 1);
    nuevo_folio := get_long_folio(split_letternumber(sig_folio)[0],sig_consec);
    Conexion.ExecSQL('update conceptos_in set sig_folio = '''+nuevo_folio+''' where concepto_in_id = '+qryConceptos.FieldByName('concepto_in_id').AsString );

    with qryDoctosIn do
    begin
      Append;
      FieldByName('docto_in_id').Value := -1;
      FieldByName('almacen_id').Value := qryAlmacenes.FieldByName('almacen_id').Value;
      FieldByName('concepto_in_id').Value := qryConceptos.FieldByName('concepto_in_id').Value;
      FieldByName('folio').Value := sig_folio ;
      FieldByName('naturaleza_concepto').Value := naturaleza;
      FieldByName('fecha').Value := Now;
      FieldByName('descripcion').Value := '';
      FieldByName('sistema_origen').Value := 'IN';
      FieldByName('usuario_creador').Value := Conexion.Params.Values['User_Name'];
      Post;
      ApplyUpdates(0);
      CommitUpdates;
      Refresh;
    end;
    docid := Conexion.ExecSQLScalar('select max(docto_in_id) from doctos_in');
    v := qryDoctosIn.Locate('docto_in_id',docid,[]);
    if naturaleza = 'E' then
	    Conexion.ExecSQL('insert into libres_entradas_in values('+docid+',''S'')')
    else
			Conexion.ExecSQL('insert into libres_salidas_in values('+docid+',''S'')');
    Conexion.Commit;
    capturar(true);
    lblCaptura.Caption := qryConceptos.FieldByName('nombre').AsString +' ' + get_short_folio(sig_folio);
    qryDetallesDocto.Filter := 'docto_in_id='+qryDoctosIn.FieldByName('docto_in_id').AsString;
    qryDetallesDocto.Filtered := True;
		qryDetallesdocto.Refresh;
    txtArticulo.SetFocus;
  end;

end;

procedure TEntradasSalidas.btnGenerarNuevoDoctoClick(Sender: TObject);
begin
	if MessageDlg('Esta seguro de que desea cerrar este documento y generar uno nuevo?',mtConfirmation,mbYesNo,0) = mrYes then
  begin
  	if qryDoctosIn.FieldByName('naturaleza_concepto').AsString = 'E' then
    	Conexion.ExecSQL('update libres_entradas_in set contando = ''N'' where docto_in_id = '+qryDoctosIn.FieldByName('docto_in_id').AsString)
    else
    	Conexion.ExecSQL('update libres_salidas_in set contando = ''N'' where docto_in_id = '+qryDoctosIn.FieldByName('docto_in_id').AsString);
    naturaleza := 'E';
    cbTipo.ItemIndex := 0;
    qryConceptos.Filter := 'naturaleza = ''E''';
    qryConceptos.Filtered := True;
    pnlCaptura.Visible := False;
    pnlGenerar.Visible := True;

  end;
end;

procedure TEntradasSalidas.btnImprimirClick(Sender: TObject);
begin
	Reporte.PrepareReport(True);
  Reporte.Print;
end;

procedure TEntradasSalidas.btnVistapreviaClick(Sender: TObject);
begin
	reporte.PrepareReport(True);
  reporte.ShowReport(True);
end;

procedure TEntradasSalidas.cbTipoChange(Sender: TObject);
begin
	if cbTipo.ItemIndex = 0 then
  	naturaleza := 'E'
  else
  	naturaleza := 'S';
  qryConceptos.Filter := 'naturaleza='''+naturaleza+'''';
  qryConceptos.Refresh;
  cbConceptos.Items.Clear;
  qryConceptos.First;
  while not qryConceptos.Eof do
  begin
  	cbConceptos.Items.Add(qryConceptos.FieldByName('nombre').AsString);
    qryConceptos.Next;
  end;
  cbConceptos.ItemIndex := 0;
end;

procedure TEntradasSalidas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	qryConceptos.Close;
  qryAlmacenes.Close;
  qryDoctosIn.Close;
  qryDoctosInDet.Close;
  qryArticulosClaves.Close;
  qryDetallesDocto.close;
	Application.Terminate;
end;

procedure TEntradasSalidas.FormShow(Sender: TObject);
var
	tabla : integer;
  docto_id : integer;
  encontrado : boolean;
begin
	tabla := Conexion.ExecSQLScalar('select count(*) from rdb$relations where rdb$relation_name = ''SICDEST_DESTINOS'' ');
  if tabla = 0
  then  Conexion.ExecSQL('CREATE TABLE SICDEST_DESTINOS ( '+
                          '      DOCTO_IN_DET_ID  INTEGER,'+
                          '      DESTINO          VARCHAR(100)'+
                          '  );');
	qryConceptos.Open;
  qryAlmacenes.Open;
  qryDoctosIn.Open;
  qryDoctosInDet.Open;
  qryArticulosClaves.Open;
  qryDetallesDocto.Open;
  qryConceptos.filter := 'naturaleza = ''E''';
  qryConceptos.Filtered := True;
  qryConceptos.Refresh;
  docto_id := Conexion.ExecSQLScalar('select coalesce(max(docto_in_id),-1) from libres_entradas_in where contando = ''S'' ');
  if docto_id = -1 then
  begin
  	docto_id := Conexion.ExecSQLScalar('select coalesce(max(docto_in_id),-1) from libres_salidas_in where contando = ''S'' ');
    if docto_id = -1 then
    begin
      capturar(False);
    end
    else
    begin
    	encontrado := True;
    end;
  end
  else
  begin
  	encontrado := True;
  end;

  if encontrado then
  begin
  	qryConceptos.Filtered := False;
    qryConceptos.Refresh;
    qryDoctosIn.Locate('docto_in_id',docto_id,[]);
    qryConceptos.Locate('concepto_in_id',qryDoctosIn.FieldByName('concepto_in_id').Value,[]);
    qryAlmacenes.Locate('almacen_id',qryDoctosIn.FieldByName('almacen_id').Value,[]);
    capturar(True);
    lblCaptura.Caption := qryConceptos.FieldByName('nombre').AsString +' ' + get_short_folio(qryDoctosIn.FieldByName('folio').AsString);
    txtArticulo.SetFocus;
    naturaleza := qryDoctosIn.FieldByName('naturaleza_concepto').Value;
    qryDetallesDocto.filter := 'docto_in_id='+qryDoctosIn.FieldByName('docto_in_id').AsString;
    qryDetallesDocto.Filtered := True;
    qryDetallesDocto.Refresh;
  end;
  cbConceptos.Items.Clear;
  qryConceptos.First;
  while not qryConceptos.Eof do
  begin
  	cbConceptos.Items.Add(qryConceptos.FieldByName('nombre').AsString);
    qryConceptos.Next;
  end;
  cbConceptos.ItemIndex := 0;

end;

procedure TEntradasSalidas.sgDetallesInvDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  s : string;
  LDelta : integer;
begin
  if (ACol=1) or (ACol>2) then
  begin
    s     := sgDetallesInv.Cells[ACol, ARow];
    LDelta := sgDetallesInv.ColWidths[ACol] - Canvas.TextWidth(s);
    Canvas.TextRect(Rect, Rect.Left+LDelta, Rect.Top+2, s);
  end
  else
  Canvas.TextRect(Rect, Rect.Left+2, Rect.Top+2, sgDetallesInv.Cells[ACol, ARow]);
end;


procedure TEntradasSalidas.sgDetallesInvKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
	v : boolean;
begin
	if (key = VK_DELETE) and (Shift=[ssCtrl]) then
  begin
    if MessageDlg('Esta Seguro de Eliminar esta Partida?',mtConfirmation,mbYesNo,0) = mrYes then
    begin
      v := qryDoctosInDet.Locate('docto_in_det_id',qryDetallesDocto.FieldByName('docto_in_det_id').AsString,[]);
      qryDoctosInDet.Delete;
      qryDoctosInDet.ApplyUpdates(0);
      qryDoctosInDet.CommitUpdates;
      qryDoctosInDet.Refresh;
      qryDetallesDocto.Close;
      qryDetallesDocto.Open;
    end;
  end;

end;

procedure TEntradasSalidas.txtArticuloKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
	FormBusquedaArticulos : TBusquedaArticulos;
begin
	if Key = VK_F4 then
  begin
    FormBusquedaArticulos := TbusquedaArticulos.Create(nil);
    FormBusquedaArticulos.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
    FormBusquedaArticulos.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
    FormBusquedaArticulos.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
    FormBusquedaArticulos.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
    FormBusquedaArticulos.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
     if FormBusquedaArticulos.ShowModal = mrOK then
     begin
       qryArticulosClaves.Locate('articulo_id',FormBusquedaArticulos.articulo_id);
       lblNombreArticulo.Caption := qryArticulosClaves.FieldByName('nombre').Asstring;
       txtArticulo.Text := qryArticulosClaves.FieldByName('clave').AsString;
       txtUnidades.SetFocus;
     end;
  end;

  if Key = VK_RETURN then
  begin
    if qryArticulosClaves.Locate('clave',txtArticulo.Text,[]) then
    begin
    	lblNombreArticulo.Caption := qryArticulosClaves.FieldByName('nombre').Asstring;
      txtUnidades.SetFocus;
    end
    else
    begin
    	MessageDlg('La clave del Articulo no se encuentra registrada.'+#13+
      						'Puede localizar el articulo por medio de la clave o del nombre.'+#13+
                  'Para buscarlo oprima la tecla F4.',mtError,[mbOk],0);
      txtArticulo.SelectAll;
      txtArticulo.SetFocus;
    end;
  end;

end;

end.

