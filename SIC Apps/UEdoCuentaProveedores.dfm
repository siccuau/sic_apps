object EdoCuentaProveedores: TEdoCuentaProveedores
  Left = 0
  Top = 0
  Caption = 'Estados de Cuenta Proveedores'
  ClientHeight = 322
  ClientWidth = 715
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 303
    Width = 715
    Height = 19
    Panels = <
      item
        Width = 50
      end
      item
        Width = 50
      end>
  end
  object GroupBox1: TGroupBox
    Left = 360
    Top = 16
    Width = 347
    Height = 145
    Caption = 'Correo Electr'#243'nico'
    TabOrder = 1
    object Label1: TLabel
      Left = 13
      Top = 24
      Width = 49
      Height = 13
      Caption = 'Remitente'
    end
    object lblDestinatario: TLabel
      Left = 13
      Top = 97
      Width = 58
      Height = 13
      Caption = 'Destinatario'
    end
    object cbCorreos: TComboBox
      Left = 13
      Top = 43
      Width = 281
      Height = 21
      TabOrder = 0
      Text = 'desarrollo@siccomputacion.com'
    end
    object txtDatosCorreo: TEdit
      Left = 13
      Top = 70
      Width = 331
      Height = 21
      Enabled = False
      TabOrder = 1
    end
    object txtDestinatario: TEdit
      Left = 13
      Top = 116
      Width = 297
      Height = 21
      TabOrder = 2
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 16
    Width = 329
    Height = 145
    Caption = 'Proveedores'
    TabOrder = 2
    object rbTodos: TRadioButton
      Left = 24
      Top = 32
      Width = 113
      Height = 17
      Caption = 'Todos'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = rbTodosClick
    end
    object rbUnProveedor: TRadioButton
      Left = 24
      Top = 64
      Width = 113
      Height = 17
      Caption = 'Un Proveedor'
      TabOrder = 1
      OnClick = rbUnProveedorClick
    end
    object cbProveedores: TComboBox
      Left = 40
      Top = 87
      Width = 257
      Height = 21
      Enabled = False
      TabOrder = 2
      Text = 'Bicicletas Antunez'
    end
    object btnActualizarProveedores: TButton
      Left = 24
      Top = 114
      Width = 97
      Height = 25
      Caption = 'Actualizar Datos'
      TabOrder = 3
      OnClick = btnActualizarProveedoresClick
    end
  end
  object btnEnviar: TButton
    Left = 553
    Top = 224
    Width = 75
    Height = 25
    Caption = 'Enviar'
    TabOrder = 3
    OnClick = btnEnviarClick
  end
  object btnVistaPrevia: TButton
    Left = 447
    Top = 224
    Width = 75
    Height = 25
    Caption = 'Vista Previa'
    TabOrder = 4
    OnClick = btnVistaPreviaClick
  end
  object GroupBox3: TGroupBox
    Left = 8
    Top = 176
    Width = 329
    Height = 105
    Caption = 'Opciones'
    TabOrder = 5
    object Label2: TLabel
      Left = 24
      Top = 32
      Width = 119
      Height = 13
      Caption = 'Movimientos a partir de: '
    end
    object dtpFechaInicio: TDateTimePicker
      Left = 24
      Top = 51
      Width = 186
      Height = 21
      Date = 36526.416611192130000000
      Time = 36526.416611192130000000
      TabOrder = 0
      OnChange = dtpFechaInicioChange
    end
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'DriverID=FB'
      'Protocol=TCPIP'
      'User_Name=sysdba'
      'Password=masterkey'
      'Database=C:\Microsip datos 2017\AD2007.FDB'
      'Server=localhost')
    LoginPrompt = False
    Left = 616
    Top = 208
  end
  object qryProveedores: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from proveedores order by nombre')
    Left = 672
    Top = 208
  end
  object BindSourceDB1: TBindSourceDB
    DataSet = qryProveedores
    ScopeMappings = <>
    Left = 24
    Top = 96
  end
  object BindingsList1: TBindingsList
    Methods = <>
    OutputConverters = <>
    Left = 4
    Top = 141
    object LinkListControlToField1: TLinkListControlToField
      Category = 'Quick Bindings'
      DataSource = BindSourceDB1
      FieldName = 'NOMBRE'
      Control = cbProveedores
      FillExpressions = <>
      FillHeaderExpressions = <>
      FillBreakGroups = <>
    end
    object LinkListControlToField2: TLinkListControlToField
      Category = 'Quick Bindings'
      DataSource = BindSourceDB2
      FieldName = 'DESCRIPCION'
      Control = cbCorreos
      FillExpressions = <>
      FillHeaderExpressions = <>
      FillBreakGroups = <>
    end
    object LinkControlToField1: TLinkControlToField
      Category = 'Quick Bindings'
      DataSource = BindSourceDB2
      FieldName = 'MOST'
      Control = txtDatosCorreo
      Track = True
    end
    object LinkControlToField2: TLinkControlToField
      Category = 'Quick Bindings'
      DataSource = BindSourceDB1
      FieldName = 'EMAIL'
      Control = txtDestinatario
      Track = True
    end
  end
  object ConexionConfig: TFDConnection
    Params.Strings = (
      'DriverID=FB'
      'User_Name=sysdba'
      'Password=masterkey'
      'Database=C:\Microsip datos 2017\System\CONFIG.FDB')
    LoginPrompt = False
    Left = 8
    Top = 192
  end
  object qryCorreos: TFDQuery
    Connection = ConexionConfig
    SQL.Strings = (
      'select  cc.cuenta_correo_id,'
      '        cc.dir_correo,'
      '        cc.descripcion,'
      '        cc.dir_correo||'#39' | '#39'||cc.nombre_remitente as most,'
      '        cc."PASSWORD",'
      '        sc.servidor,'
      '        sc.puerto'
      'from cuentas_correo cc join'
      
        'servidores_correo sc on sc.servidor_correo_id = cc.servidor_corr' +
        'eo_id'
      'where cc.tipo = '#39'E'#39)
    Left = 8
    Top = 224
  end
  object BindSourceDB2: TBindSourceDB
    DataSet = qryCorreos
    ScopeMappings = <>
    Left = 664
    Top = 160
  end
  object Reporte: TfrxReport
    Version = '5.4.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 43251.424353044000000000
    ReportOptions.LastChange = 43251.494570995370000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'procedure GroupFooter1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '                                                      '
      'end;'
      ''
      'begin'
      ''
      'end.')
    Left = 96
    Top = 168
    Datasets = <
      item
        DataSet = rptDSEdoCuenta
        DataSetName = 'frxDBDataset1'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 215.900000000000000000
      PaperHeight = 279.400000000000000000
      PaperSize = 1
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 148.212894290000000000
        Top = 18.897650000000000000
        Width = 740.409927000000000000
        object Memo2: TfrxMemoView
          Left = 2.500000000000000000
          Top = 42.411409999999990000
          Width = 736.250000000000000000
          Height = 30.147650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Auxiliar de Movimientos')
          ParentFont = False
        end
        object SysMemo3: TfrxSysMemoView
          Left = 450.000000000000000000
          Top = 124.611935720000000000
          Width = 83.059678570000000000
          Height = 18.897650000000000000
          AutoWidth = True
          DisplayFormat.FormatStr = 'dd mmmm yyyy'
          DisplayFormat.Kind = fkDateTime
          Memo.UTF8W = (
            '[DATE]')
        end
        object SysMemo4: TfrxSysMemoView
          Left = 639.077612860000000000
          Top = 128.897650000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[TIME]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 8.571428570000000000
          Top = 106.816635710000000000
          Width = 162.857142860000000000
          Height = 18.897650000000000000
          Memo.UTF8W = (
            'Movimientos a partir de ')
        end
        object frxDBDataset1FECHA_INICIO: TfrxMemoView
          Left = 178.571428570000000000
          Top = 106.816635710000000000
          Width = 160.798701430000000000
          Height = 18.897650000000000000
          DataField = 'FECHA_INICIO'
          DataSet = rptDSEdoCuenta
          DataSetName = 'frxDBDataset1'
          DisplayFormat.FormatStr = 'dd mmm yyyy'
          DisplayFormat.Kind = fkDateTime
          Memo.UTF8W = (
            '[frxDBDataset1."FECHA_INICIO"]')
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 24.635590000000000000
        Top = 355.275820000000000000
        Width = 740.409927000000000000
        DataSet = rptDSEdoCuenta
        DataSetName = 'frxDBDataset1'
        RowCount = 0
        object frxDBDataset1FECHA: TfrxMemoView
          Left = 44.074830000000000000
          Top = 2.458410000000015000
          Width = 81.870130000000000000
          Height = 18.897650000000000000
          DataField = 'FECHA'
          DataSet = rptDSEdoCuenta
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBDataset1."FECHA"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDBDataset1NOMBRE_CONCEP: TfrxMemoView
          Left = 135.324830000000000000
          Top = 2.458410000000015000
          Width = 108.110390000000000000
          Height = 18.897650000000000000
          DataField = 'NOMBRE_CONCEP'
          DataSet = rptDSEdoCuenta
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBDataset1."NOMBRE_CONCEP"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDBDataset1FOLIO: TfrxMemoView
          Left = 255.502010000000000000
          Top = 2.458410000000015000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'FOLIO'
          DataSet = rptDSEdoCuenta
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBDataset1."FOLIO"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDBDataset1IMPORTE_CARGO: TfrxMemoView
          Left = 451.250000000000000000
          Top = 2.458410000000015000
          Width = 81.988250000000000000
          Height = 18.897650000000000000
          DataField = 'IMPORTE_CARGO'
          DataSet = rptDSEdoCuenta
          DataSetName = 'frxDBDataset1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBDataset1."IMPORTE_CARGO"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDBDataset1IMPORTE_CREDITO: TfrxMemoView
          Left = 551.250000000000000000
          Top = 2.458410000000015000
          Width = 88.238250000000000000
          Height = 18.897650000000000000
          DataField = 'IMPORTE_CREDITO'
          DataSet = rptDSEdoCuenta
          DataSetName = 'frxDBDataset1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBDataset1."IMPORTE_CREDITO"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDBDataset1SALDO_FIN: TfrxMemoView
          Left = 648.750000000000000000
          Top = 2.458410000000015000
          Width = 88.238250000000000000
          Height = 18.897650000000000000
          DataField = 'SALDO_FIN'
          DataSet = rptDSEdoCuenta
          DataSetName = 'frxDBDataset1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBDataset1."SALDO_FIN"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 498.897960000000000000
        Width = 740.409927000000000000
        object Memo1: TfrxMemoView
          Left = 665.197280000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Page#]')
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 47.677180000000000000
        Top = 283.464750000000000000
        Width = 740.409927000000000000
        Condition = 'frxDBDataset1."PROVEEDOR"'
        object frxDBDataset1PROVEEDOR: TfrxMemoView
          Left = 88.750000000000000000
          Top = 0.905379999999979600
          Width = 641.880180000000000000
          Height = 18.897650000000000000
          DataField = 'PROVEEDOR'
          DataSet = rptDSEdoCuenta
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBDataset1."PROVEEDOR"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 46.574830000000000000
          Top = 23.858070000000110000
          Width = 78.238250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Fecha')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 136.574830000000000000
          Top = 23.858070000000110000
          Width = 81.988250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Concepto')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 255.502010000000000000
          Top = 23.858070000000110000
          Width = 78.238250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Folio')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 34.105751430000000000
        Top = 404.409710000000000000
        Width = 740.409927000000000000
        OnAfterPrint = 'GroupFooter1OnAfterPrint'
        object frxDBDataset1SALDO_INI_CXP: TfrxMemoView
          Left = 348.809060000000000000
          Top = 6.722170000000005000
          Width = 93.238250000000000000
          Height = 18.897650000000000000
          DataField = 'SALDO_INI_CXP'
          DataSet = rptDSEdoCuenta
          DataSetName = 'frxDBDataset1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'tahoma'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBDataset1."SALDO_INI_CXP"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDBDataset1SALDO_FINAL_CXP: TfrxMemoView
          Left = 656.250000000000000000
          Top = 6.722170000000005000
          Width = 88.238250000000000000
          Height = 18.897650000000000000
          DataField = 'SALDO_FINAL_CXP'
          DataSet = rptDSEdoCuenta
          DataSetName = 'frxDBDataset1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'tahoma'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBDataset1."SALDO_FINAL_CXP"]')
          ParentFont = False
          WordWrap = False
        end
        object SysMemo1: TfrxSysMemoView
          Left = 452.678571430000000000
          Top = 6.722170000000005000
          Width = 81.988250000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'tahoma'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[(<frxDBDataset1."CARGOS_CXP">)]')
          ParentFont = False
          WordWrap = False
        end
        object SysMemo2: TfrxSysMemoView
          Left = 551.250000000000000000
          Top = 6.722170000000005000
          Width = 88.238250000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'tahoma'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[(<frxDBDataset1."CREDITOS_CXP">)]')
          ParentFont = False
          WordWrap = False
        end
        object Line1: TfrxLineView
          Left = 1.428571430000000000
          Top = 1.957018570000059000
          Width = 734.285714290000000000
          Height = -1.428571430000000000
          Color = clBlack
          Diagonal = True
        end
      end
      object frxDBDataset1NOMBRE_EMPRESA: TfrxMemoView
        Left = 2.500000000000000000
        Top = 18.750000000000000000
        Width = 733.130180000000000000
        Height = 21.754792860000000000
        DataField = 'NOMBRE_EMPRESA'
        DataSet = rptDSEdoCuenta
        DataSetName = 'frxDBDataset1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDBDataset1."NOMBRE_EMPRESA"]')
        ParentFont = False
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 32.677180000000000000
        Top = 188.976500000000000000
        Width = 740.409927000000000000
        object Memo6: TfrxMemoView
          Left = 345.059060000000000000
          Top = 11.171150000000010000
          Width = 93.238250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo inicial')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 451.250000000000000000
          Top = 11.171150000000010000
          Width = 81.988250000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cargos')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 551.250000000000000000
          Top = 11.171150000000010000
          Width = 88.238250000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'ditos')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 648.750000000000000000
          Top = 11.171150000000010000
          Width = 88.238250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo Final')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 16.250000000000000000
          Top = 7.421150000000012000
          Width = 93.238250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Proveedor')
          ParentFont = False
        end
      end
    end
  end
  object rptDSEdoCuenta: TfrxDBDataset
    UserName = 'frxDBDataset1'
    CloseDataSource = False
    DataSet = qryEdoCuenta
    BCDToCurrency = False
    Left = 96
    Top = 216
  end
  object qryEdoCuenta: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select  p.nombre as proveedor,'
      
        '        (select SALDO_ini_CXP  from orsp_cp_aux_prov(p.proveedor' +
        '_id,:F_FECHA_inicio,current_date,'#39'N'#39','#39'N'#39')) as saldo_ini_cxp,'
      '        cp.*,'
      
        '        (select SALDO_FIN_CXP  from orsp_cp_aux_prov(p.proveedor' +
        '_id,:F_FECHA_inicio,current_date,'#39'N'#39','#39'N'#39')) as saldo_final_cxp,'
      
        '        (select cargos_cxp  from orsp_cp_aux_prov(p.proveedor_id' +
        ',:F_FECHA_inicio,current_date,'#39'N'#39','#39'N'#39')) as cargos_cxp,'
      
        '        (select creditos_cxp  from orsp_cp_aux_prov(p.proveedor_' +
        'id,:F_FECHA_inicio,current_date,'#39'N'#39','#39'N'#39')) as creditos_cxp,'
      
        '        (select de.nombre from datos_empresa de) as nombre_empre' +
        'sa,'
      
        '        (select cast(:F_FECHA_inicio as date) from rdb$database)' +
        ' as fecha_inicio'
      'from proveedores p left join'
      
        'auxiliar_prov(p.proveedor_id,:F_FECHA_inicio,current_date,(selec' +
        't SALDO_FIN_CXP  from orsp_cp_aux_prov(p.proveedor_id,'#39'01/01/190' +
        '0'#39',:F_FECHA_inicio,'#39'N'#39','#39'N'#39')) ) cp on 1=1'
      'where not cp.docto_cp_id is null'
      'order by p.nombre,cp.fecha'
      '')
    Left = 144
    Top = 168
    ParamData = <
      item
        Name = 'F_FECHA_INICIO'
        DataType = ftDate
        ParamType = ptInput
        Value = Null
      end>
  end
  object IdSMTP1: TIdSMTP
    IOHandler = SSLHandler
    SASLMechanisms = <>
    UseTLS = utUseExplicitTLS
    Left = 504
    Top = 170
  end
  object SSLHandler: TIdSSLIOHandlerSocketOpenSSL
    Destination = ':25'
    MaxLineAction = maException
    Port = 25
    DefaultPort = 0
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 440
    Top = 170
  end
  object exportpdf: TfrxPDFExport
    ShowDialog = False
    FileName = 'C:\Users\USER\Desktop\prueba.pdf'
    UseFileCache = True
    ShowProgress = False
    OverwritePrompt = False
    CreationTime = 42648.416542048610000000
    DataOnly = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 100
    Transparency = False
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = False
    Left = 616
    Top = 170
  end
  object IdMessage: TIdMessage
    AttachmentEncoding = 'UUE'
    BccList = <>
    CCList = <>
    Encoding = meDefault
    FromList = <
      item
      end>
    Recipients = <>
    ReplyTo = <>
    ConvertPreamble = True
    Left = 552
    Top = 178
  end
end
