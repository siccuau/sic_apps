object EliminaHistorial: TEliminaHistorial
  Left = 0
  Top = 0
  Caption = 'EliminaHistorial'
  ClientHeight = 242
  ClientWidth = 527
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object lblProgreso: TLabel
    Left = 240
    Top = 174
    Width = 53
    Height = 13
    Caption = 'lblProgreso'
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 223
    Width = 527
    Height = 19
    Panels = <
      item
        Width = 50
      end>
    ExplicitLeft = -278
    ExplicitWidth = 805
  end
  object btnEliminar: TButton
    Left = 216
    Top = 54
    Width = 129
    Height = 25
    Caption = 'Eliminar'
    TabOrder = 1
    OnClick = btnEliminarClick
  end
  object txtAnio: TEdit
    Left = 32
    Top = 56
    Width = 121
    Height = 21
    Alignment = taRightJustify
    TabOrder = 2
  end
  object pbProgreso: TProgressBar
    Left = 32
    Top = 104
    Width = 457
    Height = 64
    Step = 1
    TabOrder = 3
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'DriverID=FB'
      'User_Name=sysdba'
      'Password=masterkey'
      'Database=C:\Microsip datos 2017\ESTACION DE SERVICIO FERES.FDB'
      'Server=localhost'
      'Protocol=TCPIP')
    Connected = True
    LoginPrompt = False
    Left = 452
    Top = 8
  end
  object tbDoctosCC: TFDTable
    Active = True
    IndexFieldNames = 'DOCTO_CC_ID'
    Connection = Conexion
    UpdateOptions.UpdateTableName = 'DOCTOS_CC'
    TableName = 'DOCTOS_CC'
    Left = 352
    Top = 16
  end
end
