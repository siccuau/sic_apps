object MonitorBitacora: TMonitorBitacora
  Left = 0
  Top = 0
  Caption = 'Monitor Bitacora'
  ClientHeight = 577
  ClientWidth = 683
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbl_restante: TLabel
    Left = 35
    Top = 37
    Width = 3
    Height = 13
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 558
    Width = 683
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object StaticText1: TStaticText
    Left = 35
    Top = 8
    Width = 104
    Height = 23
    Caption = 'Ejecutar cada:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object num_periodo: TEdit
    Left = 145
    Top = 8
    Width = 21
    Height = 21
    TabOrder = 2
    Text = '0'
  end
  object UpDown1: TUpDown
    Left = 166
    Top = 8
    Width = 16
    Height = 21
    Associate = num_periodo
    TabOrder = 3
  end
  object cb_periodo: TComboBox
    Left = 186
    Top = 8
    Width = 90
    Height = 21
    ItemIndex = 0
    TabOrder = 4
    Text = 'Minutos'
    Items.Strings = (
      'Minutos'
      'Horas'
      'Dias')
  end
  object btn_iniciar: TButton
    Left = 305
    Top = 8
    Width = 75
    Height = 57
    Caption = 'Iniciar'
    TabOrder = 5
    OnClick = btn_iniciarClick
  end
  object grdBitacora: TStringGrid
    Left = 8
    Top = 104
    Width = 665
    Height = 448
    ColCount = 6
    FixedCols = 0
    RowCount = 2
    TabOrder = 6
    ColWidths = (
      64
      64
      64
      64
      64
      64)
  end
  object DateTimePicker1: TDateTimePicker
    Left = 35
    Top = 56
    Width = 118
    Height = 21
    Date = 43994.674353576390000000
    Time = 43994.674353576390000000
    TabOrder = 7
  end
  object StaticText2: TStaticText
    Left = 159
    Top = 56
    Width = 16
    Height = 23
    Caption = 'al'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
  end
  object DateTimePicker2: TDateTimePicker
    Left = 181
    Top = 56
    Width = 118
    Height = 21
    Date = 43994.674353576390000000
    Time = 43994.674353576390000000
    TabOrder = 9
  end
  object Button1: TButton
    Left = 536
    Top = 6
    Width = 129
    Height = 25
    Caption = 'Enviar Ahora'
    TabOrder = 10
    OnClick = Button1Click
  end
  object Timer_reorden: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer_reordenTimer
    Left = 64
    Top = 118
  end
  object Timer_restante: TTimer
    Enabled = False
    Interval = 200
    OnTimer = Timer_restanteTimer
    Left = 120
    Top = 110
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'Database=E:\Microsip Pruebas\SIC 2019.FDB'
      'DriverID=FB')
    Left = 168
    Top = 110
  end
  object SSLHandler: TIdSSLIOHandlerSocketOpenSSL
    Destination = ':25'
    MaxLineAction = maException
    Port = 25
    DefaultPort = 0
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 419
    Top = 10
  end
  object IdSMTP1: TIdSMTP
    IOHandler = SSLHandler
    SASLMechanisms = <>
    UseTLS = utUseExplicitTLS
    Left = 451
    Top = 58
  end
  object IdMessage: TIdMessage
    AttachmentEncoding = 'UUE'
    BccList = <>
    CCList = <>
    Encoding = meDefault
    FromList = <
      item
      end>
    Recipients = <>
    ReplyTo = <>
    ConvertPreamble = True
    Left = 411
    Top = 58
  end
  object qryBitacora: TFDQuery
    Connection = Conexion
    Left = 16
    Top = 96
  end
  object MainMenu1: TMainMenu
    Left = 528
    Top = 48
    object Configuracion1: TMenuItem
      Caption = 'Configuracion'
      OnClick = Configuracion1Click
    end
    object Salir1: TMenuItem
      Caption = 'Salir'
      OnClick = Salir1Click
    end
  end
  object qryEmpresas: TFDQuery
    Connection = Conexion
    Left = 472
  end
end
