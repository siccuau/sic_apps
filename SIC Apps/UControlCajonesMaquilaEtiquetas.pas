unit UControlCajonesMaquilaEtiquetas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, Vcl.ComCtrls, Data.DB, FireDAC.Comp.Client, Vcl.DBCtrls,
  Vcl.StdCtrls, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, frxClass, Vcl.Samples.Spin, frxDBSet, vcl.Printers;


type
  TControlCajonesMaquilaEtiquetas = class(TForm)
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    cmbEmpacadoras: TDBLookupComboBox;
    DSEmpacadoras: TDataSource;
    Query: TFDQuery;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    btnImprimir: TButton;
    Etiquetas: TfrxReport;
    txtInicio: TSpinEdit;
    txtNumEtiquetas: TSpinEdit;
    DSEtiquetas: TfrxDBDataset;
    QueryEtiquetas: TFDQuery;
    cmbImpresoras: TComboBox;
    Label4: TLabel;    procedure FormShow(Sender: TObject);
    procedure cmbEmpacadorasClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure cmbImpresorasChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ControlCajonesMaquilaEtiquetas: TControlCajonesMaquilaEtiquetas;

implementation

{$R *.dfm}

procedure TControlCajonesMaquilaEtiquetas.btnImprimirClick(Sender: TObject);
var
  consec,num_et : integer;
begin
  consec := txtInicio.Value;
  num_et := txtNumEtiquetas.Value;
  Conexion.ExecSQL('update sic_empacadoras set consec='+inttostr(consec+num_et)+' where id='+INTTOSTR(cmbEmpacadoras.KeyValue));

  QueryEtiquetas.sql.Text := ' select a.*,'+INTTOSTR(cmbEmpacadoras.KeyValue)+' AS EMPACADORA from'+
                            ' (with recursive n as ('+
                                  ' select '+inttostr(consec)+' as n'+
                                  ' from rdb$database'+
                                  ' union all'+
                                  ' select n.n + 1'+
                                  ' from n'+
                                  ' where n < '+IntToStr(consec+num_et-1)+''+
                                 ' )'+
                            ' select n.n'+
                            ' from n) a';
  QueryEtiquetas.Open;

  Etiquetas.PrepareReport(true);
  Etiquetas.PrintOptions.Printer := Printer.Printers[Printer.PrinterIndex];
  //Etiquetas.ShowReport;
  Etiquetas.Print;
  txtNumEtiquetas.Value := 1;
  txtInicio.Value := consec+num_et;

end;

procedure TControlCajonesMaquilaEtiquetas.cmbEmpacadorasClick(Sender: TObject);
begin
  txtInicio.Value := Conexion.ExecSQLScalar('select consec from sic_empacadoras where id=:eid',[cmbEmpacadoras.KeyValue]);
end;

procedure TControlCajonesMaquilaEtiquetas.cmbImpresorasChange(Sender: TObject);
begin
  Printer.PrinterIndex := cmbImpresoras.ItemIndex;
end;

procedure TControlCajonesMaquilaEtiquetas.FormCreate(Sender: TObject);
begin
  SetWindowLong(Handle, GWL_EXSTYLE, WS_EX_APPWINDOW);
end;

procedure TControlCajonesMaquilaEtiquetas.FormShow(Sender: TObject);
begin
  Query.Open;
  cmbEmpacadoras.KeyValue := Conexion.ExecSQLScalar('select first 1 id from sic_empacadoras');
  txtInicio.Value := Conexion.ExecSQLScalar('select consec from sic_empacadoras where id=:eid',[cmbEmpacadoras.KeyValue]);
  cmbImpresoras.Items.Clear;
  cmbImpresoras.Items.Assign(Printer.Printers);
  cmbImpresoras.ItemIndex := 0;
  Printer.PrinterIndex := 0;
end;

end.
