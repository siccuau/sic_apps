unit UCambioCostos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, Data.DB, FireDAC.Comp.Client, Vcl.StdCtrls, UBusqueda,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.VCLUI.Wait, Vcl.ComCtrls;

type
  TCambioCostos = class(TForm)
    Conexion: TFDConnection;
    btn_cancelar: TButton;
    txt_costo_anterior: TEdit;
    txt_costo_nuevo: TEdit;
    btn_guardar: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Query: TFDQuery;
    lbl_nombre_articulo: TLabel;
    StatusBar1: TStatusBar;
    Label3: TLabel;
    qryDoctosIN: TFDQuery;
    qryDoctosInDet: TFDQuery;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn_cancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure txt_costo_nuevoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btn_guardarClick(Sender: TObject);
    procedure btn_guardarKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    articulo_id : Integer;
  end;

var
  CambioCostos: TCambioCostos;

implementation

{$R *.dfm}

procedure TCambioCostos.btn_cancelarClick(Sender: TObject);
begin
   Application.Terminate;
end;

procedure TCambioCostos.btn_guardarClick(Sender: TObject);
var
  query_str:string;
begin
  query_str := 'update articulos set costo_ultima_compra = :costo where articulo_id = :articulo_id';
  Query.ExecSQL(query_str,[txt_costo_nuevo.Text,articulo_id]);
  showmessage('Costo Actualizado');
  txt_costo_nuevo.Clear;
  txt_costo_anterior.Clear;
  lbl_nombre_articulo.Caption := '';
  txt_costo_nuevo.SetFocus;
end;

procedure TCambioCostos.btn_guardarKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  FormBusquedaArticulos : TBusquedaArticulos;
begin
  if Key = VK_F4 then
    begin
       FormBusquedaArticulos := TbusquedaArticulos.Create(nil);
       if FormBusquedaArticulos.ShowModal = mrOK then
       begin
         articulo_id := FormBusquedaArticulos.articulo_id;
         Query.SQL.Text := 'select nombre, (select costo_ultima_compra from get_ultcom_art(a.articulo_id)) as costo_ultima_compra'+
         									' from articulos a where articulo_id='+inttostr(articulo_id);
         Query.Open;
         lbl_nombre_articulo.Caption := Query.FieldByName('nombre').Value;
         txt_costo_anterior.Text := Query.FieldByName('costo_ultima_compra').Value;
         txt_costo_nuevo.SetFocus;
       end;
    end;
end;

procedure TCambioCostos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Application.Terminate;
end;

procedure TCambioCostos.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var
  FormBusquedaArticulos : TBusquedaArticulos;
begin
  if Key = VK_F4 then
    begin
      FormBusquedaArticulos := TbusquedaArticulos.Create(nil);
      FormBusquedaArticulos.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
      FormBusquedaArticulos.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
      FormBusquedaArticulos.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
      FormBusquedaArticulos.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
      FormBusquedaArticulos.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
       if FormBusquedaArticulos.ShowModal = mrOK then
       begin
         articulo_id := FormBusquedaArticulos.articulo_id;
         Query.SQL.Text := 'select nombre, (select costo_ultima_compra from get_ultcom_art(a.articulo_id)) as costo_ultima_compra'+
         									' from articulos a where articulo_id='+inttostr(articulo_id);
         Query.Open;
         lbl_nombre_articulo.Caption := Query.FieldByName('nombre').Value;
         txt_costo_anterior.Text := Query.FieldByName('costo_ultima_compra').Value;
         txt_costo_nuevo.SetFocus;
       end;
    end;
end;

procedure TCambioCostos.FormShow(Sender: TObject);
begin
Conexion.Connected := True;

end;

procedure TCambioCostos.txt_costo_nuevoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var
  FormBusquedaArticulos : TBusquedaArticulos;
begin
    if Key = VK_F4 then
    begin
       FormBusquedaArticulos := TbusquedaArticulos.Create(nil);
       FormBusquedaArticulos.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
      FormBusquedaArticulos.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
      FormBusquedaArticulos.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
      FormBusquedaArticulos.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
      FormBusquedaArticulos.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
       if FormBusquedaArticulos.ShowModal = mrOK then
       begin
         articulo_id := FormBusquedaArticulos.articulo_id;
         Query.SQL.Text := 'select nombre, (select costo_ultima_compra from get_ultcom_art(a.articulo_id)) as costo_ultima_compra'+
         									' from articulos a where articulo_id='+inttostr(articulo_id);
         Query.Open;
         lbl_nombre_articulo.Caption := Query.FieldByName('nombre').Value;
         txt_costo_anterior.Text := Query.FieldByName('costo_ultima_compra').Value;
         txt_costo_nuevo.SetFocus;
       end;
    end;
end;

end.
