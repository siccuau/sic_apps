object FormPreciosFiltro: TFormPreciosFiltro
  Left = 0
  Top = 0
  Caption = 'FormPreciosFiltro'
  ClientHeight = 337
  ClientWidth = 603
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 8
    Top = 43
    Width = 33
    Height = 13
    Caption = 'Precio:'
  end
  object txtBusqueda: TEdit
    Left = 8
    Top = 8
    Width = 281
    Height = 21
    TabOrder = 0
  end
  object rb_nombre: TRadioButton
    Left = 438
    Top = 8
    Width = 65
    Height = 17
    Caption = 'Nombre'
    Checked = True
    TabOrder = 1
    TabStop = True
  end
  object rb_clave: TRadioButton
    Left = 344
    Top = 8
    Width = 73
    Height = 17
    Caption = 'Clave'
    TabOrder = 2
  end
  object db_articulos: TDBGrid
    Left = 8
    Top = 67
    Width = 561
    Height = 190
    Ctl3D = False
    DataSource = DataSource1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgRowLines, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgTitleHotTrack]
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    ReadOnly = True
    ShowHint = True
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object btnCotizar: TButton
    Left = 230
    Top = 269
    Width = 118
    Height = 25
    Caption = '&Cambiar Precios'
    TabOrder = 4
  end
  object cb_precios: TDBLookupComboBox
    Left = 55
    Top = 39
    Width = 211
    Height = 21
    KeyField = 'PRECIO_EMPRESA_ID'
    ListField = 'NOMBRE'
    ListSource = dsPrecios
    TabOrder = 5
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 312
    Width = 603
    Height = 25
    Panels = <
      item
        Text = '<< AD2007 >>'
        Width = 80
      end>
    ExplicitTop = 302
    ExplicitWidth = 577
  end
  object DataSource1: TDataSource
    DataSet = Query
    Left = 259
    Top = 96
  end
  object Query: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select ar.articulo_id, ca.clave_articulo as clave, ar.nombre, in' +
        'v.inv_fin_unid from articulos ar left join'
      'claves_articulos ca on ca.articulo_id = ar.articulo_id left JOIN'
      
        'orsp_in_aux_art(ar.articulo_id,(select nombre from almacenes al ' +
        'where al.es_ppal='#39'S'#39'),'#39'01/01/1990'#39',current_date, '#39'N'#39','#39'N'#39') inv on' +
        ' inv.articulo_id = ar.articulo_id'
      
        'where ca.rol_clave_art_id =17 and upper(nombre) like Upper('#39'%%'#39')' +
        ';'
      '')
    Left = 211
    Top = 96
  end
  object Transaction: TFDTransaction
    Connection = Conexion
    Left = 483
    Top = 136
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'DriverID=FB'
      'Protocol=TCPIP'
      'User_Name=sysdba'
      'Password=1'
      'Database=C:\Microsip datos\2015.FDB'
      'Server=localhost')
    Connected = True
    LoginPrompt = False
    Transaction = Transaction
    Left = 483
    Top = 80
  end
  object QueryAlmacenes: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from almacenes')
    Left = 208
    Top = 152
  end
  object dsAlmacenes: TDataSource
    DataSet = QueryAlmacenes
    Left = 256
    Top = 152
  end
  object dsPrecios: TDataSource
    DataSet = QueryPrecios
    Left = 256
    Top = 200
  end
  object QueryPrecios: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from precios_empresa')
    Left = 208
    Top = 200
  end
end
