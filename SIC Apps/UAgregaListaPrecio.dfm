object AgregaListaPrecio: TAgregaListaPrecio
  Left = 0
  Top = 0
  Caption = 'AgregaListaPrecio'
  ClientHeight = 249
  ClientWidth = 320
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 84
    Top = 24
    Width = 152
    Height = 13
    Caption = 'Selecciona la lista a Aregar'
    Color = clBackground
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label2: TLabel
    Left = 132
    Top = 88
    Width = 56
    Height = 13
    Caption = 'En base a:'
    Color = clBackground
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label3: TLabel
    Left = 28
    Top = 137
    Width = 49
    Height = 13
    Caption = 'Mas un : '
    Color = clBackground
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label4: TLabel
    Left = 167
    Top = 137
    Width = 21
    Height = 13
    Caption = '%'
    Color = clBackground
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object DBprecios: TDBLookupComboBox
    Left = 28
    Top = 43
    Width = 265
    Height = 21
    KeyField = 'PRECIO_EMPRESA_ID'
    ListField = 'NOMBRE'
    ListSource = DataSource
    TabOrder = 0
  end
  object Button1: TButton
    Left = 39
    Top = 172
    Width = 75
    Height = 25
    Caption = '&Aceptar'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 165
    Top = 172
    Width = 75
    Height = 25
    Caption = '&Salir'
    TabOrder = 2
    OnClick = Button2Click
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 230
    Width = 320
    Height = 19
    Panels = <
      item
        Width = 50
      end>
    ExplicitLeft = -16
    ExplicitTop = 197
    ExplicitWidth = 355
  end
  object DBpreciosbase: TDBLookupComboBox
    Left = 31
    Top = 107
    Width = 265
    Height = 21
    KeyField = 'PRECIO_EMPRESA_ID'
    ListField = 'NOMBRE'
    ListSource = DSBase
    TabOrder = 4
  end
  object txtPctje: TEdit
    Left = 96
    Top = 134
    Width = 65
    Height = 21
    TabOrder = 5
  end
  object DataSource: TDataSource
    DataSet = Query
    Left = 75
    Top = 80
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'DriverID=FB'
      'Protocol=TCPIP'
      'User_Name=sysdba'
      'Password=masterkey')
    ResourceOptions.AssignedValues = [rvKeepConnection]
    ResourceOptions.KeepConnection = False
    LoginPrompt = False
    Transaction = FDTransaction
    Left = 127
    Top = 80
  end
  object Query: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from precios_empresa a')
    Left = 179
    Top = 80
  end
  object FDTransaction: TFDTransaction
    Connection = Conexion
    Left = 23
    Top = 80
  end
  object QueryPrecios: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select a.articulo_id from articulos a order by nombre')
    Left = 235
    Top = 80
  end
  object DSBase: TDataSource
    DataSet = QueryBase
    Left = 75
    Top = 48
  end
  object QueryBase: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from precios_empresa a')
    Left = 179
    Top = 48
  end
end
