unit UPreciosFiltro;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, Vcl.ComCtrls, Data.DB, FireDAC.Comp.Client,
  FireDAC.Comp.DataSet, Vcl.DBCtrls, Vcl.StdCtrls, Vcl.Grids, Vcl.DBGrids;

type
  TFormPreciosFiltro = class(TForm)
    Label2: TLabel;
    txtBusqueda: TEdit;
    rb_nombre: TRadioButton;
    rb_clave: TRadioButton;
    db_articulos: TDBGrid;
    btnCotizar: TButton;
    cb_precios: TDBLookupComboBox;
    DataSource1: TDataSource;
    Query: TFDQuery;
    Transaction: TFDTransaction;
    Conexion: TFDConnection;
    QueryAlmacenes: TFDQuery;
    dsAlmacenes: TDataSource;
    dsPrecios: TDataSource;
    QueryPrecios: TFDQuery;
    StatusBar1: TStatusBar;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormPreciosFiltro: TFormPreciosFiltro;

implementation

{$R *.dfm}

end.
