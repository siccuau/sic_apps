object EmpresaBitacora: TEmpresaBitacora
  Left = 0
  Top = 0
  Caption = 'Empresa Principal'
  ClientHeight = 265
  ClientWidth = 319
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 6
    Width = 144
    Height = 13
    Caption = 'Seleccionar empresa principal:'
  end
  object Label2: TLabel
    Left = 16
    Top = 232
    Width = 270
    Height = 13
    Caption = 'En la empresa principal se guardara el correo electronico'
  end
  object Label3: TLabel
    Left = 16
    Top = 246
    Width = 128
    Height = 13
    Caption = ' y la contrase'#241'a del emisor'
  end
  object lbEmpresas: TListBox
    Left = 16
    Top = 25
    Width = 217
    Height = 201
    ItemHeight = 13
    TabOrder = 0
  end
  object btnAceptar: TButton
    Left = 239
    Top = 8
    Width = 72
    Height = 23
    Caption = 'Aceptar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Pitch = fpVariable
    Font.Style = []
    Font.Quality = fqDraft
    ParentFont = False
    TabOrder = 1
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 239
    Top = 37
    Width = 72
    Height = 22
    Caption = 'Cancelar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Pitch = fpVariable
    Font.Style = []
    Font.Quality = fqDraft
    ParentFont = False
    TabOrder = 2
    OnClick = btnCancelarClick
  end
  object Conexion: TFDConnection
    Left = 448
    Top = 192
  end
end
