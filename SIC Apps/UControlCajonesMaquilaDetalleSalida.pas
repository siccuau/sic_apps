unit UControlCajonesMaquilaDetalleSalida;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, Data.DB, FireDAC.Comp.Client, Vcl.ComCtrls, Vcl.StdCtrls,
  Vcl.Grids;

type
  TControlCajonesMaquilaDetalleSalida = class(TForm)
    StatusBar1: TStatusBar;
    Conexion: TFDConnection;
    Label1: TLabel;
    Label2: TLabel;
    Label13: TLabel;
    Label5: TLabel;
    Label3: TLabel;
    txtFolioMaquila: TEdit;
    txtProveedor: TEdit;
    txtFecha: TEdit;
    Edit3: TEdit;
    Edit1: TEdit;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Edit2: TEdit;
    Edit4: TEdit;
    Label6: TLabel;
    GroupBox2: TGroupBox;
    StringGrid1: TStringGrid;
    Button1: TButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ControlCajonesMaquilaDetalleSalida: TControlCajonesMaquilaDetalleSalida;

implementation

{$R *.dfm}

end.
