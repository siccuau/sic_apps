unit UControlCajonesPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, Vcl.ComCtrls, Data.DB, FireDAC.Comp.Client, Vcl.ImgList,
  Vcl.StdCtrls,UControlCajonesMaquila,UControlCajonesLista, FireDAC.VCLUI.Wait,
  System.ImageList;

type
  TControlCajonesPrincipal = class(TForm)
    btnMaquila: TButton;
    btnCajones: TButton;
    ImageList1: TImageList;
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    procedure btnMaquilaClick(Sender: TObject);
    procedure btnCajonesClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ControlCajonesPrincipal: TControlCajonesPrincipal;

implementation

{$R *.dfm}

procedure TControlCajonesPrincipal.btnCajonesClick(Sender: TObject);
var
  FormCajones:TControlCajonesLista;
begin
  FormCajones := TControlCajonesLista.Create(nil);
  FormCajones.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormCajones.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormCajones.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormCajones.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormCajones.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';

  FormCajones.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
  FormCajones.ShowModal;
end;

procedure TControlCajonesPrincipal.btnMaquilaClick(Sender: TObject);
var
  FormMaquila:TControlCajonesMaquila;
begin
  FormMaquila := TControlCajonesMaquila.Create(nil);
  FormMaquila.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormMaquila.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormMaquila.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormMaquila.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormMaquila.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';

  FormMaquila.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
  FormMaquila.ShowModal;
end;

procedure TControlCajonesPrincipal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Conexion.Close;
  Application.Terminate;
end;

procedure TControlCajonesPrincipal.FormCreate(Sender: TObject);
begin
  SetWindowLong(Handle, GWL_EXSTYLE, WS_EX_APPWINDOW);
end;

end.
