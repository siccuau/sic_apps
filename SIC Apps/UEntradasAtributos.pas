unit UEntradasAtributos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, Data.DB, FireDAC.Comp.Client, Vcl.StdCtrls, Vcl.DBCtrls;

type
  TEntradasAtributos = class(TForm)
    cb_at1: TDBLookupComboBox;
    cb_at2: TDBLookupComboBox;
    txtUnidades: TEdit;
    btnEnviar: TButton;
    FDConnection1: TFDConnection;
    Label1: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EntradasAtributos: TEntradasAtributos;

implementation

{$R *.dfm}

end.
