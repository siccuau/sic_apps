object FormLicenciaProv: TFormLicenciaProv
  Left = 0
  Top = 0
  Caption = 'Licencia'
  ClientHeight = 242
  ClientWidth = 230
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object txtLicencia: TEdit
    Left = 56
    Top = 48
    Width = 137
    Height = 21
    Alignment = taRightJustify
    PasswordChar = '*'
    TabOrder = 0
  end
  object btnVerificar: TButton
    Left = 90
    Top = 128
    Width = 75
    Height = 25
    Caption = 'Verificar'
    TabOrder = 1
    OnClick = btnVerificarClick
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 10
    OnTimer = Timer1Timer
    Left = 40
    Top = 96
  end
end
