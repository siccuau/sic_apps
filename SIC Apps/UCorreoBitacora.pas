unit UCorreoBitacora;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Vcl.StdCtrls, Data.DB,
  FireDAC.Comp.Client;

type
  TCorreoBitacora = class(TForm)
    Conexion: TFDConnection;
    Label1: TLabel;
    txtCorreo: TEdit;
    Label2: TLabel;
    txtContrasena: TEdit;
    btnGuardar: TButton;
    Label3: TLabel;
    cbServidor: TComboBox;
    btnCancelar: TButton;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    cb_cc: TCheckBox;
    cb_in: TCheckBox;
    cb_ve: TCheckBox;
    cb_cp: TCheckBox;
    cb_cm: TCheckBox;
    cb_pv: TCheckBox;
    cb_co: TCheckBox;
    cb_ba: TCheckBox;
    cb_no: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure cb_ccClick(Sender: TObject);
    procedure cb_inClick(Sender: TObject);
    procedure cb_veClick(Sender: TObject);
    procedure cb_cmClick(Sender: TObject);
    procedure cb_pvClick(Sender: TObject);
    procedure cb_coClick(Sender: TObject);
    procedure cb_baClick(Sender: TObject);
    procedure cb_noClick(Sender: TObject);
    procedure cb_cpClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CorreoBitacora: TCorreoBitacora;

implementation

{$R *.dfm}

procedure TCorreoBitacora.btnCancelarClick(Sender: TObject);
begin
self.Close;
end;

procedure TCorreoBitacora.btnGuardarClick(Sender: TObject);
var guardado:boolean;
begin
//CORREO
 if Conexion.ExecSQLScalar('select count(valor) from REGISTRY where nombre=''SIC_CORREO_BITACORA''')=0 then
   begin
    try
    Conexion.ExecSQL('insert into Registry(elemento_id,nombre,tipo,padre_id,valor) values(-1,''SIC_CORREO_BITACORA'',''V'',0,:valor)',[txtCorreo.Text]);
    Conexion.Commit;
    guardado:=true;
     except
     Conexion.Rollback;
     guardado:=false;
     end;
   end
   else
   begin
    try
      Conexion.ExecSQL('update Registry set valor=:valor where nombre=''SIC_CORREO_BITACORA''',[txtCorreo.Text]);
      Conexion.Commit;
      guardado:=true;
     except
     Conexion.Rollback;
     guardado:=false;
     end;
   end;

    //CONTRASEŅA
    if Conexion.ExecSQLScalar('select count(valor) from REGISTRY where nombre=''SIC_CONTRA_BITACORA''')=0 then
   begin
    try
    Conexion.ExecSQL('insert into Registry(elemento_id,nombre,tipo,padre_id,valor) values(-1,''SIC_CONTRA_BITACORA'',''V'',0,:valor)',[txtContrasena.Text]);
    Conexion.Commit;
    guardado:=true;
     except
     Conexion.Rollback;
     guardado:=false;
     end;
   end
   else
   begin
    try
      Conexion.ExecSQL('update Registry set valor=:valor where nombre=''SIC_CONTRA_BITACORA''',[txtContrasena.Text]);
      Conexion.Commit;
      guardado:=true;
     except
     Conexion.Rollback;
     guardado:=false;
     end;
   end;

   //SERVIDOR
      //CONTRASEŅA
    if Conexion.ExecSQLScalar('select count(valor) from registry where nombre=''SIC_TIPO_BITACORA''')=0 then
   begin
   if cbServidor.ItemIndex=0 then
    begin
      try
      Conexion.ExecSQL('insert into Registry(elemento_id,nombre,tipo,padre_id,valor) values(-1,''SIC_TIPO_BITACORA'',''V'',0,''HOTMAIL'')');
      Conexion.Commit;
      guardado:=true;
       except
       Conexion.Rollback;
       guardado:=false;
       end;
    end
    else
    begin
          try
      Conexion.ExecSQL('insert into Registry(elemento_id,nombre,tipo,padre_id,valor) values(-1,''SIC_TIPO_BITACORA'',''V'',0,''GMAIL'')');
      Conexion.Commit;
      guardado:=true;
       except
       Conexion.Rollback;
       guardado:=false;
       end;
    end;
   end
   else
   begin
   if cbServidor.ItemIndex=0 then
    begin
     try
      Conexion.ExecSQL('update Registry set valor=''HOTMAIL'' where nombre=''SIC_TIPO_BITACORA''');
      Conexion.Commit;
      guardado:=true;
     except
     Conexion.Rollback;
     guardado:=false;
     end;
    end
    else
    begin
         try
      Conexion.ExecSQL('update Registry set valor=''GMAIL'' where nombre=''SIC_TIPO_BITACORA''');
      Conexion.Commit;
      guardado:=true;
     except
     Conexion.Rollback;
     guardado:=false;
     end;
    end;
   end;

   if guardado=true then
   begin
   ShowMessage('Guardado correctamente.');
   self.Close;
   end
   else ShowMessage('ERROR al guardar los datos.');

end;

procedure TCorreoBitacora.cb_baClick(Sender: TObject);
begin
    if cb_ba.Checked=false then
   begin
   Conexion.ExecSQL('update SIC_BITACORA_CONFIG set doctos_ba=''N''');
   Conexion.Commit;
   end
   else
   begin
   Conexion.ExecSQL('update SIC_BITACORA_CONFIG set doctos_ba=''S''');
   Conexion.Commit;
   end;
end;

procedure TCorreoBitacora.cb_ccClick(Sender: TObject);
begin
    if cb_cc.Checked=false then
   begin
   Conexion.ExecSQL('update SIC_BITACORA_CONFIG set doctos_cc=''N''');
   Conexion.Commit;
   end
   else
   begin
   Conexion.ExecSQL('update SIC_BITACORA_CONFIG set doctos_cc=''S''');
   Conexion.Commit;
   end;
end;

procedure TCorreoBitacora.cb_cmClick(Sender: TObject);
begin
    if cb_cm.Checked=false then
   begin
   Conexion.ExecSQL('update SIC_BITACORA_CONFIG set doctos_cm=''N''');
   Conexion.Commit;
   end
   else
   begin
   Conexion.ExecSQL('update SIC_BITACORA_CONFIG set doctos_cm=''S''');
   Conexion.Commit;
   end;
end;

procedure TCorreoBitacora.cb_coClick(Sender: TObject);
begin
    if cb_co.Checked=false then
   begin
   Conexion.ExecSQL('update SIC_BITACORA_CONFIG set doctos_co=''N''');
   Conexion.Commit;
   end
   else
   begin
   Conexion.ExecSQL('update SIC_BITACORA_CONFIG set doctos_co=''S''');
   Conexion.Commit;
   end;
end;

procedure TCorreoBitacora.cb_cpClick(Sender: TObject);
begin
    if cb_cp.Checked=false then
   begin
   Conexion.ExecSQL('update SIC_BITACORA_CONFIG set doctos_cp=''N''');
   Conexion.Commit;
   end
   else
   begin
   Conexion.ExecSQL('update SIC_BITACORA_CONFIG set doctos_cp=''S''');
   Conexion.Commit;
   end;
end;

procedure TCorreoBitacora.cb_inClick(Sender: TObject);
begin
    if cb_in.Checked=false then
   begin
   Conexion.ExecSQL('update SIC_BITACORA_CONFIG set doctos_in=''N''');
   Conexion.Commit;
   end
   else
   begin
   Conexion.ExecSQL('update SIC_BITACORA_CONFIG set doctos_in=''S''');
   Conexion.Commit;
   end;
end;

procedure TCorreoBitacora.cb_noClick(Sender: TObject);
begin
    if cb_no.Checked=false then
   begin
   Conexion.ExecSQL('update SIC_BITACORA_CONFIG set doctos_no=''N''');
   Conexion.Commit;
   end
   else
   begin
   Conexion.ExecSQL('update SIC_BITACORA_CONFIG set doctos_no=''S''');
   Conexion.Commit;
   end;
end;

procedure TCorreoBitacora.cb_pvClick(Sender: TObject);
begin
    if cb_pv.Checked=false then
   begin
   Conexion.ExecSQL('update SIC_BITACORA_CONFIG set doctos_pv=''N''');
   Conexion.Commit;
   end
   else
   begin
   Conexion.ExecSQL('update SIC_BITACORA_CONFIG set doctos_pv=''S''');
   Conexion.Commit;
   end;
end;

procedure TCorreoBitacora.cb_veClick(Sender: TObject);
begin
    if cb_ve.Checked=false then
   begin
   Conexion.ExecSQL('update SIC_BITACORA_CONFIG set doctos_ve=''N''');
   Conexion.Commit;
   end
   else
   begin
   Conexion.ExecSQL('update SIC_BITACORA_CONFIG set doctos_ve=''S''');
   Conexion.Commit;
   end;
end;

procedure TCorreoBitacora.FormShow(Sender: TObject);
begin
  if Conexion.ExecSQLScalar('select count(valor) from REGISTRY where nombre=''SIC_CORREO_BITACORA''')<>0 then
   begin
   txtCorreo.Text:=Conexion.ExecSQLScalar('select valor from REGISTRY where nombre=''SIC_CORREO_BITACORA''');
   end;

   if Conexion.ExecSQLScalar('select count(valor) from REGISTRY where nombre=''SIC_CONTRA_BITACORA''')<>0 then
   begin
   txtContrasena.Text:=Conexion.ExecSQLScalar('select valor from REGISTRY where nombre=''SIC_CONTRA_BITACORA''');
   end;

   if Conexion.ExecSQLScalar('select valor from REGISTRY where nombre=''SIC_TIPO_BITACORA''')='GMAIL' then
   cbServidor.ItemIndex:=1;

     if Conexion.ExecSQLScalar('select doctos_ba from SIC_BITACORA_CONFIG')='S' then
   begin
   cb_ba.Checked:=true;
   end;

        if Conexion.ExecSQLScalar('select doctos_cm from SIC_BITACORA_CONFIG')='S' then
   begin
   cb_cm.Checked:=true;
   end;

        if Conexion.ExecSQLScalar('select doctos_co from SIC_BITACORA_CONFIG')='S' then
   begin
   cb_co.Checked:=true;
   end;

        if Conexion.ExecSQLScalar('select doctos_cp from SIC_BITACORA_CONFIG')='S' then
   begin
   cb_cp.Checked:=true;
   end;

        if Conexion.ExecSQLScalar('select doctos_in from SIC_BITACORA_CONFIG')='S' then
   begin
   cb_in.Checked:=true;
   end;

        if Conexion.ExecSQLScalar('select doctos_no from SIC_BITACORA_CONFIG')='S' then
   begin
   cb_no.Checked:=true;
   end;

        if Conexion.ExecSQLScalar('select doctos_pv from SIC_BITACORA_CONFIG')='S' then
   begin
   cb_pv.Checked:=true;
   end;

        if Conexion.ExecSQLScalar('select doctos_ve from SIC_BITACORA_CONFIG')='S' then
   begin
   cb_ve.Checked:=true;
   end;

           if Conexion.ExecSQLScalar('select doctos_cc from SIC_BITACORA_CONFIG')='S' then
   begin
   cb_cc.Checked:=true;
   end;

end;

end.
