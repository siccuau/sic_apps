object FCambioPreciosExcel: TFCambioPreciosExcel
  Left = 0
  Top = 0
  Caption = 'Cambio de Precios'
  ClientHeight = 195
  ClientWidth = 523
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object pb_archivo: TGauge
    Left = 8
    Top = 88
    Width = 499
    Height = 25
    Progress = 0
  end
  object txtArchivo: TEdit
    Left = 8
    Top = 24
    Width = 418
    Height = 21
    TabOrder = 0
  end
  object btnArchivo: TButton
    Left = 432
    Top = 22
    Width = 75
    Height = 25
    Caption = '...'
    TabOrder = 1
    OnClick = btnArchivoClick
  end
  object btnCambiarCostos: TButton
    Left = 213
    Top = 152
    Width = 97
    Height = 25
    Caption = 'Cambiar Costos'
    TabOrder = 2
    OnClick = btnCambiarCostosClick
  end
  object OpenDialog1: TOpenDialog
    Left = 32
    Top = 128
  end
end
