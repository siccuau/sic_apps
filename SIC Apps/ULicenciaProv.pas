unit ULicenciaProv;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, system.ioutils,Ulogin,
  Vcl.ExtCtrls;

type
  TFormLicenciaProv = class(TForm)
    txtLicencia: TEdit;
    btnVerificar: TButton;
    Timer1: TTimer;
    procedure FormShow(Sender: TObject);
    procedure btnVerificarClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormLicenciaProv: TFormLicenciaProv;

implementation

{$R *.dfm}

procedure TFormLicenciaProv.btnVerificarClick(Sender: TObject);
        var
        myFile : TextFile;
        text   : string;
        FormLogin : Tlogin;
begin
        if txtLicencia.Text = '@dm1nS1CDemo15' then
        begin
          AssignFile(myFile, TPath.Combine(TPath.GetHomePath,'pwdsic.txt'));
          Rewrite(myfile);
          Writeln(myfile,txtLicencia.Text);
          CloseFile(myFile);
          Formlogin := TLogin.Create(nil);
          FormLogin.Show;
          Hide;
        end
        else
        begin
          showmessage('Error en Contraseņa');
          txtLicencia.SetFocus;
        end;
end;

procedure TFormLicenciaProv.FormShow(Sender: TObject);
        var
        myFile : TextFile;
        text   : string;
begin


        if FileExists(TPath.Combine(TPath.GetHomePath,'pwdsic.txt')) then
        begin
            AssignFile(myFile, TPath.Combine(TPath.GetHomePath,'pwdsic.txt'));
            Reset(myFile);
            Readln(myfile,text);
            CloseFile(myFile);
            txtLicencia.Text := text;
            txtLicencia.Enabled := False;
            Timer1.Enabled := True;
        end
        else
        begin
            txtLicencia.SetFocus;
        end;
end;


procedure TFormLicenciaProv.Timer1Timer(Sender: TObject);
begin
	Timer1.Enabled := False;
	btnVerificar.Click;
end;

end.
