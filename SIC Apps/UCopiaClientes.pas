unit UCopiaClientes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.DBCtrls;

type
  TCopiaClientes = class(TForm)
    pbCopia: TProgressBar;
    btnCopia: TButton;
    Conexion: TFDConnection;
    Query: TFDQuery;
    DataSource: TDataSource;
    StatusBar1: TStatusBar;
    Label1: TLabel;
    cbMonedaOriginal: TDBLookupComboBox;
    cbMonedaNueva: TDBLookupComboBox;
    Label2: TLabel;
    Label3: TLabel;
    QryMonedaOriginal: TFDQuery;
    DSMonedaOriginal: TDataSource;
    DSMonedaNueva: TDataSource;
    QryMonedaNueva: TFDQuery;
    cbZonas: TDBLookupComboBox;
    Label4: TLabel;
    QryZonas: TFDQuery;
    DSZonas: TDataSource;
    procedure btnCopiaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CopiaClientes: TCopiaClientes;

implementation

{$R *.dfm}

procedure TCopiaClientes.btnCopiaClick(Sender: TObject);
var
  cliente_original_id:string;
  cliente_nuevo_id:string;
  query_str:string;
  progress :integer;
  zona_id : string;
  logfile :TextFile;
begin
  if (cbMonedaOriginal.KeyValue <> cbMonedaNueva.KeyValue) then
  begin

    //SE CREA LA INSTANCIA DEL ARCHIVO DE TEXTO
    AssignFile(logfile,'log.txt');
    ReWrite(logfile);

    //SI HAY ZONA ELEGIDA O NO
    if (cbZonas.KeyValue <> Null) then
    begin
      zona_id := cbZonas.KeyValue;
    end
    else
    begin
      zona_id := 'zona_cliente_id';
    end;

    // SELECCIONA LOS CLIENTES
    Query.SQL.Text:='select * from clientes where estatus='+Quotedstr('A')+' and moneda_id='+inttostr(cbMonedaOriginal.KeyValue);
    Query.Open;
    Query.First;
    pbCopia.Max := Query.RecordCount;
    progress := 0;
    label1.Caption := inttostr(progress)+'/'+inttostr(Query.RecordCount);
    while (not Query.Eof) do
    begin
      try
        //INSERTA EL REGISTRO EN LA TABLA DE CLIENTES
        cliente_original_id := Query.FieldByName('cliente_id').AsString;
        query_str := 'insert into clientes(cliente_id,nombre,contacto1,contacto2,estatus,causa_susp,'+
                        ' fecha_susp,cobrar_impuestos,retiene_impuestos,sujeto_ieps,'+
                        ' generar_intereses,emitir_edocta,limite_credito,moneda_id,'+
                        ' cond_pago_id,tipo_cliente_id,zona_cliente_id,cobrador_id,'+
                        ' vendedor_id,notas,cuenta_cxc,cuenta_anticipos,formatos_email,'+
                        ' receptor_cfd,num_prov_cliente,campos_addenda,usuario_creador,'+
                        ' fecha_hora_creacion,usuario_aut_creacion,usuario_ult_modif,'+
                        ' fecha_hora_ult_modif,usuario_aut_modif)'+
        ' SELECT -1,nombre||'+quotedstr('+')+',contacto1,contacto2,estatus,causa_susp,'+
                            ' fecha_susp,cobrar_impuestos,retiene_impuestos,sujeto_ieps,'+
                            ' generar_intereses,emitir_edocta,limite_credito,'+inttostr(cbMonedaNueva.KeyValue)+','+
                            ' cond_pago_id,tipo_cliente_id,'+zona_id+',cobrador_id,'+
                            ' vendedor_id,notas,cuenta_cxc,cuenta_anticipos,formatos_email,'+
                            ' receptor_cfd,num_prov_cliente,campos_addenda,usuario_creador,'+
                            ' fecha_hora_creacion,usuario_aut_creacion,usuario_ult_modif,'+
                            ' fecha_hora_ult_modif,usuario_aut_modif'+
        ' FROM CLIENTES WHERE cliente_id=:cli_id returning cliente_id';
        cliente_nuevo_id := Conexion.ExecSQLScalar(query_str,[cliente_original_id]);

        //INSERTA LA DIRECCION
        query_str := ' insert into dirs_clientes (dir_cli_id,cliente_id,nombre_consig,calle,nombre_calle,num_exterior,'+
                ' num_interior,colonia,colonia_clave_fiscal,poblacion,poblacion_clave_fiscal,'+
                ' referencia,ciudad_id,estado_id,codigo_postal,pais_id,telefono1,telefono2,'+
                ' fax,email,rfc_curp,tax_id,contacto,via_embarque_id,es_dir_ppal,usar_para_envios,'+
                ' usar_para_facturar,gln)'+
        ' select -1,:cliente_nuevo_id,nombre_consig,calle,nombre_calle,num_exterior,'+
                    ' num_interior,colonia,colonia_clave_fiscal,poblacion,poblacion_clave_fiscal,'+
                    ' referencia,ciudad_id,estado_id,codigo_postal,pais_id,telefono1,telefono2,'+
                    ' fax,email,rfc_curp,tax_id,contacto,via_embarque_id,es_dir_ppal,usar_para_envios,'+
                    ' usar_para_facturar,gln'+
        ' from dirs_clientes where cliente_id = :cli_id';
        Conexion.ExecSQL(query_str,[cliente_nuevo_id,cliente_original_id]);

        query_str := 'insert into claves_clientes(clave_cliente_id,clave_cliente,cliente_id,rol_clave_cli_id)'+
              ' select -1,clave_cliente||'+quotedstr('+')+',:cliente_nuevo_id,rol_clave_cli_id' +
              ' from claves_clientes where cliente_id=:cliente_original_id';
        Conexion.ExecSQL(query_str,[cliente_nuevo_id,cliente_original_id]);
      except
         WriteLn(logfile,Query.FieldByName('nombre').AsString);
      end;
        Query.Next;
        pbCopia.StepBy(1);
        progress := progress + 1;
        label1.Caption := inttostr(progress)+'/'+inttostr(pbCopia.Max);

    end;
    Query.Close;

    showmessage('Proceso Terminado');
  end
  else
  begin
    showmessage('Seleccione Monedas Diferentes');
  end;

end;

procedure TCopiaClientes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Application.Terminate;
end;

procedure TCopiaClientes.FormShow(Sender: TObject);
begin
  QryMonedaOriginal.Open;
  QryMonedaNueva.Open;
  QryZonas.Open;
end;

end.
