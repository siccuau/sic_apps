unit UClavesSAT;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, Vcl.StdCtrls, Data.DB, FireDAC.Comp.Client, Vcl.ComCtrls, Vcl.DBCtrls, ActiveX, ComObj,
  Vcl.Samples.Gauges, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet;

type
  TClavesSat = class(TForm)
    StatusBar1: TStatusBar;
    Conexion: TFDConnection;
    btnSeleccionar: TButton;
    odArchivo: TOpenDialog;
    btnImportar: TButton;
    pbProgreso: TGauge;
    gbImportar: TGroupBox;
    gbExportar: TGroupBox;
    btnExportar: TButton;
    cbNuevas: TCheckBox;
    cbReemplazar: TCheckBox;
    QueryClaves: TFDQuery;
    sdGuardar: TSaveDialog;
    procedure btnSeleccionarClick(Sender: TObject);
    procedure btnImportarClick(Sender: TObject);
    procedure btnExportarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ClavesSat: TClavesSat;

implementation

{$R *.dfm}

procedure TClavesSat.btnSeleccionarClick(Sender: TObject);
begin
 if  odArchivo.Execute then
 begin
  btnseleccionar.Caption := odArchivo.FileName;
  btnImportar.Enabled := True;
 end
 else
 begin
  btnSeleccionar.caption := 'Selecciona Archivo';
  btnImportar.Enabled := false;
 end;

end;

procedure TClavesSat.btnExportarClick(Sender: TObject);
var
  XL: OleVariant;
  col :integer;
  hoja:variant;
begin
  if sdGuardar.Execute then
  begin
    btnExportar.Enabled := False;
    XL := CreateOleObject('Excel.Application');
    XL.WorkBooks.Add(-4167);
    XL.WorkBooks[1].WorkSheets[1].Name:='Hoja1';
    hoja:=XL.WorkBooks[1].WorkSheets['Hoja1'];
    col := 1;

    QueryClaves.open;
    QueryClaves.First;
    QueryClaves.FetchAll;

    while (not QueryClaves.eof) do
    begin
      hoja.Cells[col,1].Value := '''' + QueryClaves.FieldByName('clave_art').AsString;
      hoja.Cells[col,2].Value := '''' + QueryClaves.FieldByName('clave_sat').AsString;
      col := col + 1;
      QueryClaves.Next;
    end;
    XL.ActiveWorkbook.SaveAs(sdGuardar.FileName, emptyparam, emptyparam, emptyparam, emptyparam, emptyparam);
    XL.Application.Quit;
    showmessage('Exportación correcta');
    btnExportar.enabled := true;
  end;
end;

procedure TClavesSat.btnImportarClick(Sender: TObject);
var
   XL: OleVariant;
   num_rows, i, articulo_id, clave_sat_id :integer;
   clave_articulo, clave_sat : string;
begin
  XL := CreateOleObject('Excel.Application');
  XL.WorkBooks.Open(odArchivo.FileName);
  num_rows := XL.Activesheet.UsedRange.Rows.Count;
  pbProgreso.MaxValue := num_rows;

  for i := 1 to num_rows do
  begin
    clave_articulo := XL.ActiveSheet.Cells[i,1].Value;
    clave_sat := XL.ActiveSheet.Cells[i,2].Value;
    articulo_id := Conexion.execsqlscalar('select coalesce(articulo_id,0) from ORSP_BUSCA_CLAVE_ARTICULO(:clave)',[clave_articulo]);

    if articulo_id <> 0 then
    begin
      clave_sat_id := Conexion.ExecSQLScalar('select coalesce(datos_adicionales_id,0) from datos_adicionales da where nom_tabla=''ARTICULOS'' and tipo_reg=2 and elem_id = :aid',[articulo_id]);
      //SI YA EXISTE LA CLAVE ESTABLECIDA EN EL ARTICULO EN CUESTION
      if clave_sat_id <> 0 then
      begin
        if cbReemplazar.Checked then
        begin
          Conexion.ExecSQL('update datos_adicionales set clave='''+clave_sat+''' where datos_adicionales_id = :eid',[clave_sat_id]);
          Conexion.Commit;
        end;
      end
      //SI NO EXISTE LA CLAVE DEL SAT EN EL ARTICULO EN CUESTION
      else
      begin
        if cbNuevas.Checked then
        begin
          Conexion.ExecSQL('insert into datos_adicionales values(-1,''ARTICULOS'',:aid,2,'''+clave_sat+''',null)',[articulo_id]);
          Conexion.Commit;
        end;
      end;
    end;
    pbProgreso.Progress := pbProgreso.progress + 1;
  end;
  XL.Application.Quit;
  showmessage('Proceso Completado');
  pbProgreso.Progress := 0;
end;

end.
