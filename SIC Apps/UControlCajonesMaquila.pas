unit UControlCajonesMaquila;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, Vcl.ComCtrls, Data.DB, FireDAC.Comp.Client, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids, UControlCajonesEmpacadorasLista, UControlCajonesMaquilaEtiquetas, UControlCajonesMaquilaRegistro,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet;

type
  TControlCajonesMaquila = class(TForm)
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    DBMaquilas: TDBGrid;
    btnEditarMaquila: TButton;
    btnMaquilaNueva: TButton;
    btnEmpacadoras: TButton;
    btnEtiquetas: TButton;
    QueryMaquilas: TFDQuery;
    procedure btnEmpacadorasClick(Sender: TObject);
    procedure btnEtiquetasClick(Sender: TObject);
    procedure btnMaquilaNuevaClick(Sender: TObject);
    procedure btnEditarMaquilaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ControlCajonesMaquila: TControlCajonesMaquila;

implementation

{$R *.dfm}

procedure TControlCajonesMaquila.btnEditarMaquilaClick(Sender: TObject);
var
  FormMaquila : TControlCajonesMaquilaRegistro;
begin
  //********SI NO ESTA SELECCIONADA ALGUNA MAQUILA********
  if QueryMaquilas.FieldByName('maquila_id').Asstring = '' then
  begin
    showmessage('Seleccione una maquila primero por favor');
  end
  else
  //******SI YA ESTA SELECCIONADA LA MAQUILA************
  begin
    FormMaquila := TControlCajonesMaquilaRegistro.Create(nil);
    FormMaquila.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
    FormMaquila.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
    FormMaquila.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
    FormMaquila.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
    FormMaquila.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';

    FormMaquila.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
    FormMaquila.operacion := 1;
    FormMaquila.btnDetalleSalida.Enabled := True;
    FormMaquila.btnDetalleEntrada.Enabled := True;
    FormMaquila.maquila_id := QueryMaquilas.FieldByName('maquila_id').AsInteger;
    FormMaquila.Caption := 'Maquila' + QueryMaquilas.FieldByName('folio').AsString;
    FormMaquila.ShowModal;
  end;
end;

procedure TControlCajonesMaquila.btnEmpacadorasClick(Sender: TObject);
var
  FormEmpacadoras : TControlCajonesEmpacadorasLista;
begin
  //*************PANTALLA DE EMPACADORES***************
  FormEmpacadoras := TControlCajonesEmpacadorasLista.Create(nil);
  FormEmpacadoras.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormEmpacadoras.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormEmpacadoras.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormEmpacadoras.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormEmpacadoras.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';


  FormEmpacadoras.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
  FormEmpacadoras.ShowModal;
end;

procedure TControlCajonesMaquila.btnMaquilaNuevaClick(Sender: TObject);
var
  FormMaquila : TControlCajonesMaquilaRegistro;
begin
  //********MAQUILA NUEVA********************
  FormMaquila := TControlCajonesMaquilaRegistro.Create(nil);
  FormMaquila.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormMaquila.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormMaquila.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormMaquila.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormMaquila.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';

  FormMaquila.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
  FormMaquila.operacion := 0;
  FormMaquila.btnDetalleSalida.Enabled := False;
  FormMaquila.btnDetalleEntrada.Enabled := False;
  FormMaquila.Caption := 'Nueva Maquila';
  FormMaquila.ShowModal;
end;

procedure TControlCajonesMaquila.FormShow(Sender: TObject);
begin
  //*****************SE CREA LA TABLA DE EMPACADORAS AL INGRESAR A LA PANTALLA PRINCIPAL*************
  QueryMaquilas.Open;
  Conexion.ExecSQL( 'execute block as begin if (not exists(select 1 from rdb$relations where rdb$relation_name = ''SIC_EMPACADORAS'')) then'+
    ' BEGIN'+
        ' execute statement ''create table SIC_EMPACADORAS ( id integer,nombre VARCHAR(20), pagoxunidad double precision, consec integer);'';'+
        ' execute statement ''CREATE GENERATOR sic_id_empacadora;'';'+
        ' execute statement ''set GENERATOR sic_id_empacadora TO 0;'';'+
        ' execute statement ''CREATE TRIGGER SIC_empacadoras_BI0 FOR SIC_empacadoras'+
                                ' ACTIVE BEFORE INSERT POSITION 0'+
                                ' AS'+
                                ' BEGIN'+
                                    ' if (NEW.ID is NULL) then NEW.ID = GEN_ID(sic_id_empacadora, 1);'+
                                ' END'';'+
    ' END end');


end;

procedure TControlCajonesMaquila.btnEtiquetasClick(Sender: TObject);
var
  FormEtiquetas:TControlCajonesMaquilaEtiquetas;
begin
  //****************SE ABRE LA PANTALLA DE ETIQUETAS*************
  FormEtiquetas := TControlCajonesMaquilaEtiquetas.Create(nil);
  FormEtiquetas.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormEtiquetas.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormEtiquetas.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormEtiquetas.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormEtiquetas.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';

  FormEtiquetas.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
  FormEtiquetas.ShowModal;
end;

end.
