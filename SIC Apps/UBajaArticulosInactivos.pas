unit UBajaArticulosInactivos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef,
  FireDAC.VCLUI.Wait, Vcl.ComCtrls, Data.DB, FireDAC.Comp.Client,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  Vcl.StdCtrls, FireDAC.Comp.DataSet, Vcl.CheckLst, Data.Bind.EngExt,
  Vcl.Bind.DBEngExt, System.Rtti, System.Bindings.Outputs,
  Vcl.Bind.Editors, Data.Bind.Components, Data.Bind.DBScope;

type
  TBajaArticulosInactivos = class(TForm)
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    clbArticulos: TCheckListBox;
    qryArticulos: TFDQuery;
    dtpFecha: TDateTimePicker;
    Label1: TLabel;
    btnBuscar: TButton;
    lblNumArticulos: TLabel;
    Label2: TLabel;
    cbAlmacenes: TComboBox;
    qryalmacenes: TFDQuery;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkListControlToField1: TLinkListControlToField;
    cbTodos: TCheckBox;
    btnIniciar: TButton;
    cbDardeBaja: TCheckBox;
    cbInventarioacero: TCheckBox;
    qryDoctosInvFis: TFDQuery;
    qryDoctosInvFisDet: TFDQuery;
    procedure btnBuscarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnIniciarClick(Sender: TObject);
    procedure cbTodosClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  BajaArticulosInactivos: TBajaArticulosInactivos;

implementation

{$R *.dfm}

procedure TBajaArticulosInactivos.btnBuscarClick(Sender: TObject);
begin
  if qryArticulos.Active then
  	qryArticulos.close;
	clbArticulos.Items.Clear;
	with qryArticulos do
  begin
    ParamByName('fecha').AsDate := dtpFecha.Date;
    ParamByName('almacen_id').asinteger := qryalmacenes.FieldByName('almacen_id').AsInteger;
    open;
    FetchAll;
    lblNumArticulos.Caption := IntToStr(RecordCount)+' Articulos';
    First;
    while not eof do
    begin
			clbArticulos.Items.Add(FieldByName('nombre').AsString);
    	Next;
    end;
  end;
end;

procedure TBajaArticulosInactivos.btnIniciarClick(Sender: TObject);
var
  I, sel,folio_invfis: Integer;
  nuevo_folio_invfis : string;
begin
	btnIniciar.Enabled := False;
  sel := 0;
  for i := 0 to clbArticulos.Count-1 do
  begin
  	if clbArticulos.State[i] = TCheckBoxState.cbChecked then
  		sel := sel + 1;
  end;

  if sel > 0 then
  begin
    //se crea un inventario fisico
    folio_invfis := Conexion.ExecSQLScalar('select valor from registry where nombre = ''SIG_FOLIO_INVFIS''');
    nuevo_folio_invfis := Format('%.*d',[9, folio_invfis+1]);
    if cbInventarioacero.Checked then
    begin
      with qryDoctosInvFis do
      begin
        insert;
        FieldByName('docto_invfis_id').Value := -1;
        FieldByName('Almacen_id').Value := qryalmacenes.FieldByName('almacen_id').Value;
        FieldByName('folio').Value := Conexion.ExecSQLScalar('select valor from registry where nombre = ''SIG_FOLIO_INVFIS''');
        FieldByName('Fecha').Value := Now;
        FieldByName('Aplicado').Value := 'N';
        FieldByName('descripcion').Value := 'Ajuste a ceros';
        post;
        ApplyUpdates(0);
        CommitUpdates;
        Refresh;
      end;
      conexion.ExecSQL('update registry set valor='''+nuevo_folio_invfis+''' where nombre = ''SIG_FOLIO_INVFIS''');
      Conexion.Commit;
      qryDoctosInvFis.Last;

    end;
    for I := 0 to clbArticulos.Count-1 do
    begin
      qryArticulos.Locate('nombre',clbArticulos.items[i],[]);
      if clbArticulos.State[i] = TCheckBoxState.cbChecked then
      begin
        if cbDardeBaja.Checked then
        begin
          Conexion.ExecSQL('update articulos set estatus=''B'' where nombre='''+clbArticulos.Items[i]+'''');
        end;

        if cbInventarioacero.Checked then
        begin
          with qryDoctosInvFisDet do
          begin
          	insert;
            FieldByName('docto_invfis_det_id').Value := -1;
            FieldByName('docto_invfis_id').Value := qryDoctosInvFis.FieldByName('docto_invfis_id').Value;
            FieldByName('clave_articulo').Value := Conexion.ExecSQLScalar('select clave_articulo from get_clave_art('+qryArticulos.FieldByName('articulo_id').AsString+')');
            FieldByName('articulo_id').Value := qryArticulos.FieldByName('articulo_id').Value;
            FieldByName('unidades').Value := 0;
            Post;
            ApplyUpdates(0);
            CommitUpdates;
            Refresh;
          end;
        end;
      end;
    end;
  end;
  btnBuscar.Click;
  Conexion.Commit;
  ShowMessage('Proceso terminado');
  btnIniciar.Enabled := True;
end;

procedure TBajaArticulosInactivos.cbTodosClick(Sender: TObject);
var
	i : integer;
begin
	for i := 0 to clbArticulos.Count-1 do
  begin
  	clbArticulos.State[i] := cbTodos.State;
  end;

end;

procedure TBajaArticulosInactivos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	qryArticulos.Close;
	qryalmacenes.close;
  qryDoctosInvFis.Close;
  qryDoctosInvFisDet.Close;
	Application.Terminate;
end;

procedure TBajaArticulosInactivos.FormShow(Sender: TObject);
begin
	qryalmacenes.Open;
  qryDoctosInvFis.Open;
  qryDoctosInvFisDet.Open;
end;

end.
