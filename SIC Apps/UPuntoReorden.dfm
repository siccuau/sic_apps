object PuntoReorden: TPuntoReorden
  Left = 0
  Top = 0
  Caption = 'Punto de Reorden'
  ClientHeight = 435
  ClientWidth = 532
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbl_restante: TLabel
    Left = 343
    Top = 75
    Width = 3
    Height = 13
  end
  object DBGrid1: TDBGrid
    Left = 13
    Top = 167
    Width = 511
    Height = 169
    DataSource = dsLlenado
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 416
    Width = 532
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object btn_iniciar: TButton
    Left = 343
    Top = 8
    Width = 75
    Height = 57
    Caption = 'Iniciar'
    TabOrder = 2
    OnClick = btn_iniciarClick
  end
  object StaticText1: TStaticText
    Left = 83
    Top = 8
    Width = 104
    Height = 23
    Caption = 'Ejecutar cada:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
  end
  object UpDown1: TUpDown
    Left = 214
    Top = 8
    Width = 16
    Height = 21
    Associate = num_periodo
    TabOrder = 4
  end
  object num_periodo: TEdit
    Left = 193
    Top = 8
    Width = 21
    Height = 21
    TabOrder = 5
    Text = '0'
  end
  object cb_periodo: TComboBox
    Left = 234
    Top = 8
    Width = 90
    Height = 21
    ItemIndex = 0
    TabOrder = 6
    Text = 'Minutos'
    Items.Strings = (
      'Minutos'
      'Horas'
      'Dias')
  end
  object cb_almacen: TComboBox
    Left = 192
    Top = 44
    Width = 145
    Height = 21
    TabOrder = 7
  end
  object StaticText2: TStaticText
    Left = 16
    Top = 42
    Width = 171
    Height = 23
    Caption = 'Existencias en almacen:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
  end
  object dtpFechaIni: TDateTimePicker
    Left = 57
    Top = 71
    Width = 113
    Height = 21
    Date = 43847.399206851850000000
    Time = 43847.399206851850000000
    TabOrder = 9
  end
  object dtpFechaFin: TDateTimePicker
    Left = 210
    Top = 71
    Width = 113
    Height = 21
    Date = 43847.399206851850000000
    Time = 43847.399206851850000000
    TabOrder = 10
  end
  object StaticText3: TStaticText
    Left = 21
    Top = 71
    Width = 30
    Height = 23
    Caption = 'del '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 11
  end
  object StaticText4: TStaticText
    Left = 187
    Top = 71
    Width = 16
    Height = 23
    Caption = 'al'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 12
  end
  object cbSurtido: TComboBox
    Left = 156
    Top = 100
    Width = 124
    Height = 21
    ItemIndex = 1
    TabOrder = 13
    Text = 'Punto de Reorden'
    Items.Strings = (
      'Inventario Maximo'
      'Punto de Reorden'
      'Inventario Minimo')
  end
  object StaticText5: TStaticText
    Left = 21
    Top = 100
    Width = 129
    Height = 23
    Caption = 'Punto de Sutrido:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 14
  end
  object Button1: TButton
    Left = 424
    Top = 8
    Width = 75
    Height = 57
    Caption = 'Button1'
    TabOrder = 15
    OnClick = Button1Click
  end
  object StaticText6: TStaticText
    Left = 8
    Top = 129
    Width = 159
    Height = 23
    Caption = 'Proveedor a comprar:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 16
  end
  object cbProveedor: TComboBox
    Left = 173
    Top = 129
    Width = 284
    Height = 21
    TabOrder = 17
  end
  object Timer_reorden: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer_reordenTimer
    Left = 88
    Top = 216
  end
  object Timer_restante: TTimer
    Enabled = False
    Interval = 200
    OnTimer = Timer_restanteTimer
    Left = 136
    Top = 216
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'Database=E:\Microsip Pruebas\SIC 2019.FDB'
      'DriverID=FB')
    Connected = True
    Left = 192
    Top = 216
  end
  object qryArticulos: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from articulos')
    Left = 240
    Top = 216
    object qryArticulosARTICULO_ID: TIntegerField
      FieldName = 'ARTICULO_ID'
      Origin = 'ARTICULO_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryArticulosNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Required = True
      Size = 200
    end
    object qryArticulosES_ALMACENABLE: TStringField
      FieldName = 'ES_ALMACENABLE'
      Origin = 'ES_ALMACENABLE'
      FixedChar = True
      Size = 1
    end
    object qryArticulosES_JUEGO: TStringField
      FieldName = 'ES_JUEGO'
      Origin = 'ES_JUEGO'
      FixedChar = True
      Size = 1
    end
    object qryArticulosESTATUS: TStringField
      FieldName = 'ESTATUS'
      Origin = 'ESTATUS'
      Required = True
      FixedChar = True
      Size = 1
    end
    object qryArticulosCAUSA_SUSP: TStringField
      FieldName = 'CAUSA_SUSP'
      Origin = 'CAUSA_SUSP'
      Size = 100
    end
    object qryArticulosFECHA_SUSP: TDateField
      FieldName = 'FECHA_SUSP'
      Origin = 'FECHA_SUSP'
    end
    object qryArticulosIMPRIMIR_COMP: TStringField
      FieldName = 'IMPRIMIR_COMP'
      Origin = 'IMPRIMIR_COMP'
      FixedChar = True
      Size = 1
    end
    object qryArticulosPERMITIR_AGREGAR_COMP: TStringField
      FieldName = 'PERMITIR_AGREGAR_COMP'
      Origin = 'PERMITIR_AGREGAR_COMP'
      FixedChar = True
      Size = 1
    end
    object qryArticulosLINEA_ARTICULO_ID: TIntegerField
      FieldName = 'LINEA_ARTICULO_ID'
      Origin = 'LINEA_ARTICULO_ID'
    end
    object qryArticulosUNIDAD_VENTA: TStringField
      FieldName = 'UNIDAD_VENTA'
      Origin = 'UNIDAD_VENTA'
    end
    object qryArticulosUNIDAD_COMPRA: TStringField
      FieldName = 'UNIDAD_COMPRA'
      Origin = 'UNIDAD_COMPRA'
    end
    object qryArticulosCONTENIDO_UNIDAD_COMPRA: TFMTBCDField
      FieldName = 'CONTENIDO_UNIDAD_COMPRA'
      Origin = 'CONTENIDO_UNIDAD_COMPRA'
      Required = True
      Precision = 18
      Size = 5
    end
    object qryArticulosPESO_UNITARIO: TFMTBCDField
      FieldName = 'PESO_UNITARIO'
      Origin = 'PESO_UNITARIO'
      Required = True
      Precision = 18
      Size = 5
    end
    object qryArticulosES_PESO_VARIABLE: TStringField
      FieldName = 'ES_PESO_VARIABLE'
      Origin = 'ES_PESO_VARIABLE'
      FixedChar = True
      Size = 1
    end
    object qryArticulosSEGUIMIENTO: TStringField
      FieldName = 'SEGUIMIENTO'
      Origin = 'SEGUIMIENTO'
      FixedChar = True
      Size = 1
    end
    object qryArticulosDIAS_GARANTIA: TIntegerField
      FieldName = 'DIAS_GARANTIA'
      Origin = 'DIAS_GARANTIA'
    end
    object qryArticulosES_IMPORTADO: TStringField
      FieldName = 'ES_IMPORTADO'
      Origin = 'ES_IMPORTADO'
      FixedChar = True
      Size = 1
    end
    object qryArticulosES_SIEMPRE_IMPORTADO: TStringField
      FieldName = 'ES_SIEMPRE_IMPORTADO'
      Origin = 'ES_SIEMPRE_IMPORTADO'
      FixedChar = True
      Size = 1
    end
    object qryArticulosPCTJE_ARANCEL: TFMTBCDField
      FieldName = 'PCTJE_ARANCEL'
      Origin = 'PCTJE_ARANCEL'
      Required = True
      Precision = 9
      Size = 6
    end
    object qryArticulosNOTAS_COMPRAS: TMemoField
      FieldName = 'NOTAS_COMPRAS'
      Origin = 'NOTAS_COMPRAS'
      BlobType = ftMemo
    end
    object qryArticulosIMPRIMIR_NOTAS_COMPRAS: TStringField
      FieldName = 'IMPRIMIR_NOTAS_COMPRAS'
      Origin = 'IMPRIMIR_NOTAS_COMPRAS'
      FixedChar = True
      Size = 1
    end
    object qryArticulosNOTAS_VENTAS: TMemoField
      FieldName = 'NOTAS_VENTAS'
      Origin = 'NOTAS_VENTAS'
      BlobType = ftMemo
    end
    object qryArticulosIMPRIMIR_NOTAS_VENTAS: TStringField
      FieldName = 'IMPRIMIR_NOTAS_VENTAS'
      Origin = 'IMPRIMIR_NOTAS_VENTAS'
      FixedChar = True
      Size = 1
    end
    object qryArticulosES_PRECIO_VARIABLE: TStringField
      FieldName = 'ES_PRECIO_VARIABLE'
      Origin = 'ES_PRECIO_VARIABLE'
      FixedChar = True
      Size = 1
    end
    object qryArticulosCUENTA_ALMACEN: TStringField
      FieldName = 'CUENTA_ALMACEN'
      Origin = 'CUENTA_ALMACEN'
      Size = 30
    end
    object qryArticulosCUENTA_COSTO_VENTA: TStringField
      FieldName = 'CUENTA_COSTO_VENTA'
      Origin = 'CUENTA_COSTO_VENTA'
      Size = 30
    end
    object qryArticulosCUENTA_VENTAS: TStringField
      FieldName = 'CUENTA_VENTAS'
      Origin = 'CUENTA_VENTAS'
      Size = 30
    end
    object qryArticulosCUENTA_DEVOL_VENTAS: TStringField
      FieldName = 'CUENTA_DEVOL_VENTAS'
      Origin = 'CUENTA_DEVOL_VENTAS'
      Size = 30
    end
    object qryArticulosCUENTA_COMPRAS: TStringField
      FieldName = 'CUENTA_COMPRAS'
      Origin = 'CUENTA_COMPRAS'
      Size = 30
    end
    object qryArticulosCUENTA_DEVOL_COMPRAS: TStringField
      FieldName = 'CUENTA_DEVOL_COMPRAS'
      Origin = 'CUENTA_DEVOL_COMPRAS'
      Size = 30
    end
    object qryArticulosAPLICAR_FACTOR_VENTA: TStringField
      FieldName = 'APLICAR_FACTOR_VENTA'
      Origin = 'APLICAR_FACTOR_VENTA'
      Required = True
      FixedChar = True
      Size = 1
    end
    object qryArticulosFACTOR_VENTA: TFMTBCDField
      FieldName = 'FACTOR_VENTA'
      Origin = 'FACTOR_VENTA'
      Required = True
      Precision = 18
      Size = 5
    end
    object qryArticulosRED_PRECIO_CON_IMPTO: TStringField
      FieldName = 'RED_PRECIO_CON_IMPTO'
      Origin = 'RED_PRECIO_CON_IMPTO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object qryArticulosFACTOR_RED_PRECIO_CON_IMPTO: TFMTBCDField
      FieldName = 'FACTOR_RED_PRECIO_CON_IMPTO'
      Origin = 'FACTOR_RED_PRECIO_CON_IMPTO'
      Required = True
      Precision = 18
      Size = 6
    end
    object qryArticulosUSUARIO_CREADOR: TStringField
      FieldName = 'USUARIO_CREADOR'
      Origin = 'USUARIO_CREADOR'
      Size = 31
    end
    object qryArticulosFECHA_HORA_CREACION: TSQLTimeStampField
      FieldName = 'FECHA_HORA_CREACION'
      Origin = 'FECHA_HORA_CREACION'
    end
    object qryArticulosUSUARIO_AUT_CREACION: TStringField
      FieldName = 'USUARIO_AUT_CREACION'
      Origin = 'USUARIO_AUT_CREACION'
      Size = 31
    end
    object qryArticulosUSUARIO_ULT_MODIF: TStringField
      FieldName = 'USUARIO_ULT_MODIF'
      Origin = 'USUARIO_ULT_MODIF'
      Size = 31
    end
    object qryArticulosFECHA_HORA_ULT_MODIF: TSQLTimeStampField
      FieldName = 'FECHA_HORA_ULT_MODIF'
      Origin = 'FECHA_HORA_ULT_MODIF'
    end
    object qryArticulosUSUARIO_AUT_MODIF: TStringField
      FieldName = 'USUARIO_AUT_MODIF'
      Origin = 'USUARIO_AUT_MODIF'
      Size = 31
    end
  end
  object qryAlmacenes: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from almacenes')
    Left = 280
    Top = 216
    object qryAlmacenesALMACEN_ID: TIntegerField
      FieldName = 'ALMACEN_ID'
      Origin = 'ALMACEN_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryAlmacenesNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Required = True
      Size = 50
    end
  end
  object dsAlmacenes: TDataSource
    DataSet = qryAlmacenes
    Left = 320
    Top = 216
  end
  object BindSourceDB1: TBindSourceDB
    DataSet = qryAlmacenes
    ScopeMappings = <>
    Left = 256
    Top = 144
  end
  object BindingsList1: TBindingsList
    Methods = <>
    OutputConverters = <>
    Left = 156
    Top = 149
    object LinkListControlToField1: TLinkListControlToField
      Category = 'Quick Bindings'
      DataSource = BindSourceDB1
      FieldName = 'NOMBRE'
      Control = cb_almacen
      FillExpressions = <>
      FillHeaderExpressions = <>
      FillBreakGroups = <>
    end
    object LinkListControlToField2: TLinkListControlToField
      Category = 'Quick Bindings'
      DataSource = BindSourceDB2
      FieldName = 'NOMBRE'
      Control = cbProveedor
      FillExpressions = <>
      FillHeaderExpressions = <>
      FillBreakGroups = <>
    end
  end
  object qryLlenado: TFDQuery
    Connection = Conexion
    Left = 376
    Top = 384
  end
  object dsLlenado: TDataSource
    DataSet = qryLlenado
    Left = 408
    Top = 384
  end
  object qryProveedores: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from proveedores')
    Left = 32
    Top = 376
    object qryProveedoresPROVEEDOR_ID: TIntegerField
      FieldName = 'PROVEEDOR_ID'
      Origin = 'PROVEEDOR_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryProveedoresNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Required = True
      Size = 200
    end
    object qryProveedoresCONTACTO1: TStringField
      FieldName = 'CONTACTO1'
      Origin = 'CONTACTO1'
      Size = 50
    end
    object qryProveedoresCONTACTO2: TStringField
      FieldName = 'CONTACTO2'
      Origin = 'CONTACTO2'
      Size = 50
    end
    object qryProveedoresCALLE: TStringField
      FieldName = 'CALLE'
      Origin = 'CALLE'
      Size = 430
    end
    object qryProveedoresNOMBRE_CALLE: TStringField
      FieldName = 'NOMBRE_CALLE'
      Origin = 'NOMBRE_CALLE'
      Size = 100
    end
    object qryProveedoresNUM_EXTERIOR: TStringField
      FieldName = 'NUM_EXTERIOR'
      Origin = 'NUM_EXTERIOR'
      Size = 10
    end
    object qryProveedoresNUM_INTERIOR: TStringField
      FieldName = 'NUM_INTERIOR'
      Origin = 'NUM_INTERIOR'
      Size = 10
    end
    object qryProveedoresCOLONIA: TStringField
      FieldName = 'COLONIA'
      Origin = 'COLONIA'
      Size = 100
    end
    object qryProveedoresPOBLACION: TStringField
      FieldName = 'POBLACION'
      Origin = 'POBLACION'
      Size = 100
    end
    object qryProveedoresREFERENCIA: TStringField
      FieldName = 'REFERENCIA'
      Origin = 'REFERENCIA'
      Size = 100
    end
    object qryProveedoresCIUDAD_ID: TIntegerField
      FieldName = 'CIUDAD_ID'
      Origin = 'CIUDAD_ID'
    end
    object qryProveedoresESTADO_ID: TIntegerField
      FieldName = 'ESTADO_ID'
      Origin = 'ESTADO_ID'
    end
    object qryProveedoresCODIGO_POSTAL: TStringField
      FieldName = 'CODIGO_POSTAL'
      Origin = 'CODIGO_POSTAL'
      Size = 10
    end
    object qryProveedoresPAIS_ID: TIntegerField
      FieldName = 'PAIS_ID'
      Origin = 'PAIS_ID'
    end
    object qryProveedoresTELEFONO1: TStringField
      FieldName = 'TELEFONO1'
      Origin = 'TELEFONO1'
      Size = 35
    end
    object qryProveedoresTELEFONO2: TStringField
      FieldName = 'TELEFONO2'
      Origin = 'TELEFONO2'
      Size = 35
    end
    object qryProveedoresFAX: TStringField
      FieldName = 'FAX'
      Origin = 'FAX'
      Size = 35
    end
    object qryProveedoresEMAIL: TStringField
      FieldName = 'EMAIL'
      Origin = 'EMAIL'
      Size = 200
    end
    object qryProveedoresRFC_CURP: TStringField
      FieldName = 'RFC_CURP'
      Origin = 'RFC_CURP'
      Size = 18
    end
    object qryProveedoresTAX_ID: TStringField
      FieldName = 'TAX_ID'
      Origin = 'TAX_ID'
    end
    object qryProveedoresVIA_EMBARQUE_ID: TIntegerField
      FieldName = 'VIA_EMBARQUE_ID'
      Origin = 'VIA_EMBARQUE_ID'
    end
    object qryProveedoresESTATUS: TStringField
      FieldName = 'ESTATUS'
      Origin = 'ESTATUS'
      FixedChar = True
      Size = 1
    end
    object qryProveedoresCAUSA_SUSP: TStringField
      FieldName = 'CAUSA_SUSP'
      Origin = 'CAUSA_SUSP'
      Size = 100
    end
    object qryProveedoresFECHA_SUSP: TDateField
      FieldName = 'FECHA_SUSP'
      Origin = 'FECHA_SUSP'
    end
    object qryProveedoresCARGA_IMPUESTOS: TStringField
      FieldName = 'CARGA_IMPUESTOS'
      Origin = 'CARGA_IMPUESTOS'
      FixedChar = True
      Size = 1
    end
    object qryProveedoresRETENER_IMPUESTOS: TStringField
      FieldName = 'RETENER_IMPUESTOS'
      Origin = 'RETENER_IMPUESTOS'
      FixedChar = True
      Size = 1
    end
    object qryProveedoresSUJETO_IEPS: TStringField
      FieldName = 'SUJETO_IEPS'
      Origin = 'SUJETO_IEPS'
      Required = True
      FixedChar = True
      Size = 1
    end
    object qryProveedoresEXTRANJERO: TStringField
      FieldName = 'EXTRANJERO'
      Origin = 'EXTRANJERO'
      FixedChar = True
      Size = 1
    end
    object qryProveedoresLIMITE_CREDITO: TBCDField
      FieldName = 'LIMITE_CREDITO'
      Origin = 'LIMITE_CREDITO'
      Required = True
      Precision = 18
      Size = 2
    end
    object qryProveedoresMONEDA_ID: TIntegerField
      FieldName = 'MONEDA_ID'
      Origin = 'MONEDA_ID'
      Required = True
    end
    object qryProveedoresCOND_PAGO_ID: TIntegerField
      FieldName = 'COND_PAGO_ID'
      Origin = 'COND_PAGO_ID'
      Required = True
    end
    object qryProveedoresTIPO_PROV_ID: TIntegerField
      FieldName = 'TIPO_PROV_ID'
      Origin = 'TIPO_PROV_ID'
    end
    object qryProveedoresNOTAS: TMemoField
      FieldName = 'NOTAS'
      Origin = 'NOTAS'
      BlobType = ftMemo
    end
    object qryProveedoresCUENTA_CXP: TStringField
      FieldName = 'CUENTA_CXP'
      Origin = 'CUENTA_CXP'
      Size = 30
    end
    object qryProveedoresCUENTA_ANTICIPOS: TStringField
      FieldName = 'CUENTA_ANTICIPOS'
      Origin = 'CUENTA_ANTICIPOS'
      Size = 30
    end
    object qryProveedoresCONCEPTO_BA_ID: TIntegerField
      FieldName = 'CONCEPTO_BA_ID'
      Origin = 'CONCEPTO_BA_ID'
    end
    object qryProveedoresFORMATOS_EMAIL: TStringField
      FieldName = 'FORMATOS_EMAIL'
      Origin = 'FORMATOS_EMAIL'
      Size = 100
    end
    object qryProveedoresACTIVIDAD_PRINCIPAL: TStringField
      FieldName = 'ACTIVIDAD_PRINCIPAL'
      Origin = 'ACTIVIDAD_PRINCIPAL'
      Size = 3
    end
    object qryProveedoresUSUARIO_CREADOR: TStringField
      FieldName = 'USUARIO_CREADOR'
      Origin = 'USUARIO_CREADOR'
      Size = 31
    end
    object qryProveedoresFECHA_HORA_CREACION: TSQLTimeStampField
      FieldName = 'FECHA_HORA_CREACION'
      Origin = 'FECHA_HORA_CREACION'
    end
    object qryProveedoresUSUARIO_AUT_CREACION: TStringField
      FieldName = 'USUARIO_AUT_CREACION'
      Origin = 'USUARIO_AUT_CREACION'
      Size = 31
    end
    object qryProveedoresUSUARIO_ULT_MODIF: TStringField
      FieldName = 'USUARIO_ULT_MODIF'
      Origin = 'USUARIO_ULT_MODIF'
      Size = 31
    end
    object qryProveedoresFECHA_HORA_ULT_MODIF: TSQLTimeStampField
      FieldName = 'FECHA_HORA_ULT_MODIF'
      Origin = 'FECHA_HORA_ULT_MODIF'
    end
    object qryProveedoresUSUARIO_AUT_MODIF: TStringField
      FieldName = 'USUARIO_AUT_MODIF'
      Origin = 'USUARIO_AUT_MODIF'
      Size = 31
    end
  end
  object dsProveedores: TDataSource
    DataSet = qryProveedores
    Left = 72
    Top = 376
  end
  object BindSourceDB2: TBindSourceDB
    DataSet = qryProveedores
    ScopeMappings = <>
    Left = 264
    Top = 280
  end
end
