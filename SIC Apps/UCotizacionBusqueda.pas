unit UCotizacionBusqueda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, Data.DB, FireDAC.Comp.Client, FireDAC.Comp.DataSet,
  Vcl.StdCtrls, Vcl.Grids, Vcl.DBGrids, Vcl.DBCtrls, Vcl.ComCtrls;

type
  TCotizacionBusqueda = class(TForm)
    txtBusqueda: TEdit;
    rb_nombre: TRadioButton;
    rb_clave: TRadioButton;
    db_articulos: TDBGrid;
    btnCotizar: TButton;
    DataSource1: TDataSource;
    Query: TFDQuery;
    Transaction: TFDTransaction;
    Conexion: TFDConnection;
    cb_almacenes: TDBLookupComboBox;
    Label1: TLabel;
    QueryAlmacenes: TFDQuery;
    dsAlmacenes: TDataSource;
    Label2: TLabel;
    dsPrecios: TDataSource;
    QueryPrecios: TFDQuery;
    cb_precios: TDBLookupComboBox;
    StatusBar1: TStatusBar;
    procedure txtBusquedaChange(Sender: TObject);
    procedure btnCotizarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cb_almacenesClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CotizacionBusqueda: TCotizacionBusqueda;

implementation

{$R *.dfm}

procedure TCotizacionBusqueda.btnCotizarClick(Sender: TObject);
var
  articulo_id,cotizacion_id, cliente_eventual_id, cond_pago_id, dir_cli_id :integer;
  clave_articulo, query_str, folio, nuevo_folio, clave_cliente :string;
  precio_unitario:double;
begin
  if (cb_almacenes.Text <> '') and (cb_precios.Text<>'') then
  begin
    // FOLIOS Y DATOS DE CLIENTE EVENTUAL
    folio := Conexion.ExecSQLScalar('select consecutivo from folios_ventas where tipo_docto = ''C'';');
    nuevo_folio := inttostr(strtoint(folio) + 1);
    query_str := 'update folios_ventas set consecutivo=:nuevo_folio where tipo_docto = ''C''';
    Conexion.ExecSQL(query_str,[nuevo_folio]);
    cliente_eventual_id := Conexion.ExecSQLScalar('select valor from registry where nombre = :nombre',['CLIENTE_EVENTUAL_ID']);
    clave_cliente := Conexion.ExecSQLScalar('select first 1 clave_cliente from claves_clientes where cliente_id = :clave_eventual',[cliente_eventual_id]);
    cond_pago_id := Conexion.ExecSQLScalar('select cond_pago_id from clientes where cliente_id = :cliente_id',[cliente_eventual_id]);
    dir_cli_id := Conexion.ExecSQLScalar('select dir_cli_id from dirs_clientes where cliente_id = :cliente_id and es_dir_ppal=:ppal',[cliente_eventual_id,'S']);

    // CREA ENCABEZADO COTIZACION
    query_str := 'insert into doctos_ve(docto_ve_id,tipo_docto,folio,fecha,clave_cliente,cliente_id,moneda_id,'+
              'tipo_cambio,sistema_origen,cond_pago_id, descripcion, tipo_dscto, estatus,dir_cli_id,dir_consig_id)'+
              ' values (:p0,:p1,:p2,current_date,:p3,:p4,:p5,:p6,:p7,:p8,:p9,:p10,:p11,:p12,:p13) returning docto_ve_id;';
    cotizacion_id := Conexion.ExecSQLScalar(query_str,[-1,'C',folio,clave_cliente,cliente_eventual_id,1,1,'VE',cond_pago_id,'COTIZACION APLICACION SIC','P','P',dir_cli_id,dir_cli_id]);

    //RECORRE LOS ARTICULOS
    Query.First;
    while not Query.Eof do
    begin
      articulo_id := Query.FieldByName('articulo_id').Value;
      clave_articulo := Query.FieldByName('clave').Value;
      precio_unitario := Conexion.ExecSQLScalar('select precio from precios_articulos where articulo_id=:articulo_id and precio_empresa_id=:precio_empresa',[articulo_id,cb_precios.KeyValue]);

      query_str := 'insert into doctos_ve_det(docto_ve_det_id,docto_ve_id,clave_articulo,articulo_id,unidades,precio_unitario,'+
              'precio_total_neto,rol)'+
              ' values (:p0,:p1,:p2,:p3,:p4,:p5,:p6,:p7);';
      Conexion.ExecSQL(query_str,[-1,cotizacion_id,clave_articulo,articulo_id,1,precio_unitario,precio_unitario,'N']);
      Query.Next;
    end;
    Conexion.ExecSQL('EXECUTE PROCEDURE CALC_TOTALES_DOCTO_VE(:docto_id,:solo_lectura);',[cotizacion_id,'N']);
    Conexion.ExecSQL('EXECUTE PROCEDURE APLICA_DOCTO_VE(:docto_ve_id);',[cotizacion_id]);
    showmessage('Cotizacion Creada con el folio '+folio+'.');
  end
  else
  begin
    showmessage('Seleccione el almac�n y la lista de Precio');
  end;
end;

procedure TCotizacionBusqueda.cb_almacenesClick(Sender: TObject);
begin
    txtBusquedaChange(nil);
end;

procedure TCotizacionBusqueda.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Application.Terminate;
end;

procedure TCotizacionBusqueda.FormShow(Sender: TObject);
begin
  QueryAlmacenes.Open;
  QueryPrecios.Open;
end;

procedure TCotizacionBusqueda.txtBusquedaChange(Sender: TObject);
begin
  Query.Close;
  if rb_nombre.Checked then
  begin
    Query.SQL.Text := 'select ar.articulo_id, ca.clave_articulo as clave, ar.nombre, inv.inv_fin_unid as existencia from articulos ar left join'+
    ' claves_articulos ca on ca.articulo_id = ar.articulo_id left JOIN'+
    ' orsp_in_aux_art(ar.articulo_id,'+QuotedStr(cb_almacenes.KeyValue)+',''01/01/1990'',current_date, ''N'',''N'') inv on inv.articulo_id = ar.articulo_id'+
    ' where ca.rol_clave_art_id =17 and upper(nombre) like Upper(''%'+txtBusqueda.Text+'%'') AND inv.inv_fin_unid > 0;';
  end
  else
  begin
     Query.SQL.Text := 'select ar.articulo_id, ca.clave_articulo as clave, ar.nombre, inv.inv_fin_unid as existencia from articulos ar left join'+
    ' claves_articulos ca on ca.articulo_id = ar.articulo_id left JOIN'+
    ' orsp_in_aux_art(ar.articulo_id,'+QuotedStr(cb_almacenes.KeyValue)+',''01/01/1990'',current_date, ''N'',''N'') inv on inv.articulo_id = ar.articulo_id'+
    ' where ca.rol_clave_art_id =17 and upper(clave_articulo) like Upper(''%'+txtBusqueda.Text+'%'') AND inv.inv_fin_unid > 0;';
  end;
  Query.Open;
  db_articulos.Columns[0].width :=0;
  db_articulos.Columns[1].width :=100;
  db_articulos.Columns[2].width :=150;
  db_articulos.Columns[3].width :=80;
end;
  

end.
