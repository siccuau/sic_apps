unit UEliminaHistorial;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef,
  FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client, Vcl.ComCtrls,
  Vcl.Samples.Gauges, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, Vcl.StdCtrls;

type
  TEliminaHistorial = class(TForm)
    StatusBar1: TStatusBar;
    Conexion: TFDConnection;
    btnEliminar: TButton;
    tbDoctosCC: TFDTable;
    txtAnio: TEdit;
    pbProgreso: TProgressBar;
    lblProgreso: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnEliminarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EliminaHistorial: TEliminaHistorial;

implementation

{$R *.dfm}

procedure TEliminaHistorial.btnEliminarClick(Sender: TObject);
var
   i:integer;
begin
     tbDoctosCC.Active := True;
     tbDoctosCC.Filter := 'fecha < ''01/01/'+txtAnio.Text+'''' ;
     tbdoctoscc.Filtered := True;
     tbdoctoscc.Refresh;
     //tbdoctoscc.FetchNext;
     pbProgreso.Max := tbDoctosCC.RecordCount;
     tbDoctosCC.First;
     while not tbDoctosCC.eof do
     begin
       pbProgreso.Position := pbProgreso.Position + 1;
       tbDoctosCC.Delete;
       //lblProgreso.Caption := FloatToStr((pbProgreso.Position/pbProgreso.Max*100));
       tbDoctosCC.Next;
     end;
     tbDoctosCC.Refresh;
     ShowMessage('Terminado');

end;

procedure TEliminaHistorial.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Application.Terminate;
end;

end.
