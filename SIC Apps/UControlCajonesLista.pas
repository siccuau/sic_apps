unit UControlCajonesLista;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.FB, FireDAC.Phys.FBDef, Vcl.ComCtrls, Data.DB,
  FireDAC.Comp.Client, Vcl.Grids, Vcl.DBGrids, Vcl.ImgList, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet,UControlCajonesEntradaSalida,
  FireDAC.VCLUI.Wait, System.ImageList;

type
  TControlCajonesLista = class(TForm)
    CmbTipo: TComboBox;
    LblTipo: TLabel;
    CmbClase: TComboBox;
    LblClase: TLabel;
    datos: TDBGrid;
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    Button1: TButton;
    ImageList1: TImageList;
    Query: TFDQuery;
    DataSource1: TDataSource;
    procedure Actualizar();
    procedure CmbTipoChange(Sender: TObject);
    procedure CmbClaseChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button1Click(Sender: TObject);
    procedure datosDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    modulo,precio_costo : string;
  end;

var
  ControlCajonesLista: TControlCajonesLista;

implementation

{$R *.dfm}
procedure TControlCajonesLista.Actualizar();
begin
  if CmbTipo.ItemIndex = 0 then
  begin
    //SI ES ENTRADA CON MANZANA
    if CmbClase.ItemIndex = 0 then
    begin
      Query.sql.Text := 'select docto_cm_id,folio,fecha,p.nombre as proveedor,'+
                        ' pr.nombre as productor,p.proveedor_id,pr.id as productor_id,dc.almacen_id,dc.sic_chofer,dc.sic_placas'+
                        ' from doctos_cm dc join'+
                        ' proveedores p on p.proveedor_id=dc.proveedor_id join'+
                        ' sic_productores pr on pr.id=dc.sic_productor'+
                        ' where tipo_docto=''R'' and dc.estatus <> ''C''';
      Query.Open;
      datos.Columns[0].Visible :=false;
      datos.Columns[1].Width :=60;
      datos.Columns[2].Width :=60;
      datos.Columns[3].Width :=250;
      datos.Columns[4].Width :=250;
      datos.Columns[5].Visible :=false;
      datos.Columns[6].Visible :=false;
      datos.Columns[7].Visible :=false;
      datos.Columns[8].Visible :=false;
      datos.Columns[9].Visible :=false;
      modulo := 'doctos_cm';
      precio_costo := 'precio_total_neto';
    end
    //SI ES ENTRADA VACIA
    else
    begin
      Query.sql.Text := 'select docto_in_id,FOLIO,fecha,p.nombre as proveedor,'+
                        ' pr.nombre as productor,DI.SIC_PROVEEDOR as proveedor_id,pr.id as productor_id,di.almacen_id,di.sic_chofer,di.sic_placas'+
                        ' from doctos_in di join'+
                        ' conceptos_in ci on ci.concepto_in_id=di.concepto_in_id join'+
                        ' sic_productores pr on pr.id=di.sic_productor join'+
                        ' proveedores p on p.proveedor_id=di.sic_proveedor'+
                        ' where ci.nombre=''ENTRADA CAJONES'' and di.sic_integ<>''S''';
                        Query.open;
      datos.Columns[0].Visible :=false;
      datos.Columns[1].Width :=60;
      datos.Columns[2].Width :=60;
      datos.Columns[3].Width :=250;
      datos.Columns[4].Width :=250;
      datos.Columns[5].Visible :=false;
      datos.Columns[6].Visible :=false;
      datos.Columns[7].Visible :=false;
      datos.Columns[8].Visible :=false;
      datos.Columns[9].Visible :=false;
      modulo := 'doctos_in';
      precio_costo := 'costo_total';
    end;
  end
  //SI ES SALIDA (SOLO PUEDE SER VACIA)
  else
  begin
    Query.sql.Text := 'select docto_in_id,FOLIO,fecha,''.'' as proveedor,'+
                      ' pr.nombre as productor,di.sic_proveedor as proveedor_id,pr.id as productor_id,di.almacen_id,di.sic_chofer,di.sic_placas'+
                      ' from doctos_in di join'+
                        ' conceptos_in ci on ci.concepto_in_id=di.concepto_in_id join'+
                        ' sic_productores pr on pr.id=di.sic_productor'+
                        ' where ci.nombre=''SALIDA CAJONES'' and di.sic_integ<>''S''';
                        Query.open;
      datos.Columns[0].Visible :=false;
      datos.Columns[1].Width :=60;
      datos.Columns[2].Width :=60;
      datos.Columns[3].Width :=250;
      datos.Columns[4].Width :=250;
      datos.Columns[5].Visible :=false;
      datos.Columns[6].Visible :=false;
      datos.Columns[7].Visible :=false;
      datos.Columns[8].Visible :=false;
      datos.Columns[9].Visible :=false;
      modulo := 'doctos_in';
      precio_costo := 'costo_total';
  end;
end;

procedure TControlCajonesLista.Button1Click(Sender: TObject);
var
  FormDocumento:TControlCajonesEntradaSalida;
begin
  FormDocumento := TControlCajonesEntradaSalida.Create(nil);
  FormDocumento.Conexion.Params := Conexion.Params;
  FormDocumento.docto_id := 0;
  FormDocumento.btnEtiquetas.Visible := false;
  FormDocumento.btnRecibo.visible := false;
  FormDocumento.ShowModal;
  Actualizar;
end;

procedure TControlCajonesLista.CmbClaseChange(Sender: TObject);
begin
  Actualizar;
end;

procedure TControlCajonesLista.CmbTipoChange(Sender: TObject);
begin
  Actualizar;
  case CmbTipo.ItemIndex of
    //********* ENTRADA**********
    0:begin
      CmbClase.items.Clear;
      CmbClase.items.Add('Con Manzana');
      CmbClase.items.Add('Vacios');
      CmbClase.ItemIndex := 0;
      CmbClase.Enabled := True;
    end;
    //******* SALIDA ************
    1:begin
      CmbClase.items.Clear;
      CmbClase.items.Add('Vacios');
      CmbClase.ItemIndex := 0;
      CmbClase.Enabled := False;
    end;
  end;
end;

procedure TControlCajonesLista.datosDblClick(Sender: TObject);
var
  FormDocumento : TControlCajonesEntradaSalida;
begin
  FormDocumento := TControlCajonesEntradaSalida.Create(nil);
  FormDocumento.Conexion.Params := Conexion.Params;
  FormDocumento.docto_id := Query.Fields[0].asinteger;
  //DATOS GENERALES
  FormDocumento.CmbTipo.Text := CmbTipo.Text;
  FormDocumento.CmbTipo.Enabled := false;
  FormDocumento.CmbClase.text := CmbClase.Text;
  FormDocumento.CmbClase.Enabled := false;
  FormDocumento.cmbProveedor.keyvalue := Query.FieldByName('proveedor_id').AsInteger;
  FormDocumento.cmbProveedor.Enabled := false;
  FormDocumento.cmbProductor.keyvalue := Query.FieldByName('productor_id').AsInteger;
  FormDocumento.cmbProductor.Enabled := false;
  FormDocumento.txtClaveProductor.Text := Conexion.ExecSQLScalar('select clave from sic_productores where id=:pid',[Query.FieldByName('productor_id').AsInteger]);
  FormDocumento.txtClaveProductor.Enabled := False;
  FormDocumento.btnGuardar.Visible := false;
  FormDocumento.dtpFecha.Date := StrToDate(Query.FieldByName('fecha').AsString);
  FormDocumento.dtpFecha.Enabled := false;
  FormDocumento.TxtFolio.Text := Query.fieldbyname('folio').AsString;
  FormDocumento.TxtFolio.Enabled := false;
  FormDocumento.cmbAlmacenes.KeyValue := Query.FieldByName('almacen_id').AsInteger;
  FormDocumento.cmbAlmacenes.Enabled := false;
  FormDocumento.txtChofer.Text := Query.FieldByName('sic_chofer').AsString;
  FormDocumento.txtChofer.Enabled := false;
  FormDocumento.txtPlacas.Text := Query.FieldByName('sic_placas').AsString;
  FormDocumento.txtPlacas.Enabled := false;

  if ((cmbTipo.ItemIndex=0) and (cmbClase.ItemIndex = 0))
  then FormDocumento.btnEtiquetas.Visible := true
  else FormDocumento.btnEtiquetas.Visible := false;

  //CALCULA TOTALES
  FormDocumento.txtTotalCajones.Text := Conexion.ExecSQLScalar('select coalesce(sum(sic_num_cajones),0) from '+modulo+'_det where '+modulo.Replace('s','')+'_id=:did',[inttostr(Query.Fields[0].asinteger)]);
  FormDocumento.txtTotalPesoBruto.Text := Conexion.ExecSQLScalar('select coalesce(sum(sic_peso_bruto),0) from '+modulo+'_det where '+modulo.Replace('s','')+'_id=:did',[inttostr(Query.Fields[0].asinteger)]);
  FormDocumento.txtTotalCosto.text := Conexion.ExecSQLScalar('select coalesce(sum('+precio_costo+'),0) from '+modulo+'_det where '+modulo.Replace('s','')+'_id=:did',[inttostr(Query.Fields[0].asinteger)]);
  FormDocumento.txtTotalTara.Text := Conexion.ExecSQLScalar('select coalesce(sum(sic_tara),0) from '+modulo+'_det where '+modulo.Replace('s','')+'_id=:did',[inttostr(Query.Fields[0].asinteger)]);
  FormDocumento.txtTotalPesoNeto.text := Conexion.ExecSQLScalar('select coalesce(sum(unidades),0) from '+modulo+'_det where '+modulo.Replace('s','')+'_id=:did',[inttostr(Query.Fields[0].asinteger)]);
  FormDocumento.txtPromedioporCajon.Text := Conexion.ExecSQLScalar('select coalesce(sum(unidades)/sum(sic_num_cajones),0) from '+modulo+'_det where '+modulo.Replace('s','')+'_id=:did',[inttostr(Query.Fields[0].asinteger)]);

  if ((cmbTipo.ItemIndex=0) and (cmbClase.ItemIndex = 0)) then
  begin
    FormDocumento.QueryDetalles.sql.Text := ' select  dcd.sic_num_cajones as cajones,'+
                                      ' a.nombre as articulo,'+
                                      ' dcd.precio_unitario as costo_unitario,'+
                                      ' dcd.precio_total_neto as total,'+
                                      ' dcd.sic_peso_bruto as peso_bruto,'+
                                      ' dcd.sic_tara as tara,'+
                                      ' dcd.unidades as peso_neto,'+
                                      ' dcd.sic_propietario_cajon as propietario_del_cajon'+
                              ' from doctos_cm_det dcd join'+
                              ' articulos a on a.articulo_id=dcd.articulo_id'+
                              ' where dcd.docto_cm_id ='+IntToStr(Query.Fields[0].asinteger);
    FormDocumento.QueryDetalles.Open;
    FormDocumento.grid_datos.Columns[0].Width := 45;
    FormDocumento.grid_datos.Columns[1].Width  := 350;
    FormDocumento.grid_datos.Columns[2].Width  := 60;
    FormDocumento.grid_datos.Columns[3].Width  := 80;
    FormDocumento.grid_datos.Columns[4].Width  := 80;
    FormDocumento.grid_datos.Columns[5].Width  := 60;
    FormDocumento.grid_datos.Columns[6].Width  := 60;
    FormDocumento.grid_datos.Columns[7].Width  := 140;
    FormDocumento.grid_datos.Visible := true;
    FormDocumento.grid_datos.Enabled := false;
  end;
  FormDocumento.datos.Visible := false;
  FormDocumento.ShowModal;

end;

procedure TControlCajonesLista.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Conexion.Close;
  //Application.Terminate;

end;

procedure TControlCajonesLista.FormShow(Sender: TObject);
begin
  Conexion.StartTransaction;
  Conexion.ExecSQL(' EXECUTE block'+
                    ' as'+
                    ' BEGIN'+
                        ' if (not exists(select 1 from rdb$relations where rdb$relation_name = ''SIC_PRODUCTORES'')) then'+
                        ' BEGIN'+
                            ' execute statement ''create table SIC_PRODUCTORES ( id integer,PROVEEDOR_ID integer, clave VARCHAR(20), NOMBRE VARCHAR(50) );'';'+
                            ' execute statement ''CREATE GENERATOR sic_id_productor;'';'+
                            ' execute statement ''set GENERATOR sic_id_productor TO 0;'';'+
                            ' execute statement ''CREATE TRIGGER SIC_PRODUCTORES_BI0 FOR SIC_PRODUCTORES'+
                                                    ' ACTIVE BEFORE INSERT POSITION 0'+
                                                    ' AS'+
                                                    ' BEGIN'+
                                                        ' if (NEW.ID is NULL) then NEW.ID = GEN_ID(sic_id_productor, 1);'+
                                                    ' END'';'+
                        ' END'+
                    ' END');

  Conexion.ExecSQL(' EXECUTE block'+
                    ' as'+
                    ' BEGIN'+
                        ' /*****************************CAMPOS PARA LA TABLA DOCTOS CM DET*******************/'+
                        ' if (not exists('+
                        ' select 1 from RDB$RELATION_FIELDS rf'+
                        ' where rf.RDB$RELATION_NAME = ''DOCTOS_CM_DET'' and rf.RDB$FIELD_NAME = ''SIC_TARA''))'+
                        ' then'+
                        ' execute statement ''ALTER TABLE DOCTOS_CM_DET ADD SIC_TARA double PRECISION'';'+
                    ' '+
                        ' if (not exists('+
                        ' select 1 from RDB$RELATION_FIELDS rf'+
                        ' where rf.RDB$RELATION_NAME = ''DOCTOS_CM_DET'' and rf.RDB$FIELD_NAME = ''SIC_PESO_BRUTO''))'+
                        ' then'+
                        ' execute statement ''ALTER TABLE DOCTOS_CM_DET ADD SIC_PESO_BRUTO double PRECISION'';'+
                    ' '+
                        ' if (not exists('+
                        ' select 1 from RDB$RELATION_FIELDS rf'+
                        ' where rf.RDB$RELATION_NAME = ''DOCTOS_CM_DET'' and rf.RDB$FIELD_NAME = ''SIC_NUM_CAJONES''))'+
                        ' then'+
                        ' execute statement ''ALTER TABLE DOCTOS_CM_DET ADD SIC_NUM_CAJONES INTEGER'';'+
                    ' '+
                        ' if (not exists('+
                        ' select 1 from RDB$RELATION_FIELDS rf'+
                        ' where rf.RDB$RELATION_NAME = ''DOCTOS_CM_DET'' and rf.RDB$FIELD_NAME = ''SIC_PROPIETARIO_CAJON''))'+
                        ' then'+
                        ' execute statement ''ALTER TABLE DOCTOS_CM_DET ADD SIC_PROPIETARIO_CAJON VARCHAR(50)'';'+
                    ' '+
                        ' /*****************************CAMPOS PARA LA TABLA DOCTOS IN DET*******************/'+
                        ' if (not exists('+
                        ' select 1 from RDB$RELATION_FIELDS rf'+
                        ' where rf.RDB$RELATION_NAME = ''DOCTOS_IN_DET'' and rf.RDB$FIELD_NAME = ''SIC_TARA''))'+
                        ' then'+
                        ' execute statement ''ALTER TABLE DOCTOS_IN_DET ADD SIC_TARA double PRECISION'';'+
                    ' '+
                        ' if (not exists('+
                        ' select 1 from RDB$RELATION_FIELDS rf'+
                        ' where rf.RDB$RELATION_NAME = ''DOCTOS_IN_DET'' and rf.RDB$FIELD_NAME = ''SIC_PESO_BRUTO''))'+
                        ' then'+
                        ' execute statement ''ALTER TABLE DOCTOS_IN_DET ADD SIC_PESO_BRUTO double PRECISION'';'+
                    ' '+
                        ' if (not exists('+
                        ' select 1 from RDB$RELATION_FIELDS rf'+
                        ' where rf.RDB$RELATION_NAME = ''DOCTOS_IN_DET'' and rf.RDB$FIELD_NAME = ''SIC_NUM_CAJONES''))'+
                        ' then'+
                        ' execute statement ''ALTER TABLE DOCTOS_IN_DET ADD SIC_NUM_CAJONES INTEGER'';'+
                    ' '+
                        ' if (not exists('+
                        ' select 1 from RDB$RELATION_FIELDS rf'+
                        ' where rf.RDB$RELATION_NAME = ''DOCTOS_IN_DET'' and rf.RDB$FIELD_NAME = ''SIC_PROPIETARIO_CAJON''))'+
                        ' then'+
                        ' execute statement ''ALTER TABLE DOCTOS_IN_DET ADD SIC_PROPIETARIO_CAJON VARCHAR(50)'';'+
                    ' '+
                        ' /*****************************CAMPOS PARA LA TABLA DOCTOS CM*******************/'+
                        ' if (not exists('+
                        ' select 1 from RDB$RELATION_FIELDS rf'+
                        ' where rf.RDB$RELATION_NAME = ''DOCTOS_CM'' and rf.RDB$FIELD_NAME = ''SIC_PRODUCTOR''))'+
                        ' then'+
                        ' execute statement ''ALTER TABLE DOCTOS_CM ADD SIC_PRODUCTOR INTEGER'';'+
                    ' '+
                        ' if (not exists('+
                        ' select 1 from RDB$RELATION_FIELDS rf'+
                        ' where rf.RDB$RELATION_NAME = ''DOCTOS_CM'' and rf.RDB$FIELD_NAME = ''SIC_CHOFER''))'+
                        ' then'+
                        ' execute statement ''ALTER TABLE DOCTOS_CM ADD SIC_CHOFER VARCHAR(50)'';'+
                    ' '+
                        ' if (not exists('+
                        ' select 1 from RDB$RELATION_FIELDS rf'+
                        ' where rf.RDB$RELATION_NAME = ''DOCTOS_CM'' and rf.RDB$FIELD_NAME = ''SIC_PLACAS''))'+
                        ' then'+
                        ' execute statement ''ALTER TABLE DOCTOS_CM ADD SIC_PLACAS VARCHAR(20)'';'+
                    ' '+
                        ' /*****************************CAMPOS PARA LA TABLA DOCTOS IN *******************/'+
                        ' if (not exists('+
                        ' select 1 from RDB$RELATION_FIELDS rf'+
                        ' where rf.RDB$RELATION_NAME = ''DOCTOS_IN'' and rf.RDB$FIELD_NAME = ''SIC_PRODUCTOR''))'+
                        ' then'+
                        ' execute statement ''ALTER TABLE DOCTOS_IN ADD SIC_PRODUCTOR INTEGER'';'+
                    ' '+
                        ' if (not exists('+
                        ' select 1 from RDB$RELATION_FIELDS rf'+
                        ' where rf.RDB$RELATION_NAME = ''DOCTOS_IN'' and rf.RDB$FIELD_NAME = ''SIC_CHOFER''))'+
                        ' then'+
                        ' execute statement ''ALTER TABLE DOCTOS_IN ADD SIC_CHOFER VARCHAR(50)'';'+
                    ' '+
                        ' if (not exists('+
                        ' select 1 from RDB$RELATION_FIELDS rf'+
                        ' where rf.RDB$RELATION_NAME = ''DOCTOS_IN'' and rf.RDB$FIELD_NAME = ''SIC_PLACAS''))'+
                        ' then'+
                        ' execute statement ''ALTER TABLE DOCTOS_IN ADD SIC_PLACAS VARCHAR(20)'';'+
                    ' '+
                        ' if (not exists('+
                        ' select 1 from RDB$RELATION_FIELDS rf'+
                        ' where rf.RDB$RELATION_NAME = ''DOCTOS_IN'' and rf.RDB$FIELD_NAME = ''SIC_INTEG''))'+
                        ' then'+
                        ' execute statement ''ALTER TABLE DOCTOS_IN ADD SIC_INTEG VARCHAR(1)'';'+
                    ' '+
                        ' if (not exists('+
                        ' select 1 from RDB$RELATION_FIELDS rf'+
                        ' where rf.RDB$RELATION_NAME = ''DOCTOS_IN'' and rf.RDB$FIELD_NAME = ''SIC_PROVEEDOR''))'+
                        ' then'+
                        ' execute statement ''ALTER TABLE DOCTOS_IN ADD SIC_PROVEEDOR INTEGER'';'+
                    ' END');
  Conexion.Commit;
  Actualizar;

end;

end.
