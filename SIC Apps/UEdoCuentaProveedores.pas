unit UEdoCuentaProveedores;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef,
  FireDAC.VCLUI.Wait, Vcl.ComCtrls, Data.DB, FireDAC.Comp.Client,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, Vcl.StdCtrls, System.Rtti, System.Bindings.Outputs,
  Vcl.Bind.Editors, Data.Bind.EngExt, Vcl.Bind.DBEngExt,
  Data.Bind.Components, Data.Bind.DBScope, frxClass, frxDBSet, IdMessage,
  frxExportPDF, IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack, IdSSL,
  IdSSLOpenSSL, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase, IdSMTP, IdAttachmentFile;

type
  TEdoCuentaProveedores = class(TForm)
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    btnEnviar: TButton;
    rbTodos: TRadioButton;
    rbUnProveedor: TRadioButton;
    cbProveedores: TComboBox;
    qryProveedores: TFDQuery;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkListControlToField1: TLinkListControlToField;
    ConexionConfig: TFDConnection;
    qryCorreos: TFDQuery;
    cbCorreos: TComboBox;
    btnVistaPrevia: TButton;
    Label1: TLabel;
    BindSourceDB2: TBindSourceDB;
    LinkListControlToField2: TLinkListControlToField;
    txtDatosCorreo: TEdit;
    LinkControlToField1: TLinkControlToField;
    Reporte: TfrxReport;
    rptDSEdoCuenta: TfrxDBDataset;
    qryEdoCuenta: TFDQuery;
    IdSMTP1: TIdSMTP;
    SSLHandler: TIdSSLIOHandlerSocketOpenSSL;
    exportpdf: TfrxPDFExport;
    IdMessage: TIdMessage;
    lblDestinatario: TLabel;
    txtDestinatario: TEdit;
    LinkControlToField2: TLinkControlToField;
    btnActualizarProveedores: TButton;
    GroupBox3: TGroupBox;
    dtpFechaInicio: TDateTimePicker;
    Label2: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure rbUnProveedorClick(Sender: TObject);
    procedure rbTodosClick(Sender: TObject);
    procedure btnVistaPreviaClick(Sender: TObject);
    function enviar : boolean;
    procedure btnEnviarClick(Sender: TObject);
    procedure btnActualizarProveedoresClick(Sender: TObject);
    procedure dtpFechaInicioChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EdoCuentaProveedores: TEdoCuentaProveedores;

implementation

{$R *.dfm}
function TEdoCuentaProveedores.enviar : boolean;
var
  fecha:string;
  fromc,fromm,destinos,usuario,pwd,smtp,puerto:string;
  SSLHandler: TIdSSLIOHandlerSocketOpenSSL;
  enviado : boolean;

begin
  fecha := FormatDateTime('mmddyyyy', Date);
  exportpdf.FileName :=ExtractFilePath(Application.ExeName)+'auxiliar_'+fecha+'.pdf';
  Reporte.preparereport;
  Reporte.Export(exportpdf);
  fromc := qryCorreos.FieldByName('dir_correo').AsString;
  fromm := qryCorreos.FieldByName('descripcion').AsString;
  pwd := qryCorreos.FieldByName('password').AsString;
  smtp := qryCorreos.FieldByName('servidor').AsString;
  puerto := qryCorreos.FieldByName('puerto').AsString;

  With IdMessage Do
     Begin
          Body.Clear;
          Body.Add('Se le Adjunta el Auxiliar de Movimientos'+#13+'Atte. Su cliente '+ qryEdoCuenta.FieldByName('nombre_empresa').AsString);
          From.Text := fromm;
          Recipients.EMailAddresses := txtDestinatario.Text;
          Subject := 'Auxiliar de Movimientos ('+qryEdoCuenta.FieldByName('nombre_empresa').AsString+')';
          Priority := TidMessagePriority(mpHighest);
          TIdAttachmentfile.Create(IdMessage.MessageParts,Concat(exportpdf.FileName));
     End;
     With IdSMTP1 do
     Begin
          Username := fromc;
          Password := pwd;
          Host     := smtp;
          Port     := strtoint(puerto);
          try
             Connect;
             Try
                Send(IdMessage);
             Finally
                Disconnect;
             End;
             //ShowMessage('Se envio correo en forma correcta');
             enviado := True;
          Except
              //ShowMessage('Fallo en el envio de email');
              enviado := False;
          End;
         If Connected Then Disconnect;
     End;
     deletefile(exportpdf.FileName);
     Result := enviado;
end;

procedure TEdoCuentaProveedores.btnActualizarProveedoresClick(
  Sender: TObject);
begin
	qryProveedores.Refresh;
end;

procedure TEdoCuentaProveedores.btnEnviarClick(Sender: TObject);
var
	enviados,no_enviados: integer;
begin
	btnEnviar.Enabled := False;
  btnVistaPrevia.Enabled := False;
	if rbUnProveedor.Checked then
  begin
    qryEdoCuenta.Filter := 'proveedor_id='+qryProveedores.FieldByName('proveedor_id').AsString;
    qryEdoCuenta.Filtered := True;
    qryEdoCuenta.Refresh;
    if qryEdoCuenta.RecordCount > 0 then
    begin
      Reporte.PrepareReport(True);
      if qryProveedores.FieldByName('email').asstring <> ''then
      begin
        StatusBar1.Panels.Items[1].Text := 'Enviando a ' + qryProveedores.FieldByName('email').AsString;
        if enviar then
        	enviados := enviados+1
        else
          no_enviados := no_enviados + 1;
      end
      else
      begin
        ShowMessage('Indique la direccion de Correo');
      end
    end
    else
    begin
      ShowMessage('No hay Cargos de este proveedor');
    end;
  end
  else
  begin
    qryProveedores.First;
    while not qryProveedores.eof do
    begin
      qryEdoCuenta.Filter := 'proveedor_id='+qryProveedores.FieldByName('proveedor_id').AsString;
      qryEdoCuenta.Filtered := True;
      qryEdoCuenta.Refresh;
    if qryEdoCuenta.RecordCount > 0 then
    begin
      Reporte.PrepareReport(True);
      if qryProveedores.FieldByName('email').AsString <> '' then
      begin
	      StatusBar1.Panels.Items[1].Text := 'Enviando a ' + qryProveedores.FieldByName('email').AsString;
        if enviar then
        	enviados := enviados+1
        else
          no_enviados := no_enviados + 1;
      end
      else
      begin
      	no_enviados := no_enviados + 1;
      end;
    end;
      qryProveedores.Next;
    end;
    qryProveedores.First;
  end;
  StatusBar1.Panels.Items[1].Text := 'Listo para Enviar';
  ShowMessage('Proceso completado'+#13+IntToStr(enviados)+' Correos enviados Correctamente.'+#13+IntToStr(no_enviados)+' Correos NO enviados.');
  btnEnviar.Enabled := True;
  btnVistaPrevia.Enabled := True;
  qryProveedores.Refresh;
end;

procedure TEdoCuentaProveedores.btnVistaPreviaClick(Sender: TObject);
begin
  if rbUnProveedor.Checked then
  begin
    qryEdoCuenta.Filter := 'proveedor_id='+qryProveedores.FieldByName('proveedor_id').AsString;
    qryEdoCuenta.Filtered := True;
    qryEdoCuenta.Refresh;
    if qryEdoCuenta.RecordCount > 0 then
    begin
      Reporte.PrepareReport(True);
      Reporte.ShowReport;
    end
    else
    begin
      ShowMessage('No hay Cargos de este proveedor');
    end;
  end
  else
  begin
    qryProveedores.First;
    while not qryProveedores.eof do
    begin
      qryEdoCuenta.Filter := 'proveedor_id='+qryProveedores.FieldByName('proveedor_id').AsString;
      qryEdoCuenta.Filtered := True;
      qryEdoCuenta.Refresh;
    if qryEdoCuenta.RecordCount > 0 then
    begin
      Reporte.PrepareReport(True);
      Reporte.ShowReport;
    end;
      qryProveedores.Next;
    end;
  end;
end;

procedure TEdoCuentaProveedores.dtpFechaInicioChange(Sender: TObject);
begin
	qryEdoCuenta.Close;
  qryEdoCuenta.ParamByName('f_fecha_inicio').AsDate := dtpFechaInicio.Date;
  qryEdoCuenta.Open;
end;

procedure TEdoCuentaProveedores.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     qryProveedores.Close;
     qryCorreos.Close;
     qryEdoCuenta.Close;
     Application.Terminate;
end;

procedure TEdoCuentaProveedores.FormShow(Sender: TObject);
begin
     qryProveedores.Open;
     ConexionConfig.Params.Values['Database'] := Conexion.Params.Values['Database'].Replace(StatusBar1.Panels.Items[0].Text,'system\config');
     ConexionConfig.Params.Values['Server'] := Conexion.Params.Values['Server'];
     ConexionConfig.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
     ConexionConfig.Params.Values['Password'] := Conexion.Params.Values['Password'];
     ConexionConfig.Open;
     QryCorreos.Open;
     qryEdoCuenta.ParamByName('f_fecha_inicio').AsDate := dtpFechaInicio.Date;
     qryEdoCuenta.Open;
end;

procedure TEdoCuentaProveedores.rbTodosClick(Sender: TObject);
begin
     cbProveedores.Enabled := rbUnProveedor.Checked;
end;

procedure TEdoCuentaProveedores.rbUnProveedorClick(Sender: TObject);
begin
     cbProveedores.Enabled := rbUnProveedor.Checked;
end;

end.
