unit UControlCajonesEmpacadorasAdmin;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, Vcl.ComCtrls, Data.DB, FireDAC.Comp.Client, Vcl.StdCtrls;

type
  TControlCajonesEmpacadorasAdmin = class(TForm)
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    btnGuardar: TButton;
    txtNombre: TEdit;
    txtPagoUnidad: TEdit;
    txtConsecutivo: TEdit;
    procedure btnGuardarClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
    id:integer;
  end;

var
  ControlCajonesEmpacadorasAdmin: TControlCajonesEmpacadorasAdmin;

implementation

{$R *.dfm}

procedure TControlCajonesEmpacadorasAdmin.btnGuardarClick(Sender: TObject);
begin
  if id = 0 then
  begin
    Conexion.ExecSQL('insert into sic_empacadoras values(NULL,'''+txtNombre.Text+''',:pago,:consec)',[txtPagoUnidad.Text,txtConsecutivo.Text])
  end
  else
  begin
    Conexion.ExecSQL('update sic_empacadoras set nombre='''+txtNombre.Text+''',pagoxunidad=:pago,consec =:con where id=:eid',[txtpagoUnidad.Text,txtconsecutivo.Text,id]);
  end;
  ModalResult := mrOk;
end;

end.


