unit UConfirmaCostoLiquidacion;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TConfirmaCostoLiquidacion = class(TForm)
    Label1: TLabel;
    txtCosto: TEdit;
    Button1: TButton;
    Button2: TButton;
    lblCalculado: TLabel;
    btnCancelar: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    costo : double;
  end;

var
  ConfirmaCostoLiquidacion: TConfirmaCostoLiquidacion;

implementation

{$R *.dfm}

procedure TConfirmaCostoLiquidacion.btnCancelarClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TConfirmaCostoLiquidacion.Button1Click(Sender: TObject);
begin
  costo := strtofloat(txtCosto.Text);
  ModalResult := mrOk;
end;

procedure TConfirmaCostoLiquidacion.Button2Click(Sender: TObject);
begin
  ModalResult := mrNo;
end;

end.
