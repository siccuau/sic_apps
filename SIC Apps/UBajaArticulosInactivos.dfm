object BajaArticulosInactivos: TBajaArticulosInactivos
  Left = 0
  Top = 0
  Caption = 'BajaArticulosInactivos'
  ClientHeight = 526
  ClientWidth = 803
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 24
    Width = 205
    Height = 13
    Caption = 'Dar de baja Articulos sin movimiento desde'
  end
  object lblNumArticulos: TLabel
    Left = 8
    Top = 75
    Width = 3
    Height = 13
  end
  object Label2: TLabel
    Left = 8
    Top = 54
    Width = 40
    Height = 13
    Caption = 'Almac'#233'n'
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 507
    Width = 803
    Height = 19
    Panels = <
      item
        Width = 50
      end
      item
        Width = 50
      end>
  end
  object clbArticulos: TCheckListBox
    Left = 8
    Top = 120
    Width = 787
    Height = 381
    ItemHeight = 13
    TabOrder = 1
  end
  object dtpFecha: TDateTimePicker
    Left = 232
    Top = 20
    Width = 161
    Height = 21
    Date = 43272.455817708330000000
    Time = 43272.455817708330000000
    TabOrder = 2
  end
  object btnBuscar: TButton
    Left = 424
    Top = 19
    Width = 75
    Height = 25
    Caption = 'Buscar'
    TabOrder = 3
    OnClick = btnBuscarClick
  end
  object cbAlmacenes: TComboBox
    Left = 232
    Top = 47
    Width = 161
    Height = 21
    TabOrder = 4
    Text = 'ALMACEN CEDIS'
  end
  object cbTodos: TCheckBox
    Left = 8
    Top = 97
    Width = 161
    Height = 17
    Caption = 'Marcar/Desmarcar Todos'
    TabOrder = 5
    OnClick = cbTodosClick
  end
  object btnIniciar: TButton
    Left = 408
    Top = 89
    Width = 161
    Height = 25
    Caption = 'Iniciar proceso'
    TabOrder = 6
    OnClick = btnIniciarClick
  end
  object cbDardeBaja: TCheckBox
    Left = 232
    Top = 74
    Width = 161
    Height = 17
    Caption = 'Dar de baja'
    Checked = True
    State = cbChecked
    TabOrder = 7
  end
  object cbInventarioacero: TCheckBox
    Left = 232
    Top = 97
    Width = 161
    Height = 17
    Caption = 'Existencia a Cero'
    TabOrder = 8
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'DriverID=FB'
      'Protocol=TCPIP'
      'User_Name=sysdba')
    LoginPrompt = False
    Left = 691
    Top = 24
  end
  object qryArticulos: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select  a.nombre,'
      '        a.articulo_id,'
      '        count(aux.folio) from articulos a left join'
      
        'auxiliar_artalm(a.articulo_id,:almacen_id,:fecha,current_date,0,' +
        '0) aux on aux.articulo_id = a.articulo_id'
      'where a.estatus = '#39'A'#39
      'group by a.nombre, a.articulo_id'
      'having count(aux.folio) = 0'
      ''
      
        '/*select a.nombre,a.articulo_id from articulos a order by nombre' +
        '*/'
      '')
    Left = 568
    Top = 16
    ParamData = <
      item
        Name = 'ALMACEN_ID'
        ParamType = ptInput
      end
      item
        Name = 'FECHA'
        ParamType = ptInput
      end>
  end
  object qryalmacenes: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from almacenes order by nombre')
    Left = 520
    Top = 16
  end
  object BindSourceDB1: TBindSourceDB
    DataSet = qryalmacenes
    ScopeMappings = <>
    Left = 760
    Top = 64
  end
  object BindingsList1: TBindingsList
    Methods = <>
    OutputConverters = <>
    Left = 732
    Top = 29
    object LinkListControlToField1: TLinkListControlToField
      Category = 'Quick Bindings'
      DataSource = BindSourceDB1
      FieldName = 'NOMBRE'
      Control = cbAlmacenes
      FillExpressions = <>
      FillHeaderExpressions = <>
      FillBreakGroups = <>
    end
  end
  object qryDoctosInvFis: TFDQuery
    CachedUpdates = True
    Connection = Conexion
    UpdateOptions.AssignedValues = [uvAutoCommitUpdates]
    UpdateOptions.AutoCommitUpdates = True
    SQL.Strings = (
      'select * from doctos_invfis order by DOCTO_INVFIS_ID')
    Left = 632
    Top = 8
  end
  object qryDoctosInvFisDet: TFDQuery
    CachedUpdates = True
    Connection = Conexion
    SQL.Strings = (
      'select * from doctos_invfis_det')
    Left = 624
    Top = 64
  end
end
