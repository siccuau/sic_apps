unit UCambioCostosArchivo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.StdCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef,
  FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client, ActiveX, ComObj;

type
  TCambioCostosArchivo = class(TForm)
    btnSeleccionarArchivo: TButton;
    btnIniciar: TButton;
    lblRutaArchivo: TLabel;
    odArchivo: TOpenDialog;
    pbProgreso: TProgressBar;
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSeleccionarArchivoClick(Sender: TObject);
    procedure btnIniciarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CambioCostosArchivo: TCambioCostosArchivo;

implementation

{$R *.dfm}

procedure TCambioCostosArchivo.btnIniciarClick(Sender: TObject);
var
  logfile :TextFile;
  XL: OleVariant;
  i, num_rows, art_principal_id, art_alterno_id, articulo_id, documento_id, concepto_id : integer;
  costo, unidades : double;
  clave_principal, clave_alterna, query_str, folio, nuevo_folio, clave_articulo : string;
  fecha_hora_str:string;
  actual:TDateTime;
begin
  //SE CREA LA INSTANCIA DEL ARCHIVO DE TEXTO
  AssignFile(logfile,'log_'+fecha_hora_str+'.txt');
  ReWrite(logfile);
  // SE CREA LA INSTANCIA DEL ARCHIVO DE EXCEL
  XL := CreateOleObject('Excel.Application');
  XL.WorkBooks.Open(odArchivo.FileName);
  num_rows := XL.Activesheet.UsedRange.Rows.Count;
  pbProgreso.Position := 0;
  pbProgreso.Max := num_rows;

  for i := 1 to num_rows do
  begin
      clave_articulo := '';
      articulo_id := -1;
      //SE BUSCA EL ARTICULO POR CLAVE
      clave_principal := XL.ActiveSheet.Cells[i,1].Value;
      costo := XL.ActiveSheet.Cells[i,2].Value;

      art_principal_id := Conexion.ExecSQLScalar('select articulo_id from claves_articulos where clave_articulo =:clave',[clave_principal]);

      //SE VALIDA SI ENCUENTRA POR PRINCIPAL
      if art_principal_id > 0 then
      begin
        articulo_id := art_principal_id;
        clave_articulo := clave_principal;
      end;


      if articulo_id > 0 then
      begin
        Conexion.ExecSQL('update articulos set costo_ultima_compra = :costo where articulo_id = :id',[costo,articulo_id]);
      end
      else
      begin
        //SE REGISTRA EL RENGLON EL LA LISTA TEMPORAL PARA EL REPORTE FINAL.
        WriteLn(logfile,inttostr(i)+#9+clave_principal+#9+floattostr(costo));
      end;

      //INFORMACION VISUAL
      pbProgreso.Position := pbProgreso.Position + 1;
  end;
  XL.Quit;
  XL := Unassigned;
  CloseFile(logfile);
  showmessage('Proceso Terminado.');
  pbProgreso.Position := 0;

end;

procedure TCambioCostosArchivo.btnSeleccionarArchivoClick(Sender: TObject);
begin
     if odArchivo.Execute then
     begin
       lblRutaArchivo.Caption := odArchivo.Filename;
     end;
end;

procedure TCambioCostosArchivo.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Application.Terminate;
end;

end.
