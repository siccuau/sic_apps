object ImportaSxacreditar: TImportaSxacreditar
  Left = 0
  Top = 0
  Caption = 'Importa Saldos por Acreditar'
  ClientHeight = 205
  ClientWidth = 272
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lbl_archivo: TLabel
    Left = 88
    Top = 29
    Width = 3
    Height = 13
  end
  object lbl_progress: TLabel
    Left = 102
    Top = 113
    Width = 3
    Height = 13
    Alignment = taCenter
  end
  object btn_archivo: TButton
    Left = 32
    Top = 24
    Width = 41
    Height = 25
    Caption = '...'
    TabOrder = 0
    OnClick = btn_archivoClick
  end
  object pb_articulos: TProgressBar
    Left = 25
    Top = 82
    Width = 217
    Height = 17
    TabOrder = 1
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 186
    Width = 272
    Height = 19
    Panels = <
      item
        Width = 50
      end>
    ExplicitTop = 209
    ExplicitWidth = 393
  end
  object btn_importar: TButton
    Left = 48
    Top = 133
    Width = 176
    Height = 25
    Caption = 'Importar Saldos por acreditar'
    Enabled = False
    TabOrder = 3
    OnClick = btn_importarClick
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'DriverID=FB'
      'User_Name=sysdba'
      'Password=1'
      'Database=C:\Microsip rafisa\RAFISA VALLE 2016.FDB')
    Left = 288
    Top = 16
  end
  object OpenDialog1: TOpenDialog
    Left = 336
    Top = 16
  end
  object Query: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from almacenes')
    Left = 288
    Top = 80
  end
  object Transaction: TFDTransaction
    Connection = Conexion
    Left = 288
    Top = 136
  end
  object DataSource: TDataSource
    DataSet = Query
    Left = 344
    Top = 80
  end
end
