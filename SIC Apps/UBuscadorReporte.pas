unit UBuscadorReporte;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef,
  FireDAC.VCLUI.Wait, Vcl.ComCtrls, Data.DB, FireDAC.Comp.Client,
  Vcl.Grids, Vcl.StdCtrls, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, Data.Bind.EngExt,
  Vcl.Bind.DBEngExt, System.Rtti, System.Bindings.Outputs,
  Vcl.Bind.Editors, Data.Bind.Components, Data.Bind.DBScope, Vcl.CheckLst,
  Vcl.Bind.Grid, Data.Bind.Grid, frxClass, frxDBSet, frxExportXLSX;

type
  TBuscadorReporte = class(TForm)
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    txtBusquedaExistencias: TEdit;
    Label1: TLabel;
    sgexistencias: TStringGrid;
    pcPrincipal: TPageControl;
    tsExistencias: TTabSheet;
    tsPrecios: TTabSheet;
    txtBusquedaPrecios: TEdit;
    Label2: TLabel;
    sgPrecios: TStringGrid;
    qryPrecios: TFDQuery;
    qryListasprecios: TFDQuery;
    BindingsList1: TBindingsList;
    BindSourceDB2: TBindSourceDB;
    cbAlmacenes: TComboBox;
    Label3: TLabel;
    qryAlmacenes: TFDQuery;
    BindSourceDB3: TBindSourceDB;
    LinkListControlToField1: TLinkListControlToField;
    lblNumRegExistencias: TLabel;
    lblNumRegPrecios: TLabel;
    cbListasPrecios: TCheckListBox;
    BindSourceDB1: TBindSourceDB;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    rbConImpuesto: TRadioButton;
    rbSinImpuesto: TRadioButton;
    DSPrecios: TfrxDBDataset;
    ReportePrecios: TfrxReport;
    DSExistencias: TfrxDBDataset;
    ReporteExistencias: TfrxReport;
    Label4: TLabel;
    cbMonedas: TComboBox;
    qryMonedas: TFDQuery;
    BindSourceDB4: TBindSourceDB;
    LinkListControlToField2: TLinkListControlToField;
    btnVistaPreviaPrecios: TButton;
    btnBuscarPrecios: TButton;
    btnVistaPreviaExistencias: TButton;
    btnBuscarExistencias: TButton;
    btnxportarExistencias: TButton;
    qryExistencias: TFDQuery;
    BindSourceDB5: TBindSourceDB;
    LinkGridToDataSourceBindSourceDB5: TLinkGridToDataSource;
    rbOrdenarClave: TRadioButton;
    rbOrdenarNombre: TRadioButton;
    procedure ActualizarPrecios;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure cbAlmacenesChange(Sender: TObject);
    procedure cbListasPreciosClickCheck(Sender: TObject);
    procedure rbConImpuestoClick(Sender: TObject);
    procedure rbSinImpuestoClick(Sender: TObject);
    procedure cbMonedasChange(Sender: TObject);
    procedure btnVistaPreviaPreciosClick(Sender: TObject);
    procedure txtBusquedaPreciosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnBuscarPreciosClick(Sender: TObject);
    procedure txtBusquedaExistenciasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnBuscarExistenciasClick(Sender: TObject);
    procedure btnVistaPreviaExistenciasClick(Sender: TObject);
    procedure btnxportarExistenciasClick(Sender: TObject);
    procedure rbOrdenarClaveClick(Sender: TObject);
    procedure rbOrdenarNombreClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    lprecios : TStringList;
    orden,qry_exist_str : string;
  end;

var
  BuscadorReporte: TBuscadorReporte;

implementation

{$R *.dfm}

{$region 'Precios'}
procedure TBuscadorReporte.ActualizarPrecios;
var
	i, marcados, consec: integer;
  lista_precio_id, moneda_id, pal,comp : string;
  v: boolean;
begin
	lprecios := TStringList.Create;
  consec := 1;
	marcados := 0;
  for i := 0 to cbListasPrecios.Items.count - 1 do
  begin
    if cbListasPrecios.State[i] = TCheckBoxState.cbChecked then
    	marcados := marcados + 1;
  end;
	moneda_id := qryMonedas.FieldByName('moneda_id').AsString;
	qryPrecios.SQL.Text := 'select a.nombre as articulo ';

  if marcados <= 5 then
  begin
    for i := 0 to cbListasPrecios.Items.count - 1 do
    begin
      if cbListasPrecios.State[i] = TCheckBoxState.cbChecked then
      begin
        v := qryListasprecios.Locate('nombre',cbListasPrecios.Items[i],[]);
        lista_precio_id := qryListasprecios.FieldByName('precio_empresa_id').AsString;
        lprecios.Add(cbListasPrecios.Items[i]);
        if rbConImpuesto.Checked then
          qryPrecios.SQL.Text := qryPrecios.SQL.Text +
            ', (select coalesce(precio,0) from sicbuscador_get_precio_neto(a.articulo_id,'+lista_precio_id+','+moneda_id+')) as p'+inttostr(consec)
        else
          qryPrecios.SQL.Text := qryPrecios.SQL.Text +
            ', coalesce((SELECT coalesce(PRECIO,0) FROM precios_articulos pa '+
            ' where pa.articulo_id = a.articulo_id and pa.precio_empresa_id='+lista_precio_id+
            ' and pa.moneda_id='+moneda_id+'),0) as p'+inttostr(consec);
        consec := consec + 1;
      end
      else;
    end;
    if marcados < 5 then
    for i := marcados +1 to 5 do
    begin
      qryPrecios.SQL.Text := qryPrecios.SQL.Text +
            ', 0 as p'+inttostr(i)
    end;
  end;
	qryPrecios.SQL.Text := qryPrecios.SQL.Text + ' from articulos a order by a.nombre';
  if not qryPrecios.Active then
  	qryPrecios.Open;
  qryPrecios.Refresh;
  btnBuscarPrecios.Click;

  //Encabezados
  for i := 1 to sgPrecios.ColCount - 1 do
  begin
  	if lprecios.Count >= i then
    begin
      sgPrecios.Cells[i,0] := lprecios[i-1];
      sgPrecios.ColWidths[i] := 100;
		end
    else
    begin
    	sgPrecios.ColWidths[i] := 0;
    end;

  end;
  sgPrecios.ColWidths[0] := 400;
end;



procedure TBuscadorReporte.btnBuscarPreciosClick(Sender: TObject);
var
	pal,comp : string;
  i : integer;
begin
	comp := txtBusquedaPrecios.Text;
    i := 0;
    qryExistencias.close;
    if txtBusquedaPrecios.Text <> '' then
    begin
      for pal in comp.Split([' ']) do
      begin
        if i = 0 then
          qryPrecios.Filter := 'upper(articulo) like ''%'+pal+'%'' '
        else
          qryPrecios.Filter := qryPrecios.Filter + ' and upper(articulo) like ''%'+pal+'%''';
        i := i +1 ;
      end;
      qryPrecios.Open;
      qryPrecios.Filtered := True;
    end
    else
    begin
      qryPrecios.Open;
      qryPrecios.Filtered := False;
    end;
    qryPrecios.cLOSE;
    qryPrecios.Open;
    qryPrecios.Refresh;
    for i := 1 to sgPrecios.ColCount - 1 do
    begin
      if lprecios.Count >= i then
      begin
        sgPrecios.Cells[i,0] := lprecios[i-1];
        sgPrecios.ColWidths[i] := 100;
      end
      else
      begin
        sgPrecios.ColWidths[i] := 0;
      end;

    end;
    sgPrecios.ColWidths[0] := 400;
    lblNumRegPrecios.Caption := inttostr(qryPrecios.RecordCount) + ' Articulos';
end;

procedure TBuscadorReporte.btnVistaPreviaExistenciasClick(Sender: TObject);
begin
	ReporteExistencias.PrepareReport(True);
  ReporteExistencias.ShowReport(True);
end;

procedure TBuscadorReporte.btnVistaPreviaPreciosClick(Sender: TObject);
var
	lp:TfrxMemoView;
  i: Integer;
begin
  for i := 1 to sgPrecios.ColCount-1 do
	begin
  	if sgPrecios.ColWidths[i] > 0 then
    begin
	  	lp := ReportePrecios.FindObject('L'+IntToStr(i)) as TfrxMemoView;
      lp.Text := sgPrecios.Cells[i,0];
    end;

  end;
	ReportePrecios.PrepareReport(True);
	ReportePrecios.ShowReport(True);
end;

procedure TBuscadorReporte.btnxportarExistenciasClick(Sender: TObject);
begin
	//expExcelExistencias.FileName := 'existencias';
	ReporteExistencias.PrepareReport(True);
	//ReporteExistencias.Export(expExcelExistencias);
end;

procedure TBuscadorReporte.cbListasPreciosClickCheck(Sender: TObject);
begin
	ActualizarPrecios;
end;

procedure TBuscadorReporte.cbMonedasChange(Sender: TObject);
begin
	ActualizarPrecios;
end;

procedure TBuscadorReporte.rbConImpuestoClick(Sender: TObject);
begin
	ActualizarPrecios;
end;

procedure TBuscadorReporte.rbOrdenarClaveClick(Sender: TObject);
begin
	orden := ' order by clave';
	qryExistencias.Close;
	qryExistencias.sql.Text := qry_exist_str+' order by clave';
  qryExistencias.Open;
end;

procedure TBuscadorReporte.rbOrdenarNombreClick(Sender: TObject);
begin
	orden := ' order by articulo';
	qryExistencias.Close;
	qryExistencias.sql.Text := qry_exist_str+' order by articulo';
  qryExistencias.Open;
end;

procedure TBuscadorReporte.rbSinImpuestoClick(Sender: TObject);
begin
	ActualizarPrecios;
end;

procedure TBuscadorReporte.txtBusquedaPreciosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);

begin
	if key = VK_RETURN then
  begin
		btnBuscarPrecios.Click;
  end;
end;
{$endregion}

{$REGION 'Form'}
procedure TBuscadorReporte.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	qryExistencias.Close;
  qryPrecios.Close;
  qryListasprecios.Close;
  qryAlmacenes.Close;
  qryMonedas.Close;
  qryPrecios.Close;
	Application.Terminate;
end;

procedure TBuscadorReporte.FormShow(Sender: TObject);
begin

	Conexion.ExecSQL(' create or alter procedure sicbuscador_get_precio_neto ('+
                    ' articulo_id integer,'+
                    ' precio_empresa_id integer,'+
                    ' moneda_id integer)'+
                ' returns ('+
                    ' precio numeric(18,6))'+
                ' as'+
                ' begin'+
                  ' SELECT coalesce(PRECIO,0) as precio FROM precios_articulos pa where pa.articulo_id = :articulo_id and'+
                  ' pa.precio_empresa_id = :precio_empresa_id and pa.moneda_id = :moneda_id'+
                  ' into precio;'+
                  ' execute procedure precio_con_impto(articulo_id,precio,''N'',''P'',''N'') returning_values precio;'+
                  ' suspend;'+
                ' end');

	lprecios := TStringList.Create;
	pcPrincipal.ActivePage := tsExistencias;
  try
  	qryExistencias.Open;
    qry_exist_str := qryExistencias.Sql.Text;
  Except
    //qryExistencias.SQL.Text  := qryExistencias2018.SQL.Text;
    qryExistencias.Open;
  end;
  qryPrecios.Open;
  qryAlmacenes.Open;
  qryExistencias.Refresh;
  qryListasprecios.Open;
  qryMonedas.open;

  //se llena el clb con las listas de precios
  qryListasprecios.First;
  while not qryListasprecios.Eof do
  begin
    cbListasPrecios.Items.Add(qryListasprecios.FieldByName('nombre').AsString);
    qryListasprecios.Next;
  end;
  sgPrecios.ColWidths[0] := 400;
  txtBusquedaExistencias.SetFocus;
end;

{$ENDREGION}

{$region 'Existencias'}
procedure TBuscadorReporte.txtBusquedaExistenciasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
	if key = VK_RETURN then
  begin
		btnBuscarExistencias.Click;
  end;
end;

procedure TBuscadorReporte.cbAlmacenesChange(Sender: TObject);
begin
  qryExistencias.Refresh;
end;

procedure TBuscadorReporte.btnBuscarExistenciasClick(Sender: TObject);
var
	pal,comp : string;
  i : integer;
begin
	comp := txtBusquedaExistencias.Text;
    i := 0;
    qryExistencias.close;
    qryExistencias.sql.Text := qry_exist_str + orden;
    if txtBusquedaExistencias.Text <> '' then
    begin
      for pal in comp.Split([' ']) do
      begin
        if i = 0 then
          qryExistencias.Filter := 'upper(articulo) like ''%'+pal+'%'' '
        else
          qryExistencias.Filter := qryExistencias.Filter + ' and upper(articulo) like ''%'+pal+'%''';
        i := i +1 ;
      end;
      qryExistencias.Open;
      qryExistencias.Filtered := True;
    end
    else
    begin
      qryExistencias.Open;
      qryExistencias.Filtered := False;
    end;
    qryExistencias.close;
    qryExistencias.Open;
    qryExistencias.Refresh;
    lblNumRegExistencias.Caption := inttostr(qryExistencias.RecordCount) + ' Articulos';
end;

{$endregion}

end.
