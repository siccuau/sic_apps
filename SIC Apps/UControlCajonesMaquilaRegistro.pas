unit UControlCajonesMaquilaRegistro;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.FB, FireDAC.Phys.FBDef, Data.DB, FireDAC.Comp.Client,
  Vcl.ComCtrls,UControlCajonesMaquilaDetalleEntrada, UControlCajonesMaquilaDetalleSalida,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, Vcl.DBCtrls, FireDAC.VCLUI.Wait;

type
  TControlCajonesMaquilaRegistro = class(TForm)
    txtFolioMaquila: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    lblStatus: TLabel;
    btnLiberar: TButton;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    txtNumCajones: TEdit;
    txtTotalKGMaq: TEdit;
    txtTotalKG: TEdit;
    Label9: TLabel;
    Label10: TLabel;
    txtTarimasSalida: TEdit;
    txtTotalCajas: TEdit;
    Label11: TLabel;
    Label12: TLabel;
    txtPesoPromporCaja: TEdit;
    btnDetalleSalida: TButton;
    btnDetalleEntrada: TButton;
    Label13: TLabel;
    StatusBar1: TStatusBar;
    Conexion: TFDConnection;
    Button1: TButton;
    btnGuardar: TButton;
    QueryProveedores: TFDQuery;
    DSProveedores: TDataSource;
    cbProveedores: TDBLookupComboBox;
    dtpFecha: TDateTimePicker;
    QueryAlmacenEntrada: TFDQuery;
    DSAlmacenEntrada: TDataSource;
    QueryAlmacenSalida: TFDQuery;
    DSAlmacenSalida: TDataSource;
    cbAlmacenesSalida: TDBLookupComboBox;
    cbAlmacenesEntrada: TDBLookupComboBox;
    procedure btnDetalleEntradaClick(Sender: TObject);
    procedure btnDetalleSalidaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    operacion, maquila_id : integer;
  end;

var
  ControlCajonesMaquilaRegistro: TControlCajonesMaquilaRegistro;

implementation

{$R *.dfm}

procedure TControlCajonesMaquilaRegistro.btnDetalleEntradaClick(
  Sender: TObject);
var
  FormEntrada : TControlCajonesMaquilaDetalleEntrada;
begin
  FormEntrada := TControlCajonesMaquilaDetalleEntrada.create(nil);
  FormEntrada.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormEntrada.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormEntrada.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormEntrada.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormEntrada.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
  FormEntrada.maquila_id := maquila_id;

  FormEntrada.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
  FormEntrada.ShowModal;
end;

procedure TControlCajonesMaquilaRegistro.btnDetalleSalidaClick(Sender: TObject);
var
  FormSalida : TControlCajonesMaquilaDetalleSalida;
begin
  FormSalida := TControlCajonesMaquilaDetalleSalida.create(nil);
  FormSalida.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormSalida.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormSalida.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormSalida.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormSalida.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';

  FormSalida.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
  FormSalida.ShowModal;
end;

procedure TControlCajonesMaquilaRegistro.btnGuardarClick(Sender: TObject);
var
  fecha_str, folio_str,num_tarima:string;
  i:integer;

begin
  fecha_str := FormatDateTime('mm/dd/yyyy', dtpFecha.Date);
  if operacion = 0 then
  begin
    // DA DE ALTA LA MAQUILA NUEVA (ENCABEZADO Y LAS TARIMAS ESTIMADAS EN DETALLE DE SALIDA)***************
    maquila_id := Conexion.ExecSQL('insert into sic_maquila values(-1,:proveedor_id,'':fecha'',null,''A'',current_timestamp,null,:tarimas_salida,:aeid,:asid)'+
                      ' returning maquila_id',[cbProveedores.KeyValue,fecha_str,strtoint(txtTarimasSalida.Text),cbAlmacenesEntrada.KeyValue,cbAlmacenesSalida.KeyValue]);
    folio_str := Format('%.*d',[6,(maquila_id)]);
    Conexion.ExecSQL('update sic_maquila set folio = :folio where maquila_id = :mid',[folio_str,maquila_id]);
    btnDetalleSalida.Enabled := True;
    btnDetalleEntrada.Enabled := True;

    // DA DE ALTA LOS DETALLES DE SALIDA
    for i := 1 to strtoint(txtTarimasSalida.Text) do
    begin
      num_tarima := folio_str + Format('%.*d',[3,(i)]);
      Conexion.ExecSQL('insert into sic_maquila_det_salida values (null,:mid,:num_tarima)',[maquila_id])
    end;
  end
  else
  begin
    Conexion.ExecSQL('');
  end;
end;

procedure TControlCajonesMaquilaRegistro.FormShow(Sender: TObject);
begin
  QueryProveedores.open;
  cbProveedores.KeyValue := Conexion.ExecSQLScalar('select first 1 proveedor_id from proveedores order by nombre');
  QueryAlmacenEntrada.open;
  QueryAlmacenSalida.open;
  cbAlmacenesSalida.KeyValue := Conexion.ExecSQLScalar('select first 1 almacen_id from almacenes order by nombre');
  cbAlmacenesEntrada.KeyValue := Conexion.ExecSQLScalar('select first 1 almacen_id from almacenes order by nombre');
end;

end.
