unit UEmpresaBitacora;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait,
  Vcl.StdCtrls, Data.DB, FireDAC.Comp.Client,UMonitorBitacora;

type
  TEmpresaBitacora = class(TForm)
    lbEmpresas: TListBox;
    Conexion: TFDConnection;
    btnAceptar: TButton;
    btnCancelar: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
  private
    { Private declarations }
  public
       ListaEmpresas:TStringList;
  end;

var
  EmpresaBitacora: TEmpresaBitacora;

implementation

{$R *.dfm}

procedure TEmpresaBitacora.btnAceptarClick(Sender: TObject);
var
  FormPrincipal : TMonitorBitacora; //Punto Reorden
 //FormPrincipal : TPuntoReorden;
 i,index:integer;
begin
  FormPrincipal := TMonitorBitacora.Create(nil); //Punto Reorden
  FormPrincipal.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'].Replace('System\config.fdb','') + lbEmpresas.items[lbEmpresas.ItemIndex] + '.fdb';
  FormPrincipal.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormPrincipal.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormPrincipal.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormPrincipal.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
    FormPrincipal.StatusBar1.Panels.Items[0].Text := lbEmpresas.items[lbEmpresas.ItemIndex];

    //ELIMINAR EMPRESA PRINCIPAL DE LA LISTA
   { ListaEmpresas.Find(lbEmpresas.items[lbEmpresas.ItemIndex],Index);
   ListaEmpresas.delete(Index);}

   //AGREGAR EMPRESAS A LA LISTA
  for i := 0 to ListaEmpresas.Count-1 do
  begin
    FormPrincipal.ListaEmpresas.Add(ListaEmpresas.Strings[i]);
  end;

//  FormPrincipal.StatusBar1.Panels.Items[0].Text := listaEmpresas.SelectedItem;
  FormPrincipal.Visible := True;
  FormPrincipal.Show;
  Hide;
end;

procedure TEmpresaBitacora.btnCancelarClick(Sender: TObject);
begin
Application.Terminate;
end;

procedure TEmpresaBitacora.FormCreate(Sender: TObject);
begin
ListaEmpresas:=TStringList.Create;
end;


procedure TEmpresaBitacora.FormShow(Sender: TObject);
var i:integer;
begin
  for i := 0 to ListaEmpresas.Count-1 do
  begin
   lbEmpresas.Items.Add(ListaEmpresas.Strings[i]);
  end;
end;

end.
