unit UClonaDocumentos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, Vcl.ComCtrls, Data.DB, FireDAC.Comp.Client, Vcl.StdCtrls, System.Character,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.VCLUI.Wait;

type
  TClonaDocumentos = class(TForm)
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    cbModuloOrigen: TComboBox;
    Label1: TLabel;
    cbTipoDocumentoOrigen: TComboBox;
    Label2: TLabel;
    gbOrigen: TGroupBox;
    gbDestino: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    cbModuloDestino: TComboBox;
    cbTipoDocumentoDestino: TComboBox;
    btnClonar: TButton;
    txtFolioOrigen: TEdit;
    Label5: TLabel;
    btnBuscarDocumento: TButton;
    lblTipoDocumentoOrigen: TLabel;
    lblTipoDocumentoDestino: TLabel;
    lblNumeroDetalles: TLabel;
    btnCancelar: TButton;
    QueryDetallado: TFDQuery;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbModuloOrigenChange(Sender: TObject);
    procedure cbModuloDestinoChange(Sender: TObject);
    procedure btnBuscarDocumentoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbTipoDocumentoOrigenChange(Sender: TObject);
    procedure cbTipoDocumentoDestinoChange(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnClonarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    tipo_documento_origen,tipo_documento_destino: integer;
    tipos_documentos_origen : TStringList;
    tipos_documentos_destino : TStringList;
    modulo_origen,modulo_destino : string;
    documento_origen_id,num_detalles:integer;
  end;

var
  ClonaDocumentos: TClonaDocumentos;

implementation

{$R *.dfm}

{$Region 'Funciones'}
function GetSerie(folio : String) : String;
var
  i:integer;
begin
  Result := '';
  for I := 1 to (folio.Length) do
  begin
    if (IsLetter(folio[i]))
    then Result := Result+folio[i]
    else exit;
  end;
end;

function GetNumFolio(folio : String) : String;
var
  i,num:integer;
begin
  Result := '';
  for I := 1 to (folio.Length) do
  begin
    if (IsNumber(folio[i]))
    then Result := Result+folio[i];
  end;
end;
{$ENDREGION}

{$Region 'Botones'}
procedure TClonaDocumentos.btnBuscarDocumentoClick(Sender: TObject);
var
  serie,num_folio,folio_str:string;
  query_str:string;

begin
  //*******OBTIENE LA SERIE Y EL FOLIO POR SEPARADO DEL DOCUMENTO PARA LA BUSQUEDA*******
  serie := GetSerie(txtFolioOrigen.Text);
  num_folio := GetNumFolio(txtFolioOrigen.Text);
  txtFolioOrigen.Text := serie + num_folio;
  folio_str := serie + Format('%.*d',[9-length(serie),strtoint(num_folio)]);

  //************OBTIENE EL ID DEL DOCUMENTO ORIGEN Y EL NUMERO DE ESE DOCUMENTO***********
  query_str := 'select * from '+modulo_origen +' where tipo_docto = '+QuotedStr(tipos_documentos_origen[tipo_documento_origen])+' and folio='+QuotedStr(folio_str);
  documento_origen_id := Conexion.ExecSQLScalar(query_str);

  //******************VALIDA SI ENCUENTRA EL DOCUMENTO O NO*******************
  if documento_origen_id = 0 then
  begin
    ShowMessage('Folio no encontrado.');
    txtFolioOrigen.SetFocus;
    txtFolioOrigen.SelectAll;
  end
  else
  begin
    query_str := 'Select count(*) from '+modulo_origen+'_det where '+modulo_origen.Replace('s','')+'_id='+inttostr(documento_origen_id);
    num_detalles := Conexion.ExecSQLScalar(query_str);
    //********* ACTIVA OPCIONES DE CLONAR Y CANCELAR***************
    lblNumeroDetalles.Caption := inttostr(num_detalles)+' Detalle(s)';
    btnClonar.Enabled := true;
    btnCancelar.Enabled := true;
    btnBuscarDocumento.Enabled := false;
    cbModuloOrigen.Enabled := false;
    cbTipoDocumentoOrigen.Enabled := false;
    txtFolioOrigen.Enabled := false;
    btnCancelar.SetFocus;
  end;
end;

procedure TClonaDocumentos.btnCancelarClick(Sender: TObject);
begin
  btnClonar.Enabled := false;
  btnCancelar.Enabled := false;
  btnBuscarDocumento.Enabled := true;
  cbModuloOrigen.Enabled := true;
  cbTipoDocumentoOrigen.Enabled := true;
  txtFolioOrigen.Enabled := true;
  txtFolioOrigen.SetFocus;
  txtFolioOrigen.Clear;
  lblNumeroDetalles.Caption := '';
end;

procedure TClonaDocumentos.btnClonarClick(Sender: TObject);
var
  query_str,serie_dest,folio_str,clave_cliente,estatus,clave_articulo,notas,rol_ve,rol_valor,orden_compra,fecha_orden_str,desc:string;
  documento_destino_id,cliente_id,proveedor_id,consecutivo_dest,folio_id,
  dir_cli_id,almacen_id, moneda_id,cond_pago_id, articulo_id,detalle_id,detalle_origen_id, detalle_juego_id, posicion,vendedor_id,sucursal_id : integer;
  precio_unitario, precio_total_neto, unidades,pctje_dscto: double;
  fecha_orden_compra : TDate;
  fecha_var: Variant;
  campovendedor,valorvendedor:string;
begin
  //**********INSERTA EL ENCABEZADO DE ACUERDO AL MODULO DESTINO**********
  case cbModuloDestino.ItemIndex of
    0:begin
        serie_dest := Conexion.ExecSQLScalar('select serie from folios_ventas where tipo_docto = '+QuotedStr(tipos_documentos_destino[tipo_documento_destino]));
        consecutivo_dest := Conexion.ExecSQLScalar('select consecutivo from folios_ventas where tipo_docto = '+QuotedStr(tipos_documentos_destino[tipo_documento_destino]));
        folio_id := Conexion.ExecSQLScalar('select folio_ventas_id from folios_ventas where tipo_docto = '+QuotedStr(tipos_documentos_destino[tipo_documento_destino]));
        //********** OBTIENE EL SIGUIENTE FOLIO DEL DOCUMENTO DESTINO Y SE ACTUALIZA LA TABLA DE FOLIOS*******
        if serie_dest <> '@'
        then folio_str := serie_dest + Format('%.*d',[9-length(serie_dest),(consecutivo_dest)])
        else folio_str := Format('%.*d',[9,(consecutivo_dest)]);
        Conexion.ExecSQL('update folios_ventas set consecutivo=consecutivo+1 where folio_ventas_id=:folio_id',[folio_id]);

        //********** SE OBTIENEN LOS DEMAS DATOS PARA DAR DE ALTA EL ENCABEZADO******************************
        if modulo_origen = 'doctos_ve'  then
        begin
             cliente_id := Conexion.ExecSQLScalar('select cliente_id from doctos_ve where docto_ve_id = '+inttostr(documento_origen_id));
             orden_compra := Conexion.ExecSQLScalar('select coalesce(orden_compra,'''') from doctos_ve where docto_ve_id = '+inttostr(documento_origen_id));
             desc := Conexion.ExecSQLScalar('select coalesce(descripcion,'''') from doctos_ve where docto_ve_id='+IntToStr(documento_origen_id));
             System.SysUtils.formatsettings.ShortDateFormat := 'mm/dd/yyyy';
             fecha_var := Conexion.ExecSQLScalar('select fecha_orden_compra from doctos_ve where docto_ve_id ='+inttostr(documento_origen_id));
             if fecha_var = Null then
             begin
                  fecha_orden_str := 'null';
             end
             else
             begin
                  fecha_orden_compra := fecha_var;
                  fecha_orden_str := Quotedstr(DateToStr(fecha_orden_compra));
             end;
        end
        else
        begin
             cliente_id := Conexion.ExecSQLScalar('select valor from registry where nombre=''CLIENTE_EVENTUAL_ID''');
             orden_compra := '';
             fecha_orden_str := 'null';
             desc := Conexion.ExecSQLScalar('select coalesce(descripcion,'''') from doctos_cm where docto_cm_id = '+IntToStr(documento_origen_id));
        end;


        clave_cliente := Conexion.ExecSQLScalar('select coalesce(clave_cliente,'''') from get_clave_cli(:id)',[cliente_id]);
        dir_cli_id := Conexion.ExecSQLScalar('select dir_cli_id from dirs_clientes where cliente_id = :cid and es_dir_ppal=''S''',[cliente_id]);
        almacen_id := Conexion.ExecSQLScalar('select almacen_id from almacenes where es_ppal=''S''');
        moneda_id := Conexion.ExecSQLScalar('select moneda_id from '+modulo_origen+' where '+modulo_origen.Replace('s','')+'_id='+inttostr(documento_origen_id));
        cond_pago_id := Conexion.ExecSQLScalar('select first 1 cond_pago_id from condiciones_pago');
       // vendedor_id:= Conexion.ExecSQLScalar('select COALESCE(vendedor_id,0) from clientes where cliente_id=:cli_id',[cliente_id]);
        if Conexion.ExecSQLScalar('select count(vendedor_id) from doctos_ve where docto_ve_id=:id',[inttostr(documento_origen_id)])>0 then
        vendedor_id:= Conexion.ExecSQLScalar('select vendedor_id from doctos_ve where docto_ve_id=:id',[inttostr(documento_origen_id)])
        else vendedor_id:=0;
        //*********VALIDA SI EXISTE EL CLIENTE EVENTUAL PARA DAR DE ALTA EL ENCABEZADO***********************
        campovendedor:='';
        valorvendedor:='';
        if vendedor_id <> 0 then
        begin
        campovendedor:='vendedor_id,';
        valorvendedor:=inttostr(vendedor_id)+',';
        end;

        //MSP 2020
        sucursal_id:=Conexion.ExecSQLScalar('select sucursal_id from sucursales where nombre=''Matriz''');
        if cliente_id <> 0 then
        begin
          query_str := 'insert into doctos_ve (docto_ve_id,tipo_docto,sucursal_id,folio,fecha,clave_cliente,descripcion,'+campovendedor+
                       ' cliente_id,dir_cli_id,dir_consig_id,almacen_id,moneda_id,tipo_cambio,'+
                       ' sistema_origen,tipo_dscto,estatus,cond_pago_id,orden_compra,fecha_orden_compra) values (-1,'''+tipos_documentos_destino[tipo_documento_destino]+''','''+inttostr(sucursal_id)+''','''+folio_str+''','+
                       ' current_date,'''+clave_cliente+''','''+desc+''','+valorvendedor+inttostr(cliente_id)+','+inttostr(dir_cli_id)+','+inttostr(dir_cli_id)+','+
                       ' '+inttostr(almacen_id)+','+inttostr(moneda_id)+',1,''VE'',''P'',''P'','+inttostr(cond_pago_id)+','''+orden_compra+''','+fecha_orden_str+') returning docto_ve_id';
          documento_destino_id := Conexion.ExecSQLScalar(query_str);
        end
        else
        begin
          showmessage('Falta el Cliente Eventual en las Preferencias de la Empresa (Ventas Microsip)');
        end;
      end;
    1:begin
        serie_dest := Conexion.ExecSQLScalar('select serie from folios_compras where tipo_docto = '+QuotedStr(tipos_documentos_destino[tipo_documento_destino]));
        consecutivo_dest := Conexion.ExecSQLScalar('select consecutivo from folios_compras where tipo_docto = '+QuotedStr(tipos_documentos_destino[tipo_documento_destino]));
        folio_id := Conexion.ExecSQLScalar('select folio_compras_id from folios_compras where tipo_docto = '+QuotedStr(tipos_documentos_destino[tipo_documento_destino]));
        //********** OBTIENE EL SIGUIENTE FOLIO DEL DOCUMENTO DESTINO Y SE ACTUALIZA LA TABLA DE FOLIOS*******
        if serie_dest <> '@'
        then folio_str := serie_dest + Format('%.*d',[9-length(serie_dest),(consecutivo_dest)])
        else folio_str := Format('%.*d',[9,(consecutivo_dest)]);
        Conexion.ExecSQL('update folios_compras set consecutivo=consecutivo+1 where folio_compras_id=:folio_id',[folio_id]);

        //********** SE OBTIENEN LOS DEMAS DATOS PARA DAR DE ALTA EL ENCABEZADO******************************
        cliente_id := Conexion.ExecSQLScalar('select valor from registry where nombre=''PROV_EVENTUAL_ID''');
        clave_cliente := Conexion.ExecSQLScalar('select coalesce(clave_prov,'''') from get_clave_prov(:id)',[cliente_id]);
        almacen_id := Conexion.ExecSQLScalar('select almacen_id from almacenes where es_ppal=''S''');
        moneda_id := Conexion.ExecSQLScalar('select moneda_id from '+modulo_origen+' where '+modulo_origen.Replace('s','')+'_id='+inttostr(documento_origen_id));
        cond_pago_id := Conexion.ExecSQLScalar('select first 1 cond_pago_id from condiciones_pago_cp');
        if tipo_documento_destino = 5
        then estatus := 'N'
        else estatus := 'P';

        //*********VALIDA SI EXISTE EL CLIENTE EVENTUAL PARA DAR DE ALTA EL ENCABEZADO***********************
                //MSP 2020
        sucursal_id:=Conexion.ExecSQLScalar('select sucursal_id from sucursales where nombre=''Matriz''');
        if cliente_id <> 0 then
        begin
          query_str := 'insert into doctos_cm (docto_cm_id,tipo_docto,sucursal_id,folio,fecha,clave_prov,'+
                       ' proveedor_id,almacen_id,moneda_id,tipo_cambio,'+
                       ' sistema_origen,tipo_dscto,estatus,cond_pago_id) values (-1,'''+tipos_documentos_destino[tipo_documento_destino]+''','''+inttostr(sucursal_id)+''','''+folio_str+''','+
                       ' current_date,'''+clave_cliente+''','+inttostr(cliente_id)+','+
                       ' '+inttostr(almacen_id)+','+inttostr(moneda_id)+',1,''CM'',''P'','''+estatus+''','+inttostr(cond_pago_id)+') returning docto_cm_id';
          documento_destino_id := Conexion.ExecSQLScalar(query_str);
        end
        else
        begin
          showmessage('Falta el Proveedor Eventual en las Preferencias de la Empresa (Compras Microsip)');
        end;
      end;
  end;

  //************** SE VALIDA SI ES VENTAS PARA EXTRAER EL ROL EN LA CONSULTA DE LOS DETALLES.**********
  if modulo_origen='doctos_ve'
  then rol_ve := ',rol'
  else rol_ve := '';

  //*************** DESPUES DE DAR DE ALTA EL ENCABEZADO SE DAN DE ALTA LOS DETALLES************
  query_str := 'select '+modulo_origen.Replace('s','')+'_det_id, articulo_id,unidades,precio_unitario,precio_total_neto,notas,posicion,pctje_dscto'+rol_ve+
                ' from '+modulo_origen+'_det where '+modulo_origen.Replace('s','')+'_id='+inttostr(documento_origen_id)+' order by '+modulo_origen.Replace('s','')+'_det_id, posicion';
  QueryDetallado.SQL.Text := query_str;
  QueryDetallado.Open;
  QueryDetallado.First;
  while not QueryDetallado.Eof do
  begin
    //**************DE LA CONSULTA SE SACAN LOS DATOS A INSERTAR POR DETALLE
    articulo_id := QueryDetallado.FieldByName('articulo_id').AsInteger;
    detalle_origen_id := QueryDetallado.FieldByName(modulo_origen.Replace('s','')+'_det_id').AsInteger;
    clave_articulo := Conexion.ExecSQLScalar('select coalesce(clave_articulo,'''') from get_clave_art(:aid)',[articulo_id]);
    unidades := QueryDetallado.FieldByName('unidades').AsFloat;
    precio_unitario := QueryDetallado.FieldByName('precio_unitario').AsFloat;
    precio_total_neto := QueryDetallado.FieldByName('precio_total_neto').AsFloat;
    notas := QueryDetallado.FieldByName('notas').AsString;
    pctje_dscto := QueryDetallado.FieldByName('pctje_dscto').AsFloat;
    posicion := QueryDetallado.FieldByName('posicion').Asinteger;

    //*******************SE VALIDAN LOS ROLES DE ARTICULO************************

    //SI ES COMPRAS (MODULO DESTINO)

     // rol_ve := '';




         if Conexion.ExecSQLScalar('select count(componente_id) from juegos_det where componente_id=:aid',[articulo_id])=0 then
     begin
     rol_valor := ','+QuotedStr('N');
     end
     else rol_valor :=  ','+QuotedStr('C');

            if modulo_origen='doctos_ve'
    then rol_valor := ','+QuotedStr(QueryDetallado.FieldByName('rol').AsString);

    //*************SE INSERTA EL DETALLE SOLO SI EL ROL ES DIFERENTE A 'C' (COMPONENTE)


    if ((((rol_valor = ','+QuotedStr('J')) or (rol_valor = ','+QuotedStr('N'))){ and (modulo_destino = 'doctos_cm')) or (modulo_destino = 'doctos_ve'})) then
    begin

      if modulo_destino = 'doctos_cm' then
      begin
        rol_valor := '';
        rol_ve := '';
      end;

         if modulo_destino = 'doctos_ve' then
      begin
        rol_ve := ',rol';
      end;

      query_str := 'insert into '+modulo_destino+'_det ('+modulo_destino.Replace('s','')+'_det_id, '+modulo_destino.Replace('s','')+'_id,'+
                   ' clave_articulo,articulo_id,unidades,precio_unitario,precio_total_neto,notas,posicion,pctje_dscto'+rol_ve+') values(-1,'+
                   ' '+inttostr(documento_destino_id)+','''+clave_articulo+''','+inttostr(articulo_id)+','+FloatToStr(unidades)+
                   ' ,'+FloatToStr(precio_unitario)+','+FloatToStr(precio_total_neto)+','+QuotedStr(notas)+','+inttostr(posicion)+','+floattostr(pctje_dscto)+''+rol_valor+') returning '+modulo_destino.Replace('s','')+'_det_id';
      detalle_id := Conexion.ExecSQLScalar(query_str);

      if rol_valor = ','+QuotedStr('J')  then
        detalle_juego_id := detalle_id;

      if ((rol_valor = ','+QuotedStr('C')) and (modulo_destino = 'doctos_ve'))
      then Conexion.ExecSQL('insert into sub_movtos_ve values(:id,:did)',[detalle_juego_id,detalle_id]);

    end;
    QueryDetallado.Next;
  end;

  if cbModuloDestino.ItemIndex = 0  then
  begin
    Conexion.ExecSQL('execute procedure CALC_TOTALES_DOCTO_VE(:id,''N'')',[documento_destino_id]);
    Conexion.ExecSQL('execute procedure aplica_docto_ve(:id)',[documento_destino_id]);
  end
  else
  begin
    Conexion.ExecSQL('execute procedure calc_totales_docto_cm(:id,''N'')',[documento_destino_id]);
    Conexion.ExecSQL('execute procedure aplica_docto_cm(:id)',[documento_destino_id]);
  end;

  ShowMessage('Documento Clonado. '+cbTipoDocumentoDestino.Text+' con folio: '+folio_str);
  btnCancelar.Click;

end;


{$ENDREGION}

{$REGION 'Combos'}




procedure TClonaDocumentos.cbModuloDestinoChange(Sender: TObject);
begin
  case cbModuloDestino.ItemIndex of
    0:begin
        cbTipoDocumentoDestino.Items.Clear;
        cbTipoDocumentoDestino.items.Add('Cotización');
        cbTipoDocumentoDestino.items.Add('Pedido');
        cbTipoDocumentoDestino.items.Add('Remisión');
        cbTipoDocumentoDestino.ItemIndex := 0;
        tipo_documento_destino := 0;
        modulo_destino := 'doctos_ve';
        end;

    1:begin
        cbTipoDocumentoDestino.Items.Clear;
        cbTipoDocumentoDestino.items.Add('Orden de Compra');
        cbTipoDocumentoDestino.items.Add('Recepción de Mercancia');
        cbTipoDocumentoDestino.items.Add('Compra');
        cbTipoDocumentoDestino.ItemIndex := 0;
        tipo_documento_destino := 3;
        modulo_destino := 'doctos_cm';
        end;
  end;
end;

procedure TClonaDocumentos.cbModuloOrigenChange(Sender: TObject);
begin
  case cbModuloOrigen.itemindex of
    0:begin
        cbTipoDocumentoOrigen.Items.Clear;
        cbTipoDocumentoOrigen.items.Add('Cotización');
        cbTipoDocumentoOrigen.items.Add('Pedido');
        cbTipoDocumentoOrigen.items.Add('Remisión');
        cbTipoDocumentoOrigen.items.Add('Factura');
        cbTipoDocumentoOrigen.ItemIndex := 0;
        tipo_documento_origen := 0;
        modulo_origen := 'doctos_ve';
        end;

    1:begin
        cbTipoDocumentoOrigen.Items.Clear;
        cbTipoDocumentoOrigen.items.Add('Orden de Compra');
        cbTipoDocumentoOrigen.items.Add('Recepción de Mercancia');
        cbTipoDocumentoOrigen.items.Add('Compra');
        cbTipoDocumentoOrigen.ItemIndex := 0;
        tipo_documento_origen := 4;
        modulo_origen := 'doctos_cm';
        end;
  end;
end;

procedure TClonaDocumentos.cbTipoDocumentoDestinoChange(Sender: TObject);
begin
  case cbModuloDestino.ItemIndex of
    0:begin
        tipo_documento_destino := cbTipoDocumentoDestino.ItemIndex;
        end;

    1:begin
        tipo_documento_destino := 3+cbTipoDocumentoDestino.ItemIndex;
        end;
  end;
end;

procedure TClonaDocumentos.cbTipoDocumentoOrigenChange(Sender: TObject);
begin
  case cbModuloOrigen.ItemIndex of
    0:begin
        tipo_documento_origen := cbTipoDocumentoOrigen.ItemIndex;
        end;
    1:begin
        tipo_documento_origen := 4+cbTipoDocumentoOrigen.ItemIndex;
        end;
  end;

end;

{$ENDREGION}

{$REGION 'Formulario'}
procedure TClonaDocumentos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Application.Terminate;
end;

procedure TClonaDocumentos.FormShow(Sender: TObject);
begin
  tipo_documento_origen := 0;
  tipo_documento_destino := 0;
  tipos_documentos_origen := TStringList.Create;
  tipos_documentos_destino := TStringList.Create;
  tipos_documentos_origen.CommaText := 'C,P,R,F,O,R,C';
  tipos_documentos_destino.CommaText := 'C,P,R,O,R,C';
  modulo_origen := 'doctos_ve';
  modulo_destino := 'doctos_ve';
end;
{$ENDREGION}




end.
