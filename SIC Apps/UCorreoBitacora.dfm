object CorreoBitacora: TCorreoBitacora
  Left = 0
  Top = 0
  Caption = 'Configuraracion'
  ClientHeight = 188
  ClientWidth = 639
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 337
    Height = 169
    Caption = 'Correo Electronico Emisor'
    TabOrder = 0
    object Label1: TLabel
      Left = 19
      Top = 20
      Width = 94
      Height = 13
      Caption = 'Nombre de usuario:'
    end
    object Label2: TLabel
      Left = 53
      Top = 52
      Width = 60
      Height = 13
      Caption = 'Contrase'#241'a:'
    end
    object Label3: TLabel
      Left = 69
      Top = 87
      Width = 44
      Height = 13
      Caption = 'Servidor:'
    end
    object btnCancelar: TButton
      Left = 53
      Top = 123
      Width = 75
      Height = 25
      Caption = 'Cancelar'
      TabOrder = 0
      OnClick = btnCancelarClick
    end
    object btnGuardar: TButton
      Left = 142
      Top = 118
      Width = 75
      Height = 35
      Caption = 'Guardar'
      TabOrder = 1
      OnClick = btnGuardarClick
    end
    object txtContrasena: TEdit
      Left = 119
      Top = 49
      Width = 133
      Height = 21
      PasswordChar = '*'
      TabOrder = 2
    end
    object txtCorreo: TEdit
      Left = 119
      Top = 17
      Width = 205
      Height = 21
      TabOrder = 3
    end
    object cbServidor: TComboBox
      Left = 119
      Top = 84
      Width = 69
      Height = 21
      ItemIndex = 0
      TabOrder = 4
      Text = 'Hotmail'
      Items.Strings = (
        'Hotmail'
        'G-mail')
    end
  end
  object GroupBox2: TGroupBox
    Left = 360
    Top = 8
    Width = 265
    Height = 169
    Caption = 'Modulos incluidos'
    TabOrder = 1
    object cb_cc: TCheckBox
      Left = 16
      Top = 24
      Width = 121
      Height = 17
      Caption = 'Cuentas por Cobrar'
      TabOrder = 0
      OnClick = cb_ccClick
    end
    object cb_in: TCheckBox
      Left = 16
      Top = 47
      Width = 82
      Height = 17
      Caption = 'Inventarios'
      TabOrder = 1
      OnClick = cb_inClick
    end
    object cb_ve: TCheckBox
      Left = 16
      Top = 70
      Width = 58
      Height = 17
      Caption = 'Ventas'
      TabOrder = 2
      OnClick = cb_veClick
    end
    object cb_cp: TCheckBox
      Left = 16
      Top = 93
      Width = 114
      Height = 17
      Caption = 'Cuentas por Pagar'
      TabOrder = 3
      OnClick = cb_cpClick
    end
    object cb_cm: TCheckBox
      Left = 16
      Top = 116
      Width = 66
      Height = 17
      Caption = 'Compras'
      TabOrder = 4
      OnClick = cb_cmClick
    end
    object cb_pv: TCheckBox
      Left = 16
      Top = 139
      Width = 98
      Height = 17
      Caption = 'Punto de Venta'
      TabOrder = 5
      OnClick = cb_pvClick
    end
    object cb_co: TCheckBox
      Left = 160
      Top = 24
      Width = 81
      Height = 17
      Caption = 'Contabilidad'
      TabOrder = 6
      OnClick = cb_coClick
    end
    object cb_ba: TCheckBox
      Left = 160
      Top = 47
      Width = 65
      Height = 17
      Caption = 'Bancos'
      TabOrder = 7
      OnClick = cb_baClick
    end
    object cb_no: TCheckBox
      Left = 160
      Top = 70
      Width = 65
      Height = 17
      Caption = 'Nomina'
      TabOrder = 8
      OnClick = cb_noClick
    end
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'Database=E:\Microsip Pruebas\SIC 2019.FDB'
      'DriverID=FB')
    Left = 264
    Top = 94
  end
end
