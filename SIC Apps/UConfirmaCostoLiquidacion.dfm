object ConfirmaCostoLiquidacion: TConfirmaCostoLiquidacion
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biHelp]
  BorderStyle = bsSingle
  Caption = 'Deseas Recostear con el Sugerido?'
  ClientHeight = 164
  ClientWidth = 373
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 53
    Top = 40
    Width = 73
    Height = 13
    Caption = 'Costo Sugerido'
  end
  object lblCalculado: TLabel
    Left = 149
    Top = 64
    Width = 73
    Height = 13
    Caption = 'Costo Sugerido'
  end
  object txtCosto: TEdit
    Left = 149
    Top = 37
    Width = 161
    Height = 21
    TabOrder = 0
  end
  object Button1: TButton
    Left = 63
    Top = 104
    Width = 75
    Height = 25
    Caption = 'Si'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 144
    Top = 104
    Width = 75
    Height = 25
    Caption = 'No'
    TabOrder = 2
    OnClick = Button2Click
  end
  object btnCancelar: TButton
    Left = 225
    Top = 104
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 3
    OnClick = btnCancelarClick
  end
end
