unit UImportaSxacreditar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, Data.DB, FireDAC.Comp.Client, FireDAC.Comp.DataSet,
  Vcl.ComCtrls, Vcl.StdCtrls,  ActiveX, ComObj;

type
  TImportaSxacreditar = class(TForm)
    lbl_archivo: TLabel;
    lbl_progress: TLabel;
    btn_archivo: TButton;
    pb_articulos: TProgressBar;
    StatusBar1: TStatusBar;
    btn_importar: TButton;
    Conexion: TFDConnection;
    OpenDialog1: TOpenDialog;
    Query: TFDQuery;
    Transaction: TFDTransaction;
    DataSource: TDataSource;
    procedure btn_importarClick(Sender: TObject);
    procedure btn_archivoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ImportaSxacreditar: TImportaSxacreditar;

implementation

{$R *.dfm}

procedure TImportaSxacreditar.btn_archivoClick(Sender: TObject);
begin
  if OpenDialog1.Execute then
  begin
    btn_importar.Enabled := True;
    lbl_archivo.Caption := OpenDialog1.FileName;
  end
  else
  begin
    btn_importar.Enabled := False;
    lbl_archivo.Caption := '';
  end;

end;

procedure TImportaSxacreditar.btn_importarClick(Sender: TObject);
var
  logfile :TextFile;
  XL: OleVariant;
  i, num_rows, concepto_id, cliente_id, documento_id : integer;
  costo, unidades : double;
  concepto_nombre, fecha, query_str, folio, nombre_cliente, clave_cliente, tipo_cambio, importe, impuesto : string;
begin

  // SE CREA LA INSTANCIA DEL ARCHIVO DE EXCEL
  XL := CreateOleObject('Excel.Application');
  XL.WorkBooks.Open(OpenDialog1.FileName);
  num_rows := XL.Activesheet.UsedRange.Rows.Count;
  pb_articulos.Position := 0;
  pb_articulos.Max := num_rows;

  for i := 1 to num_rows do
  begin
      //SE BUSCA EL ARTICULO POR CLAVE
      concepto_nombre := XL.ActiveSheet.Cells[i,1].Value;
      concepto_id := Conexion.ExecSQLScalar('select concepto_cc_id from conceptos_cc where nombre=:n',[concepto_nombre]);

      folio := XL.ActiveSheet.Cells[i,2].Value;
      fecha := XL.ActiveSheet.Cells[i,3].Value;
      nombre_cliente := XL.ActiveSheet.Cells[i,4].Value;
      cliente_id := Conexion.ExecSQLScalar('select cliente_id from clientes where nombre=:n',[nombre_cliente]);
      clave_cliente := Conexion.ExecSQLScalar('select first 1 clave_cliente from claves_clientes where cliente_id=:cc',[cliente_id]);
      tipo_cambio := XL.ActiveSheet.Cells[i,5].Value;
      importe := XL.ActiveSheet.Cells[i,6].Value;
      impuesto := XL.ActiveSheet.Cells[i,7].Value;

      //SE CREA EL ENCABEZADO DEL DOCUMENTO DE CUENTAS POR COBRAR
      query_str := 'insert into doctos_cc(docto_cc_id,concepto_cc_id,folio,naturaleza_concepto,fecha,clave_cliente,cliente_id,'+
                'tipo_cambio,sistema_origen,estatus,estatus_ant) '+
                'values (-1,:p1,:p2,'+Quotedstr('R')+',:p3,:p4,:p5,:p6,'+Quotedstr('CC')+','+QuotedStr('N')+','+QuotedStr('N')+') returning docto_cc_id;';
      documento_id := Conexion.ExecSQLScalar(query_str,[concepto_id,folio,fecha,clave_cliente,cliente_id,tipo_cambio]);

      //SE INSERTA EL DETALLE
      query_str := 'insert into importes_doctos_cc(impte_docto_cc_id,docto_cc_id,fecha,estatus,tipo_impte,importe,impuesto)'+
                  ' values (-1,:p1,:p2,'+QuotedStr('N')+','+QuotedStr('A')+',:p3,:p4)';
      Conexion.ExecSQL(query_str,[documento_id,fecha,importe,impuesto]);

      //INFORMACION VISUAL
      lbl_progress.Caption := inttostr(i)+'/'+inttostr(num_rows);
      pb_articulos.Position := pb_articulos.Position + 1;
  end;
  XL.Quit;
  XL := Unassigned;

  showmessage('Proceso Terminado.');

end;

end.
